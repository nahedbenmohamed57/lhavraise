-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  sam. 08 fév. 2020 à 22:19
-- Version du serveur :  5.5.56-MariaDB
-- Version de PHP :  5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `admin_lh`
--

-- --------------------------------------------------------

--
-- Structure de la table `access`
--

CREATE TABLE `access` (
  `access_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `access`
--

INSERT INTO `access` (`access_id`, `role_id`, `module_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 2, 1),
(12, 2, 2),
(13, 2, 3),
(14, 2, 4),
(15, 2, 5),
(16, 2, 6),
(17, 2, 7),
(18, 2, 8),
(19, 2, 10),
(20, 3, 1),
(21, 3, 6),
(22, 3, 7);

-- --------------------------------------------------------

--
-- Structure de la table `action`
--

CREATE TABLE `action` (
  `action_id` int(11) NOT NULL,
  `action_name` varchar(60) NOT NULL,
  `action_perim` varchar(100) NOT NULL,
  `echeance` varchar(100) NOT NULL,
  `periode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `action`
--

INSERT INTO `action` (`action_id`, `action_name`, `action_perim`, `echeance`, `periode`) VALUES
(1, 'première action', 'pôle adulte', '', 3);

-- --------------------------------------------------------

--
-- Structure de la table `correlation`
--

CREATE TABLE `correlation` (
  `correlation_id` int(11) NOT NULL,
  `id_determinant` int(11) NOT NULL,
  `id_facteur` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `correlation`
--

INSERT INTO `correlation` (`correlation_id`, `id_determinant`, `id_facteur`, `value`) VALUES
(1, 27, 1, 1),
(2, 27, 2, 1),
(3, 27, 3, 1),
(4, 27, 2, 1),
(5, 28, 1, 1),
(6, 28, 2, 1),
(7, 28, 2, 1),
(8, 28, 4, 1),
(9, 29, 1, 1),
(10, 29, 2, 1),
(11, 29, 3, 1),
(12, 29, 2, 1),
(13, 29, 1, 1),
(14, 30, 1, 1),
(15, 30, 2, 1),
(16, 30, 3, 1),
(17, 31, 1, 1),
(18, 31, 2, 1),
(19, 31, 3, 1),
(20, 32, 1, 1),
(21, 32, 2, 1),
(22, 33, 1, 1),
(23, 33, 2, 1),
(24, 33, 2, 1),
(25, 34, 2, 1),
(26, 34, 3, 1),
(27, 34, 2, 1),
(28, 35, 3, 1),
(29, 36, 3, 1),
(30, 36, 2, 1),
(31, 37, 3, 1),
(32, 37, 2, 1),
(33, 38, 2, 1),
(34, 39, 1, 1),
(35, 39, 3, 1),
(36, 39, 2, 1),
(37, 40, 1, 1),
(38, 40, 3, 1),
(39, 40, 2, 1),
(40, 41, 1, 1),
(41, 41, 3, 1),
(42, 41, 2, 1),
(43, 42, 3, 1),
(44, 43, 2, 1),
(45, 44, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `determinant`
--

CREATE TABLE `determinant` (
  `determinant_id` int(11) NOT NULL,
  `determinant_name` varchar(250) NOT NULL,
  `determinant_importance` int(11) DEFAULT '0',
  `family_id` int(11) NOT NULL,
  `family_nature` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `determinant`
--

INSERT INTO `determinant` (`determinant_id`, `determinant_name`, `determinant_importance`, `family_id`, `family_nature`) VALUES
(27, 'Absentéisme hors mat + formation', 0, 11, 'negatif'),
(28, 'Taux de remplacement', 0, 11, 'positif'),
(29, 'Nb d\'absences courtes (3 jours et moins)', 0, 11, 'negatif'),
(30, 'Nb de PP en arrêt au moins 1 foi', 0, 0, ''),
(31, 'Nb de jour moyen par ETP', 0, 11, 'negatif'),
(32, 'Nb AT / ETP', 0, 11, 'negatif'),
(33, 'Nb jour AT / ETP', 0, 11, 'negatif'),
(34, 'Turn-over de l\'effectif du périmètre évalué', 0, 12, 'negatif'),
(35, 'Turn-over des professionnels (lien hiérarchique)', 0, 12, 'negatif'),
(36, 'Taux de CDD', 0, 12, 'negatif'),
(37, 'Nb de démissions + Nb de ruptures conventionnelle', 0, 0, ''),
(53, 'Taux de formation', 0, 12, 'positif'),
(39, 'Taux de restriction d\'aptitude', 0, 13, 'negatif'),
(40, 'Nb d\'inaptitudes au poste', 0, 13, 'negatif'),
(41, 'Nb de maladie professionnelle', 0, 13, 'negatif'),
(42, 'Ratio d\'encadrement', 0, 14, 'negatif'),
(43, 'Nb d’évènements indésirables - Typologie : Violences', 0, 14, 'negatif'),
(44, 'Nb d’évènements indésirables (Global)', 0, 14, 'negatif'),
(47, 'test 2', 0, 0, 'positif'),
(51, 'Nb de démissions + Nb de ruptures conventionnelles', 0, 12, 'negatif'),
(52, 'Nb de PP en arrêt au moins 1 fois', 0, 11, 'negatif');

-- --------------------------------------------------------

--
-- Structure de la table `device`
--

CREATE TABLE `device` (
  `device_id` int(11) NOT NULL,
  `device_name` varchar(250) NOT NULL,
  `determinant_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `device_has_listing`
--

CREATE TABLE `device_has_listing` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `establishment`
--

CREATE TABLE `establishment` (
  `id_perimetre` int(11) NOT NULL,
  `perimetre_nom` varchar(250) NOT NULL,
  `pole_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `establishment`
--

INSERT INTO `establishment` (`id_perimetre`, `perimetre_nom`, `pole_id`) VALUES
(4, 'Centre d\'Activités de Jour La Salamandre', 0),
(6, 'MAS les Marronniers', 0),
(7, 'FAM La Salamandre', 4),
(9, 'IMP La renaissance', 5),
(13, 'IME L\'arbre à papillons', 5),
(15, 'SESSAD Autisme', 5),
(54, 'EFELIHA ', 9),
(55, 'ESAT Porte Océane', 9),
(57, 'CAP Emploi', 9),
(59, 'ESAT La lézarde', 9),
(105, 'test', 0),
(106, 'etap 44', 0),
(107, 'etap 44', 0),
(109, 'MAS Manoir d\'Epremesnil', 4),
(113, 'FAM Le Perrey', 4),
(114, 'SAMSAH', 4),
(115, 'SAVS', 4),
(116, 'Foyer Edmond Debraize', 4),
(117, 'MAS Les Constellations', 4),
(118, 'IMP L\'espérance', 5),
(119, 'EEAP Les Myosotis', 5);

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

CREATE TABLE `evaluation` (
  `evaluation_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `avancement` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `id_perimetre` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evaluation`
--

INSERT INTO `evaluation` (`evaluation_id`, `device_id`, `listing_id`, `avancement`, `user_id`, `id_perimetre`) VALUES
(60, 55, 4, 50, 1, 0),
(59, 54, 8, 100, 1, 0),
(58, 53, 4, 50, 1, 0),
(57, 52, 7, 88, 1, 0),
(56, 51, 3, 38, 1, 0),
(55, 50, 7, 88, 1, 0),
(54, 49, 4, 50, 1, 0),
(53, 48, 6, 75, 1, 0),
(52, 47, 4, 50, 1, 0),
(51, 46, 6, 75, 1, 0),
(50, 45, 3, 38, 1, 0),
(49, 44, 6, 75, 1, 0),
(48, 42, 6, 75, 1, 0),
(47, 43, 3, 38, 1, 0),
(61, 43, 3, 38, 7, 0),
(62, 42, 5, 63, 7, 0),
(63, 44, 3, 38, 7, 0),
(64, 45, 7, 88, 7, 0),
(65, 46, 4, 50, 7, 0),
(66, 47, 7, 88, 7, 0),
(67, 48, 5, 63, 7, 0),
(68, 49, 4, 50, 7, 0),
(69, 50, 8, 100, 7, 0),
(70, 51, 7, 88, 7, 0),
(71, 52, 5, 63, 7, 0),
(72, 53, 5, 63, 7, 0),
(73, 54, 2, 25, 7, 0),
(74, 55, 6, 75, 7, 0),
(75, 66, 4, 50, 5, 0),
(76, 67, 32, 80, 5, 0),
(77, 68, 3, 38, 5, 0),
(78, 69, 33, 100, 5, 0),
(79, 70, 3, 38, 5, 0),
(80, 71, 31, 60, 5, 0),
(81, 42, 8, 100, 5, 4),
(82, 43, 8, 100, 5, 4),
(83, 44, 8, 100, 5, 4),
(84, 45, 8, 100, 5, 4),
(85, 46, 8, 100, 5, 4),
(86, 47, 8, 100, 5, 4),
(87, 48, 8, 100, 5, 4),
(88, 49, 8, 100, 5, 4),
(89, 50, 8, 100, 5, 4),
(90, 51, 8, 100, 5, 4),
(91, 52, 8, 100, 5, 4),
(92, 53, 8, 100, 5, 4),
(93, 54, 8, 100, 5, 4),
(94, 55, 8, 100, 5, 4),
(95, 56, 2, 25, 5, 0),
(96, 57, 5, 63, 5, 0),
(97, 58, 6, 75, 5, 0),
(98, 59, 13, 100, 5, 0),
(99, 60, 12, 75, 5, 0),
(100, 61, 6, 75, 5, 0),
(101, 62, 17, 100, 5, 0),
(102, 63, 19, 67, 5, 0),
(103, 64, 23, 75, 5, 0),
(104, 65, 27, 75, 5, 0),
(105, 42, 1, 13, 5, 6),
(106, 43, 1, 13, 5, 6),
(107, 44, 1, 13, 5, 6),
(108, 45, 1, 13, 5, 6),
(109, 46, 1, 13, 5, 6),
(110, 47, 1, 13, 5, 6),
(111, 48, 1, 13, 5, 6),
(112, 49, 1, 13, 5, 6),
(113, 50, 1, 13, 5, 6),
(114, 51, 1, 13, 5, 6),
(115, 52, 1, 13, 5, 6),
(116, 53, 1, 13, 5, 6),
(117, 54, 1, 13, 5, 6),
(118, 55, 1, 13, 5, 6),
(119, 42, 2, 25, 5, 3),
(120, 43, 4, 50, 5, 3),
(121, 44, 8, 100, 5, 3),
(122, 45, 8, 100, 5, 3),
(123, 46, 8, 100, 5, 3),
(124, 47, 8, 100, 5, 3),
(125, 48, 8, 100, 5, 3),
(126, 49, 8, 100, 5, 3),
(127, 50, 8, 100, 5, 3),
(128, 51, 8, 100, 5, 3),
(129, 52, 8, 100, 5, 3),
(130, 53, 8, 100, 5, 3),
(131, 54, 8, 100, 5, 3),
(132, 55, 8, 100, 5, 3),
(133, 56, 5, 63, 5, 3),
(134, 57, 7, 88, 5, 3),
(135, 58, 8, 100, 5, 3),
(136, 59, 13, 100, 5, 3),
(137, 60, 12, 75, 5, 3),
(138, 61, 5, 63, 5, 3),
(139, 62, 16, 75, 5, 3),
(140, 63, 20, 100, 5, 3),
(141, 64, 23, 75, 5, 3),
(142, 65, 26, 50, 5, 3),
(143, 66, 4, 50, 5, 3),
(144, 67, 33, 100, 5, 3),
(145, 68, 5, 63, 5, 3),
(146, 69, 32, 80, 5, 3),
(147, 70, 3, 38, 5, 3),
(148, 71, 33, 100, 5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `facteur`
--

CREATE TABLE `facteur` (
  `facteur_id` int(11) NOT NULL,
  `facteur_name` varchar(250) NOT NULL,
  `facteur_importance` int(11) NOT NULL,
  `family_rps_id` int(11) NOT NULL,
  `family_description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `facteur`
--

INSERT INTO `facteur` (`facteur_id`, `facteur_name`, `facteur_importance`, `family_rps_id`, `family_description`) VALUES
(1, 'Quantité de travail', 5, 1, ''),
(2, 'Pressions temporelle', 5, 1, ''),
(3, 'Charge mentale', 5, 1, '                                                                   Desscription...                         \r\n                                                 \r\n                        '),
(4, 'Conciliation vie pro/perso', 5, 1, '                                   Desscription...                         \r\n                        '),
(5, 'Situations de souffrance / de détresse', 7, 2, ''),
(6, 'Tensions avec usagers /externes', 3, 2, ''),
(7, 'Clarté des rôles', 4, 3, ''),
(8, 'Moyens de travail', 2, 3, ''),
(9, 'Conflits de vaeurs', 3, 3, ''),
(10, 'Autonomie', 7, 3, ''),
(11, 'Soutien des collègues', 6, 4, ''),
(12, 'Soutien de la hiérarchie', 5, 4, ''),
(13, 'Reconnaissance et équité perçue', 3, 4, '                                   Desscription...                         \r\n                        '),
(14, 'Communication', 7, 5, ''),
(15, 'Accompagnement du changement', 8, 5, ''),
(16, 'Incertitude par rapport à l\'avenir', 6, 5, '');

-- --------------------------------------------------------

--
-- Structure de la table `family`
--

CREATE TABLE `family` (
  `family_id` int(11) NOT NULL,
  `family_name` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `family`
--

INSERT INTO `family` (`family_id`, `family_name`) VALUES
(11, 'ABSENTEISME'),
(12, 'GPEC'),
(13, 'HANDICAP'),
(14, 'ACTIVITES');

-- --------------------------------------------------------

--
-- Structure de la table `family_rps`
--

CREATE TABLE `family_rps` (
  `family_id` int(11) NOT NULL,
  `family_rps_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `family_rps`
--

INSERT INTO `family_rps` (`family_id`, `family_rps_name`) VALUES
(1, 'Exigence du travail'),
(2, 'Exigences émotionnelles'),
(3, 'Qualité du travail'),
(4, 'Rapports sociaux'),
(5, 'Communication & transformations');

-- --------------------------------------------------------

--
-- Structure de la table `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `file` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `indicators`
--

CREATE TABLE `indicators` (
  `id_indicator` int(11) NOT NULL,
  `id_perimetre` int(11) NOT NULL,
  `family_id` int(11) NOT NULL,
  `determinant_id` int(11) NOT NULL,
  `year3` int(11) NOT NULL,
  `year2` int(11) NOT NULL,
  `year1` int(11) NOT NULL,
  `trend_index` int(11) NOT NULL,
  `date_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `indicators`
--

INSERT INTO `indicators` (`id_indicator`, `id_perimetre`, `family_id`, `determinant_id`, `year3`, `year2`, `year1`, `trend_index`, `date_insert`) VALUES
(1, 4, 11, 27, 0, 1, 8, 4, '2018-11-21 07:53:57'),
(2, 4, 11, 28, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(3, 4, 11, 29, 0, 4, 1, 1, '2018-11-21 07:53:57'),
(4, 4, 11, 30, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(5, 4, 11, 31, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(6, 4, 11, 32, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(7, 4, 11, 33, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(8, 4, 12, 34, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(9, 4, 12, 35, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(10, 4, 12, 36, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(11, 4, 12, 37, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(12, 4, 12, 38, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(13, 4, 13, 39, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(14, 4, 13, 40, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(15, 4, 13, 41, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(16, 4, 14, 42, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(17, 4, 14, 43, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(18, 4, 14, 44, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(19, 4, 18, 50, 0, 0, 0, 0, '2018-11-21 07:53:57'),
(20, 4, 11, 27, 0, 5, 8, 1, '2018-11-21 07:59:18'),
(21, 4, 11, 28, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(22, 4, 11, 29, 0, 4, 1, 1, '2018-11-21 07:59:18'),
(23, 4, 11, 30, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(24, 4, 11, 31, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(25, 4, 11, 32, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(26, 4, 11, 33, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(27, 4, 12, 34, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(28, 4, 12, 35, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(29, 4, 12, 36, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(30, 4, 12, 37, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(31, 4, 12, 38, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(32, 4, 13, 39, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(33, 4, 13, 40, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(34, 4, 13, 41, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(35, 4, 14, 42, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(36, 4, 14, 43, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(37, 4, 14, 44, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(38, 4, 18, 50, 0, 0, 0, 0, '2018-11-21 07:59:18'),
(39, 8, 11, 27, 0, 5, 8, 1, '2018-11-22 07:54:24'),
(40, 8, 11, 28, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(41, 8, 11, 29, 0, 4, 1, 1, '2018-11-22 07:54:24'),
(42, 8, 11, 30, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(43, 8, 11, 31, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(44, 8, 11, 32, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(45, 8, 11, 33, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(46, 8, 12, 34, 2, 0, 0, 1, '2018-11-22 07:54:24'),
(47, 8, 12, 35, 4, 0, 4, 4, '2018-11-22 07:54:24'),
(48, 8, 12, 36, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(49, 8, 12, 37, 0, 4, 4, 1, '2018-11-22 07:54:24'),
(50, 8, 12, 38, 4, 0, 0, 4, '2018-11-22 07:54:24'),
(51, 8, 13, 39, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(52, 8, 13, 40, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(53, 8, 13, 41, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(54, 8, 14, 42, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(55, 8, 14, 43, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(56, 8, 14, 44, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(57, 8, 18, 50, 0, 0, 0, 0, '2018-11-22 07:54:24'),
(58, 17, 11, 27, 0, 5, 8, 1, '2018-11-22 11:03:52'),
(59, 17, 11, 28, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(60, 17, 11, 29, 0, 4, 1, 1, '2018-11-22 11:03:52'),
(61, 17, 11, 30, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(62, 17, 11, 31, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(63, 17, 11, 32, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(64, 17, 11, 33, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(65, 17, 12, 34, 2, 0, 0, 1, '2018-11-22 11:03:52'),
(66, 17, 12, 35, 4, 0, 4, 4, '2018-11-22 11:03:52'),
(67, 17, 12, 36, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(68, 17, 12, 37, 0, 4, 4, 1, '2018-11-22 11:03:52'),
(69, 17, 12, 38, 4, 0, 0, 4, '2018-11-22 11:03:52'),
(70, 17, 13, 39, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(71, 17, 13, 40, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(72, 17, 13, 41, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(73, 17, 14, 42, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(74, 17, 14, 43, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(75, 17, 14, 44, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(76, 17, 18, 50, 0, 0, 0, 0, '2018-11-22 11:03:52'),
(77, 55, 11, 27, 0, 5, 8, 1, '2019-02-05 12:42:49'),
(78, 55, 11, 28, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(79, 55, 11, 29, 0, 4, 1, 1, '2019-02-05 12:42:49'),
(80, 55, 11, 30, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(81, 55, 11, 31, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(82, 55, 11, 32, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(83, 55, 11, 33, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(84, 55, 12, 34, 2, 0, 0, 1, '2019-02-05 12:42:49'),
(85, 55, 12, 35, 4, 0, 4, 4, '2019-02-05 12:42:49'),
(86, 55, 12, 36, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(87, 55, 12, 37, 0, 4, 4, 1, '2019-02-05 12:42:49'),
(88, 55, 12, 38, 4, 0, 0, 4, '2019-02-05 12:42:49'),
(89, 55, 13, 39, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(90, 55, 13, 40, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(91, 55, 13, 41, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(92, 55, 14, 42, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(93, 55, 14, 43, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(94, 55, 14, 44, 0, 0, 0, 0, '2019-02-05 12:42:49'),
(95, 7, 11, 27, 0, 5, 8, 1, '2019-02-06 09:25:42'),
(96, 7, 11, 28, 10, 50, 3, 4, '2019-02-06 09:25:42'),
(97, 7, 11, 29, 4, 4, 4, 1, '2019-02-06 09:25:42'),
(98, 7, 11, 30, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(99, 7, 11, 31, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(100, 7, 11, 32, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(101, 7, 11, 33, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(102, 7, 12, 34, 2, 0, 0, 1, '2019-02-06 09:25:42'),
(103, 7, 12, 35, 4, 0, 4, 4, '2019-02-06 09:25:42'),
(104, 7, 12, 36, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(105, 7, 12, 37, 0, 4, 4, 1, '2019-02-06 09:25:42'),
(106, 7, 12, 38, 4, 0, 0, 4, '2019-02-06 09:25:42'),
(107, 7, 13, 39, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(108, 7, 13, 40, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(109, 7, 13, 41, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(110, 7, 14, 42, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(111, 7, 14, 43, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(112, 7, 14, 44, 0, 0, 0, 0, '2019-02-06 09:25:42'),
(113, 7, 11, 27, 0, 5, 50, 4, '2019-02-06 09:26:30'),
(114, 7, 11, 28, 10, 50, 10, 4, '2019-02-06 09:26:30'),
(115, 7, 11, 29, 4, 4, 5, 1, '2019-02-06 09:26:30'),
(116, 7, 11, 30, 40, 100, 42, 1, '2019-02-06 09:26:30'),
(117, 7, 11, 31, 1, 0, 0, 1, '2019-02-06 09:26:30'),
(118, 7, 11, 32, 10, 0, 5, 0, '2019-02-06 09:26:30'),
(119, 7, 11, 33, 50, 5, 0, 1, '2019-02-06 09:26:30'),
(120, 7, 12, 34, 2, 0, 0, 1, '2019-02-06 09:26:30'),
(121, 7, 12, 35, 4, 0, 4, 4, '2019-02-06 09:26:30'),
(122, 7, 12, 36, 0, 0, 0, 0, '2019-02-06 09:26:30'),
(123, 7, 12, 37, 0, 4, 4, 1, '2019-02-06 09:26:30'),
(124, 7, 12, 38, 4, 0, 0, 4, '2019-02-06 09:26:30'),
(125, 7, 13, 39, 0, 0, 0, 0, '2019-02-06 09:26:30'),
(126, 7, 13, 40, 0, 0, 0, 0, '2019-02-06 09:26:30'),
(127, 7, 13, 41, 0, 0, 0, 0, '2019-02-06 09:26:30'),
(128, 7, 14, 42, 0, 0, 0, 0, '2019-02-06 09:26:30'),
(129, 7, 14, 43, 0, 0, 0, 0, '2019-02-06 09:26:30'),
(130, 7, 14, 44, 0, 0, 0, 0, '2019-02-06 09:26:30');

-- --------------------------------------------------------

--
-- Structure de la table `listing`
--

CREATE TABLE `listing` (
  `listing_id` int(11) NOT NULL,
  `listing_name` varchar(250) NOT NULL,
  `listing_score` int(11) NOT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `listing`
--

INSERT INTO `listing` (`listing_id`, `listing_name`, `listing_score`, `deleted`) VALUES
(1, 'Le dispositif n\'existe pas', 1, 0),
(2, 'Certains acteurs agissent d\'une façon isolée, sans réelle formalisation', 2, 0),
(3, 'Certains acteurs ont développé quelques pratiques formalisées', 3, 0),
(4, 'Le dispositif a fait l\'objet d\'une réflexion / discussion centrale', 4, 0),
(5, 'Le dispositif a été actés, formalisé et communiqué ', 5, 0),
(6, 'Le dispositif est formalisé et connu de façon homogène par ses pilotes et ses bénificaires ', 6, 0),
(7, 'Le dispositif est déclanché at appliqué de façon systématique', 7, 0),
(8, 'Le dispositif est systématisé et inscrit dans un processus d\'amélioration continue', 8, 0),
(10, 'Pas de règles collectives en la matière', 1, 0),
(11, 'L\'encadrement dispose de quelques directives', 2, 0),
(12, 'L\'encadrement dispose d\'outils et de relais internes clairs', 3, 0),
(13, 'les outils et dipositifs sont systématisés et l\'encadrement professionnalisé', 4, 0),
(14, 'Les salariés n\'ont qu\'une connaissance très vague du rôle des IRM', 1, 0),
(15, 'Les salariés connaissent leurs interlocuteurs locaux', 2, 0),
(16, 'Les salariés ont une idée relative du rôle des IRP', 3, 0),
(17, 'Les salariés ont une idée précide du rôle t des modalités de saisine', 4, 0),
(18, 'Perception négatibe homogène', 1, 0),
(19, 'Perception hétérogène ', 2, 0),
(20, 'Perception positive homogène', 3, 0),
(21, 'Dialogue social pauvre/ limité', 1, 0),
(22, 'Dialogue social répondant aux standars légaux', 2, 0),
(23, 'Dialogue social riche donnat lieu à des accords spécifiques ou des chantiers paritaires', 3, 0),
(24, 'Travail paritaire systématique', 4, 0),
(25, 'Insuffisant', 1, 0),
(26, 'Moyen', 2, 0),
(27, 'Satisfaisant', 3, 0),
(28, 'Exemplaire', 4, 0),
(29, 'Il n\'existe pas de réel schéma de pilotage', 1, 0),
(30, 'Le pilotage tient à quelques acteurs ', 2, 0),
(31, 'Le pilotage est assuré par le CHSCT', 3, 0),
(32, 'Le pilotage est assuré par le CHSCT et associe à l\'encadrement', 4, 0),
(33, 'Le pilotage est assuré par le CHSCT et associe à l\'encadrement et les salariés', 5, 0),
(35, 'test cotation 2', 2, 1),
(36, 'a', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `icone` varchar(250) DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `modules`
--

INSERT INTO `modules` (`module_id`, `module_name`, `link`, `icone`, `sort`) VALUES
(1, 'Accueil', 'dashboard', 'Accueil_W', 1),
(2, 'Gérer les utilisateurs', 'users', 'Gerer_les_utilisateurs_W', 2),
(3, 'Définir les périmètres', 'perimetre', 'Definir_les_perimetres_W', 3),
(4, 'Paramétrer la méthode d\'évaluation', 'evaluation', 'Parametrer_la_methode_evaluation_W', 4),
(5, 'Paramétrer la période d\'évaluation', 'periode', 'Periode_W', 5),
(6, 'Saisir les indicateurs', 'indicateurs', 'Saisir_les_indicateurs_W', 6),
(7, 'Evaluer les RPS', 'rps_evaluation', 'Evaluer_les_RPS_W', 7),
(8, 'Résultats', 'resultat', 'Resultats_W', 8),
(9, 'Plan d\'actions', 'action', 'Plan_d\'actions_W', 9),
(10, 'Ressources : uploader', 'Outil', 'Ressources_W', 10);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`) VALUES
(13, 'sassi.elhem@gmail.com', 'cJmo8mBCb2vGiVjTVNYe6Lc34faHv3LRpA+MaqEfg='),
(14, 'superadmin@gmail.com', 'lR9CIwKTw+kBY2ytUMcbsGZOiPruJXI2RxxAlcy9JqI=');

-- --------------------------------------------------------

--
-- Structure de la table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `libelle` varchar(250) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `periode`
--

INSERT INTO `periode` (`id`, `libelle`, `active`) VALUES
(1, 'Periode 1', 0),
(2, 'Priode 2', 0),
(3, 'Periode 2', 0),
(4, 'Test', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pole`
--

CREATE TABLE `pole` (
  `id_pole` int(11) NOT NULL,
  `pole_nom` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pole`
--

INSERT INTO `pole` (`id_pole`, `pole_nom`) VALUES
(4, 'Pôle Adulte'),
(5, 'Pôle Enfance'),
(9, 'Pôle Travail'),
(19, '');

-- --------------------------------------------------------

--
-- Structure de la table `priority_risks`
--

CREATE TABLE `priority_risks` (
  `id_priority_risks` int(11) NOT NULL,
  `name_priority_risks` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `priority_risks`
--

INSERT INTO `priority_risks` (`id_priority_risks`, `name_priority_risks`) VALUES
(1, 'test risque'),
(2, 'test risque 2'),
(4, 'my risque'),
(5, 'Charge mentale'),
(6, 'Charge mentale');

-- --------------------------------------------------------

--
-- Structure de la table `qualitative_evaluation`
--

CREATE TABLE `qualitative_evaluation` (
  `id_qualitative_eval` int(11) NOT NULL,
  `id_rps_evaluation` int(11) NOT NULL,
  `id_priority_risks` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `qualitative_evaluation`
--

INSERT INTO `qualitative_evaluation` (`id_qualitative_eval`, `id_rps_evaluation`, `id_priority_risks`) VALUES
(1, 5, 1),
(2, 5, 2),
(4, 10, 4),
(5, 27, 5),
(6, 25, 6);

-- --------------------------------------------------------

--
-- Structure de la table `quantitative_evaluation`
--

CREATE TABLE `quantitative_evaluation` (
  `id_quantitative_eval` int(11) NOT NULL,
  `rps_evaluation_id` int(11) NOT NULL,
  `facteur_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `intensity` int(2) NOT NULL,
  `priority` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `quantitative_evaluation`
--

INSERT INTO `quantitative_evaluation` (`id_quantitative_eval`, `rps_evaluation_id`, `facteur_id`, `description`, `intensity`, `priority`) VALUES
(95, 5, 15, '', 1, 0),
(94, 5, 14, '', 1, 0),
(93, 5, 13, '', 1, 0),
(92, 5, 12, '', 1, 0),
(91, 5, 11, '', 1, 0),
(90, 5, 10, '', 1, 0),
(89, 5, 9, '', 1, 0),
(88, 5, 8, '', 1, 0),
(87, 5, 7, '', 1, 0),
(86, 5, 6, '', 1, 0),
(85, 5, 5, '', 1, 0),
(84, 5, 4, 'test', 4, 0),
(82, 5, 2, '', 1, 0),
(83, 5, 3, '', 1, 0),
(81, 5, 1, 'hello', 3, 1),
(78, 7, 14, '', 1, 0),
(77, 7, 13, '', 1, 0),
(76, 7, 12, '', 1, 0),
(75, 7, 11, '', 1, 0),
(74, 7, 10, '', 1, 0),
(73, 7, 9, '', 1, 0),
(72, 7, 8, '', 1, 0),
(71, 7, 7, '', 1, 0),
(70, 7, 6, '', 1, 1),
(69, 7, 5, '', 3, 0),
(68, 7, 4, '', 1, 0),
(67, 7, 3, '', 1, 1),
(66, 7, 2, '', 1, 0),
(65, 7, 1, '', 3, 0),
(79, 7, 15, '', 1, 0),
(80, 7, 16, '', 1, 0),
(96, 5, 16, '', 1, 0),
(97, 9, 1, '', 3, 1),
(98, 9, 2, '', 2, 1),
(99, 9, 3, '', 4, 0),
(100, 9, 4, '', 2, 1),
(101, 9, 5, '', 1, 0),
(102, 9, 6, '', 1, 0),
(103, 9, 7, '', 1, 0),
(104, 9, 8, '', 1, 0),
(105, 9, 9, '', 1, 0),
(106, 9, 10, '', 1, 0),
(107, 9, 11, '', 1, 0),
(108, 9, 12, '', 1, 0),
(109, 9, 13, '', 1, 0),
(110, 9, 14, '', 1, 0),
(111, 9, 15, '', 1, 0),
(112, 9, 16, '', 1, 0),
(113, 10, 1, 'test desc', 3, 1),
(114, 10, 2, '', 2, 1),
(115, 10, 3, '', 1, 0),
(116, 10, 4, '', 1, 1),
(117, 10, 5, '', 1, 0),
(118, 10, 6, '', 1, 0),
(119, 10, 7, '', 1, 0),
(120, 10, 8, '', 1, 0),
(121, 10, 9, '', 1, 0),
(122, 10, 10, '', 1, 0),
(123, 10, 11, '', 1, 0),
(124, 10, 12, '', 1, 0),
(125, 10, 13, '', 1, 0),
(126, 10, 14, '', 1, 0),
(127, 10, 15, '', 1, 0),
(128, 10, 16, '', 1, 0),
(129, 25, 1, 'Test de situations de travail  qui pourrait subvenir... quelle est la visibilité ??', 3, 0),
(130, 25, 2, '', 3, 0),
(131, 25, 3, '', 4, 1),
(132, 25, 4, '', 2, 0),
(133, 25, 5, '', 2, 1),
(134, 25, 6, '', 1, 0),
(135, 25, 7, '', 1, 0),
(136, 25, 8, '', 1, 0),
(137, 25, 9, '', 1, 0),
(138, 25, 10, '', 1, 0),
(139, 25, 11, '', 1, 0),
(140, 25, 12, '', 1, 0),
(141, 25, 13, '', 1, 0),
(142, 25, 14, '', 1, 0),
(143, 25, 15, '', 1, 0),
(144, 25, 16, '', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `qvt_role`
--

CREATE TABLE `qvt_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `role_val` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `qvt_role`
--

INSERT INTO `qvt_role` (`role_id`, `role_name`, `role_val`) VALUES
(1, 'Super Administrateur', 1),
(2, 'Administrateur', 2),
(3, 'Évaluateur', 3);

-- --------------------------------------------------------

--
-- Structure de la table `qvt_user`
--

CREATE TABLE `qvt_user` (
  `user_id` int(11) NOT NULL,
  `user_firstname` varchar(60) NOT NULL,
  `user_lastname` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `user_role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `qvt_user`
--

INSERT INTO `qvt_user` (`user_id`, `user_firstname`, `user_lastname`, `user_email`, `user_password`, `active`, `user_role`) VALUES
(1, 'Chef', 'Pole', 'chefPole2@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 1, 3),
(5, 'Laurent', 'ADCONSEIL', 'superadmin@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1, 1),
(6, 'chefPollllllllllllll', 'chefPole2', 'chefPole2@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0, 3),
(7, 'test admin', 'test', 'sassi.elhem@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1, 2),
(9, 'test', 'test', 'test@gmail.com', 'c3add7b94781ee70ec7c817c79f7b7bd', 1, 1),
(10, 'Mohamed Ali', 'Bougharriou', 'mohamedali@directbox.com', '25f9e794323b453885f5181f1b624d0b', 1, 1),
(11, 'Nahed', 'BMN', 'nahed@test.fr', 'd41d8cd98f00b204e9800998ecf8427e', 1, 1),
(15, 'test32', 'test33', 'superadmin2@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 1, 1),
(16, 'Laurent', 'Michaut', 'laurent_michaut@yahoo.fr', 'f31679f536486b9888db9760e3392348', 0, 3),
(17, 'Animateurs', 'MAS Le Manoir', 'animateurDUERP@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 1, 3),
(18, 'ADCONSEIL', 'ADCONSEIL', 'ADCONSEIL@gmail.com', '862877a8336a39884409d0d6f45b0019', 1, 1),
(19, 'Animateur', 'MAS', 'animateurMAS@gmail.com', '60c062c72f8ff33ff8b3e199384f9d84', 1, 3),
(20, 'Ligue Havraise', 'Administrateur', 'adminLH@gmail.com', '6c9cdce9f6d927cea4c621b33ca05013', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `rps_evaluation`
--

CREATE TABLE `rps_evaluation` (
  `id_rps_evaluation` int(11) NOT NULL,
  `pole_id` int(11) NOT NULL,
  `establishment_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `evaluation_date` int(11) NOT NULL,
  `Evaluator` varchar(255) NOT NULL,
  `salaried` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rps_evaluation`
--

INSERT INTO `rps_evaluation` (`id_rps_evaluation`, `pole_id`, `establishment_id`, `unit_id`, `evaluation_date`, `Evaluator`, `salaried`) VALUES
(1, 4, 5, 2, 1, 'sqxq', 'qsx'),
(2, 4, 5, 2, 1, 'test eva', 'test sal'),
(3, 4, 6, 7, 2, '', ''),
(4, 4, 6, 7, 3, 'sqxq', 'test sal'),
(5, 4, 5, 2, 3, 'test eva', 'test sal'),
(6, 4, 6, 7, 3, 'aa', 'aa'),
(7, 5, 8, 1, 3, 'tedt', 'ttt'),
(8, 4, 5, 2, 3, 'efg', 'df'),
(9, 6, 16, 13, 3, 'test eva', 'df'),
(10, 4, 4, 9, 3, 'ss', 'ss'),
(11, 6, 16, 13, 3, 'qQ', ''),
(12, 4, 4, 9, 0, '', ''),
(13, 4, 4, 9, 3, 'Mohamed ALi Bougharriou', 'test 1 2 3 4 5'),
(14, 4, 4, 9, 0, 'FF', 'gg'),
(15, 5, 10, 4, 3, '', ''),
(16, 4, 4, 9, 3, 'cc', 'cc'),
(17, 4, 4, 9, 3, 'FF', 'vv'),
(18, 4, 5, 2, 3, 'FF', 'vv'),
(19, 4, 5, 2, 3, 'FF', 'vv'),
(20, 4, 4, 9, 3, 'FF', 'vv'),
(21, 5, 8, 11, 3, 'FF', 'vv'),
(22, 4, 109, 16, 3, '', ''),
(23, 4, 109, 19, 3, 'Corinne et Françoise', ''),
(24, 4, 109, 19, 3, '', ''),
(25, 4, 109, 14, 3, '', ''),
(26, 4, 109, 14, 3, '', ''),
(27, 4, 109, 15, 3, '', ''),
(28, 4, 109, 14, 0, '', ''),
(29, 4, 109, 14, 0, '', ''),
(30, 4, 7, 20, 0, '', ''),
(31, 4, 109, 14, 0, '', ''),
(32, 4, 109, 17, 0, '', ''),
(33, 4, 7, 20, 0, '', ''),
(34, 5, 9, 21, 3, '', ''),
(35, 4, 7, 20, 3, '', ''),
(36, 5, 9, 21, 0, '', ''),
(37, 4, 7, 20, 0, '', ''),
(38, 4, 7, 20, 3, '', ''),
(39, 4, 7, 20, 3, '', ''),
(40, 4, 7, 20, 4, '', ''),
(41, 4, 7, 20, 4, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `unite`
--

CREATE TABLE `unite` (
  `id_unite` int(11) NOT NULL,
  `unite_nom` varchar(11) NOT NULL,
  `perimetre_id` int(11) NOT NULL,
  `pole_id` int(11) NOT NULL,
  `lieux` varchar(250) NOT NULL,
  `nombre_personne` int(11) NOT NULL,
  `fonction` varchar(250) NOT NULL,
  `commantaire` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `unite`
--

INSERT INTO `unite` (`id_unite`, `unite_nom`, `perimetre_id`, `pole_id`, `lieux`, `nombre_personne`, `fonction`, `commantaire`) VALUES
(1, 'unit1', 8, 5, '', 0, '', ''),
(2, 'unit2n', 5, 4, '', 0, '', ''),
(3, 'unit4', 5, 4, '', 0, '', ''),
(4, 'test', 10, 5, '', 0, '', ''),
(6, 'new unit', 10, 5, '', 1, '', ''),
(7, 'unittest1', 6, 4, 'test@test.tr', 22, 'test', 'teeeet'),
(8, 'unit test 2', 111, 15, 'Paris', 2, 'test', 'asazsd'),
(9, 'Med ali', 4, 4, 'Paris', 1, 'test', 'ttt'),
(10, 'unit de tes', 112, 16, 'Paris', 22, 'test', ''),
(11, 'unit pole 2', 8, 5, 'Paris', 22, 'test', ''),
(12, 'unit2 test ', 27, 7, 'Paris', 22, 'test', ''),
(13, 'unittest568', 16, 6, 'Paris', 22, 'test', ''),
(14, 'Maintenance', 109, 4, '', 7, '', ''),
(15, 'Maintenance', 109, 4, '', 7, '', ''),
(16, 'Travail de ', 109, 4, '', 7, '', ''),
(17, 'Professionn', 109, 4, '', 55, '', ''),
(18, 'Administrat', 109, 4, '', 3, '', ''),
(19, 'Encadrant', 109, 4, '', 0, '4', ''),
(20, 'Encadrant', 7, 4, '', 0, '', ''),
(21, ' nio df ois', 9, 5, 'sd', 0, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `working_situations`
--

CREATE TABLE `working_situations` (
  `id_working_situations` int(11) NOT NULL,
  `name_working_situations` varchar(255) NOT NULL,
  `id_priority_risks` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `working_situations`
--

INSERT INTO `working_situations` (`id_working_situations`, `name_working_situations`, `id_priority_risks`) VALUES
(1, 'hello world', 1),
(2, 'rtrt', 2),
(3, 'my test 1dd', 2),
(4, 'Gestion des usagers', 5);

-- --------------------------------------------------------

--
-- Structure de la table `work_areas`
--

CREATE TABLE `work_areas` (
  `id_work_areas` int(11) NOT NULL,
  `work_areas_name` varchar(255) NOT NULL,
  `id_working_situations` int(11) NOT NULL,
  `id_priority_risks` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `work_areas`
--

INSERT INTO `work_areas` (`id_work_areas`, `work_areas_name`, `id_working_situations`, `id_priority_risks`) VALUES
(1, 'sss', 2, 1),
(2, 'qsqsqs', 1, 2),
(3, 'nahed d', 2, 2),
(4, 'Définir un scénarii en mode dégradé', 4, 5);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`access_id`),
  ADD KEY `FK_role` (`role_id`),
  ADD KEY `FK_module` (`module_id`);

--
-- Index pour la table `correlation`
--
ALTER TABLE `correlation`
  ADD PRIMARY KEY (`correlation_id`);

--
-- Index pour la table `determinant`
--
ALTER TABLE `determinant`
  ADD PRIMARY KEY (`determinant_id`),
  ADD KEY `family_id` (`family_id`);

--
-- Index pour la table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`device_id`),
  ADD KEY `determinant_id` (`determinant_id`);

--
-- Index pour la table `device_has_listing`
--
ALTER TABLE `device_has_listing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_listing_id` (`listing_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Index pour la table `establishment`
--
ALTER TABLE `establishment`
  ADD PRIMARY KEY (`id_perimetre`);

--
-- Index pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`evaluation_id`);

--
-- Index pour la table `facteur`
--
ALTER TABLE `facteur`
  ADD PRIMARY KEY (`facteur_id`);

--
-- Index pour la table `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`family_id`);

--
-- Index pour la table `family_rps`
--
ALTER TABLE `family_rps`
  ADD PRIMARY KEY (`family_id`);

--
-- Index pour la table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `indicators`
--
ALTER TABLE `indicators`
  ADD PRIMARY KEY (`id_indicator`);

--
-- Index pour la table `listing`
--
ALTER TABLE `listing`
  ADD PRIMARY KEY (`listing_id`),
  ADD KEY `device_id` (`listing_score`);

--
-- Index pour la table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pole`
--
ALTER TABLE `pole`
  ADD PRIMARY KEY (`id_pole`);

--
-- Index pour la table `priority_risks`
--
ALTER TABLE `priority_risks`
  ADD PRIMARY KEY (`id_priority_risks`);

--
-- Index pour la table `qualitative_evaluation`
--
ALTER TABLE `qualitative_evaluation`
  ADD PRIMARY KEY (`id_qualitative_eval`);

--
-- Index pour la table `quantitative_evaluation`
--
ALTER TABLE `quantitative_evaluation`
  ADD PRIMARY KEY (`id_quantitative_eval`),
  ADD UNIQUE KEY `id_qualitative_eval` (`id_quantitative_eval`);

--
-- Index pour la table `qvt_role`
--
ALTER TABLE `qvt_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Index pour la table `qvt_user`
--
ALTER TABLE `qvt_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_role` (`user_role`);

--
-- Index pour la table `rps_evaluation`
--
ALTER TABLE `rps_evaluation`
  ADD PRIMARY KEY (`id_rps_evaluation`);

--
-- Index pour la table `unite`
--
ALTER TABLE `unite`
  ADD PRIMARY KEY (`id_unite`),
  ADD UNIQUE KEY `pole_id` (`id_unite`);

--
-- Index pour la table `working_situations`
--
ALTER TABLE `working_situations`
  ADD PRIMARY KEY (`id_working_situations`);

--
-- Index pour la table `work_areas`
--
ALTER TABLE `work_areas`
  ADD PRIMARY KEY (`id_work_areas`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `access`
--
ALTER TABLE `access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `correlation`
--
ALTER TABLE `correlation`
  MODIFY `correlation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT pour la table `determinant`
--
ALTER TABLE `determinant`
  MODIFY `determinant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT pour la table `device`
--
ALTER TABLE `device`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT pour la table `device_has_listing`
--
ALTER TABLE `device_has_listing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT pour la table `establishment`
--
ALTER TABLE `establishment`
  MODIFY `id_perimetre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT pour la table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `evaluation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT pour la table `facteur`
--
ALTER TABLE `facteur`
  MODIFY `facteur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `family`
--
ALTER TABLE `family`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `family_rps`
--
ALTER TABLE `family_rps`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `indicators`
--
ALTER TABLE `indicators`
  MODIFY `id_indicator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT pour la table `listing`
--
ALTER TABLE `listing`
  MODIFY `listing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `pole`
--
ALTER TABLE `pole`
  MODIFY `id_pole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `priority_risks`
--
ALTER TABLE `priority_risks`
  MODIFY `id_priority_risks` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `qualitative_evaluation`
--
ALTER TABLE `qualitative_evaluation`
  MODIFY `id_qualitative_eval` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `quantitative_evaluation`
--
ALTER TABLE `quantitative_evaluation`
  MODIFY `id_quantitative_eval` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT pour la table `qvt_role`
--
ALTER TABLE `qvt_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `qvt_user`
--
ALTER TABLE `qvt_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `rps_evaluation`
--
ALTER TABLE `rps_evaluation`
  MODIFY `id_rps_evaluation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT pour la table `unite`
--
ALTER TABLE `unite`
  MODIFY `id_unite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `working_situations`
--
ALTER TABLE `working_situations`
  MODIFY `id_working_situations` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `work_areas`
--
ALTER TABLE `work_areas`
  MODIFY `id_work_areas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `qvt_user`
--
ALTER TABLE `qvt_user`
  ADD CONSTRAINT `qvt_user_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `qvt_role` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
