-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 14 Novembre 2018 à 07:00
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `liguehavraise`
--

-- --------------------------------------------------------

--
-- Structure de la table `access`
--

CREATE TABLE `access` (
  `access_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `access`
--

INSERT INTO `access` (`access_id`, `role_id`, `module_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 2, 1),
(12, 2, 2),
(13, 2, 3),
(24, 3, 11),
(15, 2, 5),
(16, 2, 6),
(17, 2, 7),
(18, 2, 8),
(19, 2, 10),
(20, 3, 1),
(21, 3, 6),
(22, 3, 7),
(23, 1, 11),
(25, 2, 9),
(26, 2, 11);

-- --------------------------------------------------------

--
-- Structure de la table `determinant`
--

CREATE TABLE `determinant` (
  `determinant_id` int(11) NOT NULL,
  `determinant_name` varchar(250) NOT NULL,
  `determinant_importance` int(11) DEFAULT '0',
  `family_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `determinant`
--

INSERT INTO `determinant` (`determinant_id`, `determinant_name`, `determinant_importance`, `family_id`) VALUES
(19, 'Favoriser l\'équité et l\'égalité', 2, 2),
(18, 'Accompagner les parcours professionnels', 2, 8),
(6, 'Prévenir les risques professionnels et les AT', 2, 3),
(17, 'Soutenir les individus et les collectifs tout au long de leur carrière', 2, 8),
(16, 'Accompagner les changements', 2, 8),
(14, 'Prévenir la pénibilité', 2, 3),
(20, 'Encourager le management éthique', 2, 2),
(21, 'Améliorer le dialogue social', 2, 2),
(22, 'Prévenir les risques psychosociaux', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `device`
--

CREATE TABLE `device` (
  `device_id` int(11) NOT NULL,
  `device_name` varchar(250) NOT NULL,
  `determinant_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `device`
--

INSERT INTO `device` (`device_id`, `device_name`, `determinant_id`) VALUES
(43, 'Dispositif d\'intégration et de professionnalisation', 18),
(42, 'Dispositif de gestion des carrières (GPEC et fin de carrière notamment)', 18),
(46, 'Dispositif de maintien dans l\'emploi des salariés en risque d\'inaptitude - situation de handicap', 17),
(44, 'Dispositif de formation continue', 18),
(45, 'Dispositif d\'évaluation des compétences', 18),
(47, 'Dispositif de maintien dans l\'emploi des salariés en situation de fragilité - de précarité', 17),
(48, 'Dispositif de maintien dans l\'emploi des salariés en situation de rupture liée aux compétences', 17),
(49, 'Dispositif de réaction immédiate aux violences exogènes et endogènes', 17),
(50, 'Dispositif de veille permanente (récolte, analyse et prévention des évènements indésirables)', 17),
(51, 'Dispositif de formation aux spécificités du public', 17),
(52, 'Dispositif de promotion de bonnes pratiques relationnelles (Ex. : charte management)', 17),
(53, 'Dispositif de concertation préalable et de co-construction.', 16),
(54, 'Dispositif de régulation en aval des changements.', 16),
(55, 'Dispositif de suivi des impacts (en CHSCT notamment)', 16),
(56, 'Dispositif et critères d\'arbitrage et de prise de décision.', 19),
(57, 'Dispositif de promotion de l\'égalité des chances.', 19),
(58, 'Dispositif de clarification des délégations de pouvoirs.', 20),
(59, 'Capacité de l\'encadrement à assurer une communication efficiente.', 20),
(60, 'Capacité de l\'encadrement à soutenir les subordonnés en cas de difficulté.', 20),
(61, 'Dispositif de professionnalisation et de régulation à la disposition des encadrants.', 20),
(62, 'Connaissance des IRP et de leur rôle par les salariés', 21),
(63, 'Regards croisés direction / IRP sur la qualité du dialogue social', 21),
(64, 'Accords signés / chantiers paritaires', 21),
(65, 'Niveau de compétence des IRP au regard des thématiques traitées', 21),
(66, 'Dispositif d\'évaluation et de prévention des risques professionnels', 6),
(67, 'Modalités de pilotage du plan annuel de prévention des risques professionnels.', 6),
(68, 'Dispositif d\'évaluation et de prévention de la pénibilité', 14),
(69, 'Modalités de pilotage du plan de prévention de la pénibilité.', 14),
(70, 'Dispositif d\'évaluation et de prévention des RPS', 22),
(71, 'Modalités de pilotage du plan de prévention des risques psychosociaux.', 22);

-- --------------------------------------------------------

--
-- Structure de la table `device_has_listing`
--

CREATE TABLE `device_has_listing` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `device_has_listing`
--

INSERT INTO `device_has_listing` (`id`, `device_id`, `listing_id`) VALUES
(81, 43, 2),
(82, 43, 3),
(83, 43, 4),
(84, 43, 5),
(85, 43, 6),
(86, 43, 7),
(87, 43, 8),
(88, 42, 1),
(89, 42, 2),
(90, 42, 3),
(91, 42, 4),
(92, 42, 5),
(93, 42, 6),
(94, 42, 7),
(95, 42, 8),
(96, 44, 1),
(97, 44, 2),
(98, 44, 3),
(99, 44, 4),
(100, 44, 5),
(101, 44, 6),
(102, 44, 7),
(103, 44, 8),
(104, 45, 1),
(105, 45, 2),
(106, 45, 3),
(107, 45, 4),
(108, 45, 5),
(109, 45, 6),
(110, 45, 7),
(111, 45, 8),
(112, 46, 1),
(113, 46, 2),
(114, 46, 3),
(115, 46, 4),
(116, 46, 5),
(117, 46, 6),
(118, 46, 7),
(119, 46, 8),
(120, 47, 1),
(121, 47, 2),
(122, 47, 3),
(123, 47, 4),
(124, 47, 5),
(125, 47, 6),
(126, 47, 7),
(127, 47, 8),
(128, 48, 1),
(129, 48, 2),
(130, 48, 3),
(131, 48, 4),
(132, 48, 5),
(133, 48, 6),
(134, 48, 7),
(135, 48, 8),
(136, 49, 1),
(137, 49, 2),
(138, 49, 3),
(139, 49, 4),
(140, 49, 5),
(141, 49, 6),
(142, 49, 7),
(143, 49, 8),
(144, 50, 1),
(145, 50, 2),
(146, 50, 3),
(147, 50, 4),
(148, 50, 5),
(149, 50, 6),
(150, 50, 7),
(151, 50, 8),
(152, 51, 1),
(153, 51, 2),
(154, 51, 3),
(155, 51, 4),
(156, 51, 5),
(157, 51, 6),
(158, 51, 7),
(159, 51, 8),
(160, 52, 1),
(161, 52, 2),
(162, 52, 3),
(163, 52, 4),
(164, 52, 5),
(165, 52, 6),
(166, 52, 7),
(167, 52, 8),
(168, 53, 1),
(169, 53, 2),
(170, 53, 3),
(171, 53, 4),
(172, 53, 5),
(173, 53, 6),
(174, 53, 7),
(175, 53, 8),
(176, 54, 1),
(177, 54, 2),
(178, 54, 3),
(179, 54, 4),
(180, 54, 5),
(181, 54, 6),
(182, 54, 7),
(183, 54, 8),
(184, 55, 1),
(185, 55, 2),
(186, 55, 3),
(187, 55, 4),
(188, 55, 5),
(189, 55, 6),
(190, 55, 7),
(191, 55, 8),
(192, 56, 1),
(193, 56, 2),
(194, 56, 3),
(195, 56, 4),
(196, 56, 5),
(197, 56, 6),
(198, 56, 7),
(199, 56, 8),
(200, 57, 1),
(201, 57, 2),
(202, 57, 3),
(203, 57, 4),
(204, 57, 5),
(205, 57, 6),
(206, 57, 7),
(207, 57, 8),
(208, 58, 1),
(209, 58, 2),
(210, 58, 3),
(211, 58, 4),
(212, 58, 5),
(213, 58, 6),
(214, 58, 7),
(215, 58, 8),
(216, 59, 10),
(217, 59, 11),
(218, 59, 12),
(219, 59, 13),
(220, 60, 10),
(221, 60, 11),
(222, 60, 12),
(223, 60, 13),
(224, 61, 1),
(225, 61, 2),
(226, 61, 3),
(227, 61, 4),
(228, 61, 5),
(229, 61, 6),
(230, 61, 7),
(231, 61, 8),
(233, 62, 14),
(234, 62, 15),
(235, 62, 16),
(236, 62, 17),
(237, 63, 18),
(238, 63, 19),
(239, 63, 20),
(240, 64, 21),
(244, 64, 23),
(243, 64, 22),
(245, 64, 24),
(246, 65, 25),
(247, 65, 26),
(248, 65, 27),
(249, 65, 28),
(250, 66, 1),
(251, 66, 2),
(252, 66, 3),
(253, 66, 4),
(254, 66, 5),
(255, 66, 6),
(256, 66, 7),
(257, 66, 8),
(258, 67, 29),
(259, 67, 30),
(260, 67, 31),
(261, 67, 32),
(262, 67, 33),
(263, 68, 1),
(264, 68, 2),
(265, 68, 3),
(266, 68, 4),
(267, 68, 5),
(268, 68, 6),
(269, 68, 7),
(270, 68, 8),
(271, 69, 29),
(272, 69, 30),
(273, 69, 31),
(274, 69, 32),
(275, 69, 33),
(276, 70, 1),
(277, 70, 2),
(278, 70, 3),
(279, 70, 4),
(280, 70, 5),
(281, 70, 6),
(282, 70, 7),
(283, 70, 8),
(284, 71, 29),
(285, 71, 30),
(286, 71, 31),
(287, 71, 32),
(288, 71, 33),
(80, 43, 1);

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

CREATE TABLE `evaluation` (
  `evaluation_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `avancement` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `id_perimetre` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `evaluation`
--

INSERT INTO `evaluation` (`evaluation_id`, `device_id`, `listing_id`, `avancement`, `user_id`, `id_perimetre`) VALUES
(60, 55, 4, 50, 1, 0),
(59, 54, 8, 100, 1, 0),
(58, 53, 4, 50, 1, 0),
(57, 52, 7, 88, 1, 0),
(56, 51, 3, 38, 1, 0),
(55, 50, 7, 88, 1, 0),
(54, 49, 4, 50, 1, 0),
(53, 48, 6, 75, 1, 0),
(52, 47, 4, 50, 1, 0),
(51, 46, 6, 75, 1, 0),
(50, 45, 3, 38, 1, 0),
(49, 44, 6, 75, 1, 0),
(48, 42, 6, 75, 1, 0),
(47, 43, 3, 38, 1, 0),
(61, 43, 3, 38, 7, 0),
(62, 42, 5, 63, 7, 0),
(63, 44, 3, 38, 7, 0),
(64, 45, 7, 88, 7, 0),
(65, 46, 4, 50, 7, 0),
(66, 47, 7, 88, 7, 0),
(67, 48, 5, 63, 7, 0),
(68, 49, 4, 50, 7, 0),
(69, 50, 8, 100, 7, 0),
(70, 51, 7, 88, 7, 0),
(71, 52, 5, 63, 7, 0),
(72, 53, 5, 63, 7, 0),
(73, 54, 2, 25, 7, 0),
(74, 55, 6, 75, 7, 0),
(75, 66, 4, 50, 5, 0),
(76, 67, 32, 80, 5, 0),
(77, 68, 3, 38, 5, 0),
(78, 69, 33, 100, 5, 0),
(79, 70, 3, 38, 5, 0),
(80, 71, 31, 60, 5, 0),
(81, 42, 8, 100, 5, 4),
(82, 43, 8, 100, 5, 4),
(83, 44, 8, 100, 5, 4),
(84, 45, 8, 100, 5, 4),
(85, 46, 8, 100, 5, 4),
(86, 47, 8, 100, 5, 4),
(87, 48, 8, 100, 5, 4),
(88, 49, 8, 100, 5, 4),
(89, 50, 8, 100, 5, 4),
(90, 51, 8, 100, 5, 4),
(91, 52, 8, 100, 5, 4),
(92, 53, 8, 100, 5, 4),
(93, 54, 8, 100, 5, 4),
(94, 55, 8, 100, 5, 4),
(95, 56, 2, 25, 5, 0),
(96, 57, 5, 63, 5, 0),
(97, 58, 6, 75, 5, 0),
(98, 59, 13, 100, 5, 0),
(99, 60, 12, 75, 5, 0),
(100, 61, 6, 75, 5, 0),
(101, 62, 17, 100, 5, 0),
(102, 63, 19, 67, 5, 0),
(103, 64, 23, 75, 5, 0),
(104, 65, 27, 75, 5, 0),
(105, 42, 1, 13, 5, 6),
(106, 43, 1, 13, 5, 6),
(107, 44, 1, 13, 5, 6),
(108, 45, 1, 13, 5, 6),
(109, 46, 1, 13, 5, 6),
(110, 47, 1, 13, 5, 6),
(111, 48, 1, 13, 5, 6),
(112, 49, 1, 13, 5, 6),
(113, 50, 1, 13, 5, 6),
(114, 51, 1, 13, 5, 6),
(115, 52, 1, 13, 5, 6),
(116, 53, 1, 13, 5, 6),
(117, 54, 1, 13, 5, 6),
(118, 55, 1, 13, 5, 6),
(119, 42, 2, 25, 5, 3),
(120, 43, 4, 50, 5, 3),
(121, 44, 8, 100, 5, 3),
(122, 45, 8, 100, 5, 3),
(123, 46, 8, 100, 5, 3),
(124, 47, 8, 100, 5, 3),
(125, 48, 8, 100, 5, 3),
(126, 49, 8, 100, 5, 3),
(127, 50, 8, 100, 5, 3),
(128, 51, 8, 100, 5, 3),
(129, 52, 8, 100, 5, 3),
(130, 53, 8, 100, 5, 3),
(131, 54, 8, 100, 5, 3),
(132, 55, 8, 100, 5, 3),
(133, 56, 5, 63, 5, 3),
(134, 57, 7, 88, 5, 3),
(135, 58, 8, 100, 5, 3),
(136, 59, 13, 100, 5, 3),
(137, 60, 12, 75, 5, 3),
(138, 61, 5, 63, 5, 3),
(139, 62, 16, 75, 5, 3),
(140, 63, 20, 100, 5, 3),
(141, 64, 23, 75, 5, 3),
(142, 65, 26, 50, 5, 3),
(143, 66, 4, 50, 5, 3),
(144, 67, 33, 100, 5, 3),
(145, 68, 5, 63, 5, 3),
(146, 69, 32, 80, 5, 3),
(147, 70, 3, 38, 5, 3),
(148, 71, 33, 100, 5, 3),
(149, 42, 3, 38, 2, 1),
(150, 43, 1, 13, 2, 1),
(151, 44, 6, 75, 2, 1),
(152, 45, 7, 88, 2, 1),
(153, 46, 7, 88, 2, 1),
(154, 47, 7, 88, 2, 1),
(155, 48, 4, 50, 2, 1),
(156, 49, 3, 38, 2, 1),
(157, 50, 5, 63, 2, 1),
(158, 51, 6, 75, 2, 1),
(159, 52, 8, 100, 2, 1),
(160, 53, 3, 38, 2, 1),
(161, 54, 5, 63, 2, 1),
(162, 55, 5, 63, 2, 1),
(163, 42, 1, 13, 1, 2),
(164, 43, 1, 13, 1, 2),
(165, 44, 1, 13, 1, 2),
(166, 45, 1, 13, 1, 2),
(167, 46, 1, 13, 1, 2),
(168, 47, 1, 13, 1, 2),
(169, 48, 1, 13, 1, 2),
(170, 49, 1, 13, 1, 2),
(171, 50, 1, 13, 1, 2),
(172, 51, 1, 13, 1, 2),
(173, 52, 1, 13, 1, 2),
(174, 53, 1, 13, 1, 2),
(175, 54, 1, 13, 1, 2),
(176, 55, 1, 13, 1, 2),
(177, 56, 1, 13, 1, 2),
(178, 57, 1, 13, 1, 2),
(179, 58, 1, 13, 1, 2),
(180, 59, 10, 25, 1, 2),
(181, 60, 10, 25, 1, 2),
(182, 61, 1, 13, 1, 2),
(183, 62, 14, 25, 1, 2),
(184, 63, 18, 33, 1, 2),
(185, 64, 21, 25, 1, 2),
(186, 65, 25, 25, 1, 2),
(187, 66, 1, 13, 1, 2),
(188, 67, 29, 20, 1, 2),
(189, 68, 1, 13, 1, 2),
(190, 69, 29, 20, 1, 2),
(191, 70, 1, 13, 1, 2),
(192, 71, 29, 20, 1, 2),
(193, 42, 1, 13, 2, 2),
(194, 43, 3, 38, 2, 2),
(195, 44, 5, 63, 2, 2),
(196, 45, 5, 63, 2, 2),
(197, 46, 5, 63, 2, 2),
(198, 47, 5, 63, 2, 2),
(199, 48, 6, 75, 2, 2),
(200, 49, 5, 63, 2, 2),
(201, 50, 5, 63, 2, 2),
(202, 51, 5, 63, 2, 2),
(203, 52, 5, 63, 2, 2),
(204, 53, 4, 50, 2, 2),
(205, 54, 6, 75, 2, 2),
(206, 55, 5, 63, 2, 2),
(207, 56, 4, 50, 2, 2),
(208, 57, 5, 63, 2, 2),
(209, 58, 4, 50, 2, 2),
(210, 59, 12, 75, 2, 2),
(211, 60, 12, 75, 2, 2),
(212, 61, 4, 50, 2, 2),
(213, 62, 14, 25, 2, 2),
(214, 63, 18, 33, 2, 2),
(215, 64, 22, 50, 2, 2),
(216, 65, 26, 50, 2, 2),
(217, 66, 7, 88, 2, 2),
(218, 67, 32, 80, 2, 2),
(219, 68, 4, 50, 2, 2),
(220, 69, 32, 80, 2, 2),
(221, 70, 4, 50, 2, 2),
(222, 71, 32, 80, 2, 2),
(223, 42, 2, 25, 2, 3),
(224, 43, 5, 63, 2, 3),
(225, 44, 6, 75, 2, 3),
(226, 45, 5, 63, 2, 3),
(227, 46, 7, 88, 2, 3),
(228, 47, 7, 88, 2, 3),
(229, 48, 8, 100, 2, 3),
(230, 49, 5, 63, 2, 3),
(231, 50, 3, 38, 2, 3),
(232, 51, 5, 63, 2, 3),
(233, 52, 5, 63, 2, 3),
(234, 53, 5, 63, 2, 3),
(235, 54, 6, 75, 2, 3),
(236, 55, 5, 63, 2, 3),
(237, 56, 2, 25, 2, 3),
(238, 57, 5, 63, 2, 3),
(239, 58, 5, 63, 2, 3),
(240, 59, 12, 75, 2, 3),
(241, 60, 12, 75, 2, 3),
(242, 61, 4, 50, 2, 3),
(243, 62, 16, 75, 2, 3),
(244, 63, 18, 33, 2, 3),
(245, 64, 22, 50, 2, 3),
(246, 65, 27, 75, 2, 3),
(247, 66, 2, 25, 2, 3),
(248, 67, 33, 100, 2, 3),
(249, 68, 3, 38, 2, 3),
(250, 69, 31, 60, 2, 3),
(251, 70, 4, 50, 2, 3),
(252, 71, 31, 60, 2, 3),
(253, 56, 3, 38, 2, 1),
(254, 57, 6, 75, 2, 1),
(255, 58, 3, 38, 2, 1),
(256, 59, 13, 100, 2, 1),
(257, 60, 11, 50, 2, 1),
(258, 61, 1, 13, 2, 1),
(259, 62, 17, 100, 2, 1),
(260, 63, 19, 67, 2, 1),
(261, 64, 21, 25, 2, 1),
(262, 65, 25, 25, 2, 1),
(263, 66, 2, 25, 2, 1),
(264, 67, 32, 80, 2, 1),
(265, 68, 2, 25, 2, 1),
(266, 69, 32, 80, 2, 1),
(267, 70, 3, 38, 2, 1),
(268, 71, 32, 80, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `family`
--

CREATE TABLE `family` (
  `family_id` int(11) NOT NULL,
  `family_name` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `family`
--

INSERT INTO `family` (`family_id`, `family_name`) VALUES
(8, '1. PRATIQUES DE SOUTIEN TOUT AU LONG DE LA CARRIERE'),
(2, '2. PRATIQUES DE RENFORCEMENT DE LA COHESION'),
(3, '3. PRATIQUES DE PRMOTION DE LA SANTE ET DE LA SECURITE');

-- --------------------------------------------------------

--
-- Structure de la table `importance`
--

CREATE TABLE `importance` (
  `id` int(11) NOT NULL,
  `valeur` int(11) NOT NULL,
  `pole_id` int(11) NOT NULL,
  `determinant_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `importance`
--

INSERT INTO `importance` (`id`, `valeur`, `pole_id`, `determinant_id`) VALUES
(66, 1, 1, 18),
(67, 1, 1, 17),
(68, 1, 1, 16),
(69, 1, 1, 19),
(70, 1, 1, 20),
(71, 1, 1, 21),
(72, 1, 1, 6),
(73, 1, 1, 14),
(74, 1, 1, 22),
(75, 1, 5, 18),
(76, 1, 5, 17),
(77, 1, 5, 16),
(78, 1, 5, 19),
(79, 4, 5, 20),
(80, 3, 5, 21),
(81, 3, 5, 6),
(82, 1, 5, 14),
(83, 1, 5, 22),
(84, 2, 2, 18),
(85, 2, 2, 17),
(86, 2, 2, 16),
(87, 2, 2, 19),
(88, 2, 2, 20),
(89, 2, 2, 21),
(90, 2, 2, 6),
(91, 2, 2, 14),
(92, 2, 2, 22),
(93, 3, 3, 18),
(94, 2, 3, 17),
(95, 2, 3, 16),
(96, 2, 3, 19),
(97, 3, 3, 20),
(98, 2, 3, 21),
(99, 2, 3, 6),
(100, 3, 3, 14),
(101, 2, 3, 22);

-- --------------------------------------------------------

--
-- Structure de la table `listing`
--

CREATE TABLE `listing` (
  `listing_id` int(11) NOT NULL,
  `listing_name` varchar(250) NOT NULL,
  `listing_score` int(11) NOT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `listing`
--

INSERT INTO `listing` (`listing_id`, `listing_name`, `listing_score`, `deleted`) VALUES
(1, 'Le dispositif n\'existe pas', 1, 0),
(2, 'Certains acteurs agissent d\'une façon isolée, sans réelle formalisation', 2, 0),
(3, 'Certains acteurs ont développé quelques pratiques formalisées', 3, 0),
(4, 'Le dispositif a fait l\'objet d\'une réflexion / discussion centrale', 4, 0),
(5, 'Le dispositif a été actés, formalisé et communiqué ', 5, 0),
(6, 'Le dispositif est formalisé et connu de façon homogène par ses pilotes et ses bénificaires ', 6, 0),
(7, 'Le dispositif est déclanché at appliqué de façon systématique', 7, 0),
(8, 'Le dispositif est systématisé et inscrit dans un processus d\'amélioration continue', 8, 0),
(10, 'Pas de règles collectives en la matière', 1, 0),
(11, 'L\'encadrement dispose de quelques directives', 2, 0),
(12, 'L\'encadrement dispose d\'outils et de relais internes clairs', 3, 0),
(13, 'les outils et dipositifs sont systématisés et l\'encadrement professionnalisé', 4, 0),
(14, 'Les salariés n\'ont qu\'une connaissance très vague du rôle des IRM', 1, 0),
(15, 'Les salariés connaissent leurs interlocuteurs locaux', 2, 0),
(16, 'Les salariés ont une idée relative du rôle des IRP', 3, 0),
(17, 'Les salariés ont une idée précide du rôle t des modalités de saisine', 4, 0),
(18, 'Perception négatibe homogène', 1, 0),
(19, 'Perception hétérogène ', 2, 0),
(20, 'Perception positive homogène', 3, 0),
(21, 'Dialogue social pauvre/ limité', 1, 0),
(22, 'Dialogue social répondant aux standars légaux', 2, 0),
(23, 'Dialogue social riche donnat lieu à des accords spécifiques ou des chantiers paritaires', 3, 0),
(24, 'Travail paritaire systématique', 4, 0),
(25, 'Insuffisant', 1, 0),
(26, 'Moyen', 2, 0),
(27, 'Satisfaisant', 3, 0),
(28, 'Exemplaire', 4, 0),
(29, 'Il n\'existe pas de réel schéma de pilotage', 1, 0),
(30, 'Le pilotage tient à quelques acteurs ', 2, 0),
(31, 'Le pilotage est assuré par le CHSCT', 3, 0),
(32, 'Le pilotage est assuré par le CHSCT et associe à l\'encadrement', 4, 0),
(33, 'Le pilotage est assuré par le CHSCT et associe à l\'encadrement et les salariés', 5, 0),
(35, 'test cotation 2', 2, 1),
(36, 'a', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `icone` varchar(250) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `rights` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `modules`
--

INSERT INTO `modules` (`module_id`, `module_name`, `link`, `icone`, `sort`, `rights`) VALUES
(1, 'définir les périmètres', 'users', 'permietres_W', 3, '1,2,3,4'),
(2, 'Paramétrer la période d’évaluation\n', 'family', 'Periode_W', 5, '1,2'),
(3, 'Evaluer les risques\n', 'evaluation', 'Evaluer_les_RPS_W', 7, '1,2,3,4'),
(4, 'Résultats', 'resultat', 'Resultats_W', 8, '1,2,3,4'),
(5, 'Plan d’actions\n', 'Outil', 'Plan_actions_W', 9, '1,2,3,4'),
(6, 'Ressources : uploader\n', 'login/logout', 'logout', 10, '1,2,3,4'),
(8, 'gérer les utilisateurs', 'users', 'gestion_utilisateurs_W', 2, '1,2,3,4'),
(7, 'Accueil', 'dashboard', 'Accueil_W', 1, '1,2,3,4'),
(9, 'Paramétrer la méthode d’évaluation\n', 'pole', 'param_eval_W', 4, '1,2,3,4'),
(10, 'Ressources : downloader\n', 'support', 'Ressources_W', 11, '1,2,3,4'),
(11, 'Saisir les indicateurs\n', 'importance', 'saisie_indicateurs_W', 6, '1,2,3,4'),
(12, 'Accueil', 'dashboard', 'Accueil_W', 1, NULL),
(13, 'Gérer les utilisateurs', 'users', 'gestion_utilisateurs_W', 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`) VALUES
(13, 'sassi.elhem@gmail.com', 'cJmo8mBCb2vGiVjTVNYe6Lc34faHv3LRpA+MaqEfg='),
(14, 'superadmin@gmail.com', 'lR9CIwKTw+kBY2ytUMcbsGZOiPruJXI2RxxAlcy9JqI=');

-- --------------------------------------------------------

--
-- Structure de la table `perimetre`
--

CREATE TABLE `perimetre` (
  `id_perimetre` int(11) NOT NULL,
  `perimetre_nom` varchar(250) NOT NULL,
  `pole_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `perimetre`
--

INSERT INTO `perimetre` (`id_perimetre`, `perimetre_nom`, `pole_id`) VALUES
(1, 'Direction Générale', 1),
(2, 'Cuisine Centrale / Cafeteria', 2),
(3, 'Mecs Lucie Aubrac', 3),
(4, 'Foyer de vie Catalpas', 4),
(5, 'Foyer de vie les Marronniers', 4),
(6, 'MAS les Marronniers', 4),
(7, 'FAM Le Tourret', 4),
(8, 'Handicapes Vieillisants Las Canneles', 5),
(9, 'Maison de retraite Las Canneles', 5),
(10, 'FAM Les Canneles', 5),
(11, 'ESAT Terres de Garonne (BPAS)', 5),
(12, 'ESAT Terres de Garonne (BAPC)', 5),
(13, 'Foyer d\'Hebergement La Glacerie', 5),
(14, 'Foyer de vie des Cèdres', 5),
(15, 'SAVS Henri Cros', 5),
(16, 'Foyer de vie de Pousinies', 6),
(17, 'Foyer d\'hébergement Pousinies', 6),
(18, 'SAVS Pousinies', 6),
(19, 'ESAT Pousinies (BPAS)', 6),
(20, 'ESAT Pousinies (BAPC)', 6),
(21, 'SAMSAH de Pousinies (dont TED)', 6),
(22, 'FAM de Bordeneuve (dont TED)', 6),
(23, 'Handicapes Vieillisants Bordeneuve ', 6),
(24, 'Maison de retraite Bordeneuve ', 6),
(25, 'IME TED (Venerque Guilhem)', 7),
(26, 'IME TED (Tlse Bruyères)', 7),
(27, 'IME TED (Pechbonnieu CAPVA)', 7),
(28, 'IME TED (Aussonne)', 7),
(29, 'IME DI Légers/Moyens (Venerque Guilhem)', 7),
(30, 'IME DI Légers/Moyens (Aussonne)', 7),
(31, 'IME DI Légers/Moyens (Pechbonnieu CAPVA)', 7),
(32, 'IME DI Moyens/Sévères (Tlse Les Bruyères)', 7),
(33, 'SESSAD IME (Venerque Guilhem)', 7),
(34, 'SESSAD IME TED (Tlse sud Les Bruyères)', 7),
(35, 'SESSAD CAP Midi-Pyrénées (65 Tarbes - SAIDEDA)', 7),
(36, 'SESSAD CAP Midi-Pyrénées (32 Auch - SAIDA)', 7),
(37, 'SESSAD CAP Midi-Pyrénées (31 Tlse)', 7),
(38, 'SESSAD CAP Midi-Pyrénées (09 Pamiers)', 7),
(39, 'SESSAD CAP Midi-Pyrénées (82 Montauban)', 7),
(40, 'SESSAD ITEP PRG (Colomiers)', 8),
(41, 'SESSAD ITEP PRG (Aucamville)', 8),
(42, 'SESSAD ITEP PRG (Tlse sud)', 8),
(43, 'SESSAD ITEP PRG (Cugnaux)', 8),
(44, 'SESSAD ITEP PRG (Ramonville)', 8),
(45, 'ITEP Enfants (Castanet Aux 4 Vents)', 8),
(46, 'ITEP Enfants (St-Loup Charta)', 8),
(47, 'ITEP Adolescents (Cugnaux L\'Oustalet)', 8),
(48, 'ITEP Adolescents (Tlse Lambert)', 8),
(49, 'CMPP St-Simon (Muret)', 9),
(50, 'CMPP St-Simon (Tlse Bagatelle)', 9),
(51, 'CMPP St-Simon (Cugnaux)', 9),
(52, 'CMPP St-Simon (Plaisance)', 9),
(53, 'CMPP St-Simon (Lourdes)', 9),
(54, 'ITEP St-Simon (Tlse Les Ormes)', 9),
(55, 'ITEP St-Simon (Muret les Pins)', 9),
(56, 'CAFS St-Simon (Tlse Les Ormes)', 9),
(57, 'Equipe mobile expérimentale', 9),
(58, 'SESSAD Beroï', 9),
(59, 'ITEP Beroï', 9),
(60, 'CMPP St-Simon (Muret)', 9),
(61, 'Coopération CAMSP 65', 9),
(62, 'Hopital de jour St-Léon', 10),
(63, 'Hopital de jour Les Autans', 10),
(64, 'Hopital de jour Les Bourdettes', 10),
(65, 'Hopital de jour Magellan', 10),
(66, 'Hopital de jour Lou Caminel', 10),
(67, 'Ambulatoire CMP/CATTP (Balma)', 10),
(68, 'Ambulatoire CMP (Castanet Tolosan)', 10),
(69, 'Ambulatoire CMP/CATTP (St Leon)', 10),
(70, 'Ambulatoire CMP/CATTP (Tlse La Farouette)', 10),
(71, 'Ambulatoire CMP (Tlse La Reynerie)', 10),
(72, 'Ambulatoire CMP/CATTP (Lou Caminel)', 10),
(73, 'Ambulatoire CMP/CATTP (Quint Fonsegrives)', 10),
(74, 'Ambulatoire CMP/CATTP (Revel)', 10),
(75, 'Ambulatoire CMP (St Orens)', 10),
(76, 'Ambulatoire CMP (Villefranche)', 10),
(77, 'Ambulatoire Périnatalité (Ambroise Paré)', 10),
(78, 'Ambulatoire Périnatalité (Sarrus Teinturiers)', 10),
(79, 'Ambulatoire Périnatalité (Clinique de l\'Union)', 10),
(80, 'Coopération UMES (Unité Mobile d’Evaluation et de Soutien)', 10),
(81, 'Coopération EVALTED (diagnostic troubles autistique)', 10),
(82, 'Coopération CAPPA (Coopération d\'Appui des Parcours Précauces en Autisme)', 10),
(83, 'AED', 10),
(84, 'ISS Toulouse - Région niv.III et niv.V', 11),
(85, 'ISS Albi - Région niv.III', 11),
(86, 'ISS Tarbes - Région niv.III et niv.V', 11),
(87, 'ISS Toulouse - Formation continue', 11),
(88, 'ISS Albi - Formation continue', 11),
(89, 'ISS Tarbes - Formation continue', 11),
(90, 'ISS Albi - CFA', 11),
(91, 'Empan', 11),
(92, 'IME les Sources de Nayrac', 12),
(93, 'ESAT les Sources de Nayrac (BPAS)', 12),
(94, 'ESAT les Sources de Nayrac (BAPC)', 12),
(95, 'SESSAD IME les Sources de Nayrac', 12),
(96, 'CAMSP les Sources de Nayrac', 12),
(97, 'AEMO AED', 12),
(98, 'CADA (Tlse Sardelis)', 13),
(99, 'CPH (Tlse Sardelis)', 13),
(100, 'Mecs St-Joseph (Miremont)', 13),
(101, 'SAP.SJ-MAP (Tlse Colasson)', 13),
(102, 'SAP.SJ-CJSE (Tlse Colasson)', 13),
(103, 'CFA Spécialisé', 13),
(104, 'Insertion 31', 13);

-- --------------------------------------------------------

--
-- Structure de la table `pole`
--

CREATE TABLE `pole` (
  `id_pole` int(11) NOT NULL,
  `pole_nom` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pole`
--

INSERT INTO `pole` (`id_pole`, `pole_nom`) VALUES
(1, 'Direction Générale'),
(2, 'Cuisine Centrale'),
(3, 'MECS Lucie Aubrac'),
(4, 'Pôle Adulte 31'),
(5, 'Pôle Adultes Henri Cros'),
(6, 'Pôle Pousinies Bordeneuve'),
(7, 'Pôle Enfances Plurielles'),
(8, 'Pôle Rives Garonne'),
(9, 'Pôle Béroï Collectif St-Simon'),
(10, 'Pôle Guidance infantile'),
(11, 'Pôle Institut St-Simon'),
(12, 'Pôle Lotois'),
(13, 'Pôle Social');

-- --------------------------------------------------------

--
-- Structure de la table `qvt_role`
--

CREATE TABLE `qvt_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `role_val` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `qvt_role`
--

INSERT INTO `qvt_role` (`role_id`, `role_name`, `role_val`) VALUES
(1, 'super_admin', 1),
(2, 'admin', 2),
(3, 'évaluateur', 3);

-- --------------------------------------------------------

--
-- Structure de la table `qvt_user`
--

CREATE TABLE `qvt_user` (
  `user_id` int(11) NOT NULL,
  `user_firstname` varchar(60) NOT NULL,
  `user_lastname` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `user_role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `qvt_user`
--

INSERT INTO `qvt_user` (`user_id`, `user_firstname`, `user_lastname`, `user_email`, `user_password`, `active`, `user_role`) VALUES
(1, 'super', 'admin', 'superadmin@lh.org', '25f9e794323b453885f5181f1b624d0b', 1, 1),
(2, 'admin', 'admin', 'admin@lh.org', '25f9e794323b453885f5181f1b624d0b', 1, 2),
(5, 'evaluateur', 'evaluateur', 'evaluateur@lh.org', '25f9e794323b453885f5181f1b624d0b', 1, 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`access_id`),
  ADD KEY `FK_role` (`role_id`),
  ADD KEY `FK_module` (`module_id`);

--
-- Index pour la table `determinant`
--
ALTER TABLE `determinant`
  ADD PRIMARY KEY (`determinant_id`),
  ADD KEY `family_id` (`family_id`);

--
-- Index pour la table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`device_id`),
  ADD KEY `determinant_id` (`determinant_id`);

--
-- Index pour la table `device_has_listing`
--
ALTER TABLE `device_has_listing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_listing_id` (`listing_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Index pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`evaluation_id`);

--
-- Index pour la table `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`family_id`);

--
-- Index pour la table `importance`
--
ALTER TABLE `importance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pole_id` (`pole_id`),
  ADD KEY `determinant_id` (`determinant_id`);

--
-- Index pour la table `listing`
--
ALTER TABLE `listing`
  ADD PRIMARY KEY (`listing_id`),
  ADD KEY `device_id` (`listing_score`);

--
-- Index pour la table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `perimetre`
--
ALTER TABLE `perimetre`
  ADD PRIMARY KEY (`id_perimetre`);

--
-- Index pour la table `pole`
--
ALTER TABLE `pole`
  ADD PRIMARY KEY (`id_pole`);

--
-- Index pour la table `qvt_role`
--
ALTER TABLE `qvt_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Index pour la table `qvt_user`
--
ALTER TABLE `qvt_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_role` (`user_role`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `access`
--
ALTER TABLE `access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `determinant`
--
ALTER TABLE `determinant`
  MODIFY `determinant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `device`
--
ALTER TABLE `device`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT pour la table `device_has_listing`
--
ALTER TABLE `device_has_listing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;
--
-- AUTO_INCREMENT pour la table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `evaluation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;
--
-- AUTO_INCREMENT pour la table `family`
--
ALTER TABLE `family`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `importance`
--
ALTER TABLE `importance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT pour la table `listing`
--
ALTER TABLE `listing`
  MODIFY `listing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `perimetre`
--
ALTER TABLE `perimetre`
  MODIFY `id_perimetre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT pour la table `pole`
--
ALTER TABLE `pole`
  MODIFY `id_pole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `qvt_role`
--
ALTER TABLE `qvt_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `qvt_user`
--
ALTER TABLE `qvt_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `qvt_user`
--
ALTER TABLE `qvt_user`
  ADD CONSTRAINT `qvt_user_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `qvt_role` (`role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
