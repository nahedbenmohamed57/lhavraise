-- MySQL dump 10.13  Distrib 5.5.58, for debian-linux-gnu (x86_64)
--
-- Host: 10.2.10.77    Database: clicksolrc3cpay
-- ------------------------------------------------------
-- Server version	5.6.39-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `3cp_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3cp_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `tel1` varchar(20) DEFAULT NULL,
  `adress` varchar(20) NOT NULL,
  `role` varchar(20) NOT NULL,
  `tax` varchar(250) NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `3cp_account` WRITE;
/*!40000 ALTER TABLE `3cp_account` DISABLE KEYS */;
INSERT INTO `3cp_account` VALUES (152,'Gherab','sassi.elhem@gmail.com','15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225','2154879','tunis','webMaster','',1),(161,'test','test@gmail.com','9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',NULL,'tunis','Client','123',1),(162,'essai','essai@htomail.fr','71b4e190fc7a0aa86f24cb18d88c09bfd8a45292f1ae434fac3c0351f4d838d3',NULL,'monastir','Client','741',1),(163,'test','test@gmail.com','9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',NULL,'test','Client','123',1),(164,'essai','essai@gmail.com','71b4e190fc7a0aa86f24cb18d88c09bfd8a45292f1ae434fac3c0351f4d838d3',NULL,'essai','Client','1232',1),(165,'azerty','azerty@gmail.com','f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9',NULL,'azerty','Client','123',1),(174,'elhem','e.sassi@3click-solutions.com','66befeedf2d742a28ff44606f05c583271ebb743d18fdd34c0efd831f7f8b9ec',NULL,'123','Client','123456789',1),(175,'dali','m.bougharriou@3click-solutions.com','66befeedf2d742a28ff44606f05c583271ebb743d18fdd34c0efd831f7f8b9ec',NULL,'tunis','Client','123456789',1),(176,'3click solutons','bougharriou.mohamed.ali@gmail.com','7b087e4c3d3b4320b045cd3e9f564c9621377426c3f06a755dbb30bfb6f0b178',NULL,'6, rue des cyclamens','Client','1441750T',1);
/*!40000 ALTER TABLE `3cp_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `3cp_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3cp_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `adress` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `tax` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `3cp_customers` WRITE;
/*!40000 ALTER TABLE `3cp_customers` DISABLE KEYS */;
INSERT INTO `3cp_customers` VALUES (1,'client1','','',''),(2,'client2\n','','',''),(52,'elhem','monastir','elhem@gmail.com','123456789');
/*!40000 ALTER TABLE `3cp_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3cp_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `date` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `sumTTC` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `devise` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `3cp_invoices` WRITE;
/*!40000 ALTER TABLE `3cp_invoices` DISABLE KEYS */;
INSERT INTO `3cp_invoices` VALUES (75,'041','2019-01-01','40','open','TND',176),(74,'021','2019-01-01','51','open','TND',176),(73,'3 eme essai','2019-01-01','50','open','TND',176),(72,'2 eme test','2018-01-01','20','open','Dolar',176),(71,'3click solutinos','1981-01-01','150','open','TND',176),(67,'test montant 4','2018-03-01','963','open','TND',165),(70,'facture pour 3click solutions','1981-04-01','150','open','Euro',176),(76,'042','2018-01-01','20','open','TND',176),(77,'fact3','2018-03-15','123','open','TND',165),(78,'facture test','2018-03-06','147','payer','TND',165),(79,'test de finalisation','2018-01-01','2036.131','open','TND',176),(80,'MOhamed Ali Bougharriou','2018-01-01','120.00','open','Euro',176),(81,'22032018','2018-01-01','120.00','open','Dolar',176),(82,'22_03_2018_2','2018-02-02','152.000','payer','TND',176);
/*!40000 ALTER TABLE `3cp_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3cp_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `position` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `3cp_modules` WRITE;
/*!40000 ALTER TABLE `3cp_modules` DISABLE KEYS */;
INSERT INTO `3cp_modules` VALUES (1,'dashboard','dashboard','ion-ios-home-outline',1,'admin');
/*!40000 ALTER TABLE `3cp_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3cp_password_resets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `3cp_password_resets` WRITE;
/*!40000 ALTER TABLE `3cp_password_resets` DISABLE KEYS */;
INSERT INTO `3cp_password_resets` VALUES (1,'m.bougharriou@3click-solutions.com','axi99fR8w2EbQQbU6fw8NEk1GfK6fGBRtjjq70x9HY='),(2,'e.sassi@3click-solutions.com','8J29uAs3RbJ6645Ifh1dfG7WjuHeR37+41UdIGDdSo='),(3,'m.bougharriou@3click-solutions.com','EIICNwg6eKp7iDtCrIFKJhqyy5OiZGuehSrgQsm87yA='),(4,'e.sassi@3click-solutions.com','3Lusil0I3yTxt2MvQBrjo81rCGNF+t595ck8rOEex+8='),(5,'e.sassi@3click-solutions.com','rhGTiHqfLyt1yoForTyBba6K19LxPzrTqaqnyI+gODg='),(6,'e.sassi@3click-solutions.com','UXg1WmCt4b4WPy6wgLOL08sNNOb6izCRYi096kPoWs='),(7,'e.sassi@3click-solutions.com','2Eyn8dV3Z1mOjD2nhvqO+1mXZ+QmWpYK+ICFcrH5wo='),(8,'e.sassi@3click-solutions.com','s9qUtFNtYYb1EWOCRr0+96PeciEctOyeG5wNXimNQ='),(9,'e.sassi@3click-solutions.com','L6Y4iB7rEDwCWJIuWzebqpkrm7ctD99+VZqJDWiGE='),(10,'m.bougharriou@3click-solutions.com','WRCad9PX1FxPgCM0CHZG7oMksdSMAjkzNk2OKmANtxw='),(11,'bougharriou.mohamed.ali@gmail.com','JMWi8h426erDw8Y3W96GaHVHaFb5hPNIOEwvEl41kw='),(12,'sassi.elhem@gmail.com','9Q19e4kIZNLHCYO0G1A7VuUtFds1Iln1gENLbg2r7c=');
/*!40000 ALTER TABLE `3cp_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pay`
--

DROP TABLE IF EXISTS `pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3cp_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `montant` varchar(250) NOT NULL,
  `reference` varchar(250) NOT NULL,
  `devise` varchar(250) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pay`
--

LOCK TABLES `3cp_pay` WRITE;
/*!40000 ALTER TABLE `3cp_pay` DISABLE KEYS */;
INSERT INTO `3cp_pay` VALUES (86,'147.000','747c9701d4edd4809626','TND',78),(85,'123,456.000','f7e66bdff4b5e79951e7','TND',77),(84,'20.000','0c1640ecd91a1c696ca1','TND',76),(83,'40.000','da44068cf93ae3f3cbfd','TND',75),(82,'51.000','918a77bb7bea0ec9afe9','TND',74),(81,'50.000','a020ba531a216abba570','TND',73),(80,'150.000','6024ea3331aa85f372b9','TND',71),(79,'150.00','31e9a46fd607e6ed9c5c','Euro',70),(78,'123.147','5928b3f0b6249904a333','Euro',62),(77,'963.000','c2d6dcb1ef3038ec9d34','TND',67),(76,'789.000','8abbacabdc026cef5393','TND',64),(75,'123.147','ab73bcf09adfe9d739ec','TND',60),(87,'','','',0),(88,'1,024.00','391aa10fb216946fc917','Euro',79),(89,'120.00','1ff42b0638fa10200792','Euro',80),(90,'120.00','95f43ddab791803d26c1','Dolar',81),(91,'152.000','1a819b380e8d1d6874d8','TND',82);
/*!40000 ALTER TABLE `3cp_pay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'clicksolrc3cpay'
--

--
-- Dumping routines for database 'clicksolrc3cpay'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-18  2:01:03
