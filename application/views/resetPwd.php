<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('application/views/includes/header.php'); ?>
</head>
<body>
	<header>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-12 col-md-12 col-12">
					<img src="<?=base_url()?>assets/images/logo.png" alt="logo">
				</div>
			</div>
		</div>
	</header>
	<section class="login">
		<div class="container-fluid">
			<div class="row">
				<div class="bloc-login">
					<h2>Modification du mot de passe</h2>		
					<div class="form1">
						<input name="password1" type="password" id="password1" class="form-control" placeholder="Mot de passe" required>
							<div class="help-block"></div>
						<br>
							<input name="password2" type="password" id="password2" class="form-control" placeholder="Confirmer mot de passe" required>
							<div class="help-block"></div>
						<br>
						<div class="form-group" id="submit-group">
							<button id="submit-login" onclick="checkPwd('<?php echo ($token); ?>')" class="btn">Envoyer</button>
						</div>
						<br>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer style="margin-top: 134px;">
		<?php include('application/views/includes/footer.php'); ?>
	</footer>
</body>
</html>
