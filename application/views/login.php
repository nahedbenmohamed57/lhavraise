<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include('application/views/includes/header.php'); ?>
</head>
<body>
<header class="header-login">
    <div class="container-fluid">
        <div class="row padQ">
            <div class="col-xl-7 col-md-7 col-7" style=" float: right;">
                <!-- <img src="<?=base_url()?>assets/images/logo.png" alt="logo"> -->
            </div>
			<div class="col-xl-5 col-md-5 col-5" style=" float: left;">
				<form method="post" name="login" id="login">
                    <div class="form-group" id="user-group">
                        <label for="user" class="col-sm-5 control-label" >Identifiant</label>
                        <input type="text" class="col-sm-7 form-control" id="user" name="user" placeholder="">
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group" id="password-group">
                        <label for="password" class="col-sm-5 control-label">Mot de passe</label>
                        <input type="password" class="col-sm-7 form-control" id="password" name="password" placeholder="">
                        <div class="help-block"></div>
                    </div>
                    <a href="<?php echo site_url('/login/forgetPwd') ?>" class="col-sm-7 forgot-password" id="forgot-password">Mot de passe oublié</a>
                    <div class="form-group btn-login" id="submit-group">
                        <button id="submit-login" class="btn">CONNEXION</button>
                    </div>
                    <div id="the-message"></div>
                </form>
            </div>
        </div>
    </div>
    <section class="login">
    <div class="container-fluid vertical-middle">
        <div class="text-center">
            <h1 class="display-1 masQ" style="font-size: 50px;"><b>Evaluation interne <br>des risques psychosociaux</b></h1>
            <span class="dwd-fancyline fancyline_after"></span>
        </div>
        
    </div>
</section>
</header>

</body>
</html>