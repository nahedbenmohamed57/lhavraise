<div class="container-fluid">
    <div class="row">
		<div class="col-md-6">
			<a href="/family" class="btn sousMenu submit mr-2" style=''>
				<i class="fas fa-arrow-left fa-titre" title="Retour"></i>
				<span style="color:#fff;">Retour</span>
			</a>
			
			<h2>
				<strong style="color: rgb(38, 96, 133);">
					Editer les dispositifs
				</strong>
			</h2>
		</div>
	</div>
</div>
<!-- <div class="row">
	<button class="sousMenu submit" style="right:250px !important;position: absolute; z-index: auto;">
		<i class="fas fa-plus fa-titre" title="Ajouter"></i>
		<a onclick="showAjaxModal('<?php echo base_url();?>family/listing/add/<?=$determinant->determinant_id;?>','<?php echo($titleCotation);?>');">
			<span style="color:#fff;">Ajouter Cotation</span>
		</a>
	</button>

	<button class="sousMenu submit" style="right: 134px !important;position: absolute; z-index: auto;">
		<a href="<?php echo base_url();?>family/crud_listing/show">
			<span style="color:#fff;">Liste Cotation</span>
		</a>
	</button>
</div> -->
<div class="row" style="margin-top: 24px;">
	<div class="col-xs-12 col-sm-12 col-lg-6">
		<div class="tabelDrop">
			<div class="table-head head-deter">
				<span class="titleDeterminant"><?php echo($determinant->determinant_name); ?></span>
			</div>

			<div>
				<table class='table display'  style="margin-top: 40px;">
					<tbody>
					<?php foreach($devices as $device){ ?>
						<tr id="<?php echo($device->device_id) ; ?>">
							<td>
								<div  class="col-xs-10 col-sm-10 col-lg-10" onclick="showListing(<?=$device->device_id;?>)" style="padding-bottom: 15px;">
								<?php echo($device->device_name) ; ?>
								</div>
							</td>
							<td style="width: 12%;">
								<a href="#" data-href="<?php echo base_url();?>family/device/delete/<?=$determinant->determinant_id;?>/<?php echo($device->device_id) ; ?>" data-toggle="modal" data-target="#modal_delete">
									<i class="fas fa-trash fa-deter" title="Supprimer"></i>
								</a>
								<a onclick="showAjaxModal('<?php echo base_url();?>family/device/update/<?=$determinant->determinant_id;?>/<?php echo($device->device_id) ; ?>','<?php echo($titleUpdateDevice);?>');">
								<i class="fas fa-edit fa-deter" title="Modifier"></i>
								</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>

		<a onclick="showAjaxModal('<?php echo base_url();?>family/device/add/<?=$determinant->determinant_id;?>','<?php echo($titleDevice);?>');">
			<button class="sousMenu submit" style="width: 100%; margin-top: 24px; cursor: pointer; color:#fff;">
			<i class="fas fa-plus fa-titre" title="Ajouter"></i>
			<span>Ajouter un  dispositif</span></button>
		</a>
	</div>

	<!-- <div class="tabelDropLeft col-xs-12 col-sm-12 col-lg-5">
		<div class="table-head head-deter">
			<span class="titleDeterminant">Cotations</span>
		</div>
		<div class="subcont no-padding min-height-410 " style="margin-top: 40px;" >
			<div class="listeDevice"  style="margin-left: 2px;">
			</div>
		</div>
	</div> -->
</div>
<div class="text-right">
<br>
<br>
</div>


