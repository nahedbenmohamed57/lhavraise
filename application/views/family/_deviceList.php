<?php   
 //   $attributes = array('class' => '', 'id' => 'user_form');
  //  echo form_open_multipart('family/device/addListing//'.$device_id, $attributes); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                </div>
                <div class="panel-body"> 
                    <input type="hidden" name="device_id" class="form-control" id="device_id"  value="<?php echo($device_id); ?>">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Nom</label>
                        <div class="col-md-12" style="margin-bottom: 20px;">
                            <select  class="col-md-12" name="listing_id" id="listing_id">
                                <?php foreach($listing as $val){ ?>
                                    <option class="col-md-12" value="<?=$val->listing_id;?>"><?php echo ($val->listing_score.' - '.$val->listing_name);?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" onclick="submitform()" class="submit">
                                Ajouter
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function submitform() {
 var listingId =  $('#listing_id').find(":selected").val();
    $.ajax({
        url: window.location.origin + '/family/device/addListing/false/' + '<?php echo($device_id); ?>' ,
        data: {"listing_id":listingId},
        dataType: 'json',
        type: 'POST',
        success: function (resp) {
           showListing(resp);
           $('#modal_ajax').modal('toggle');
        }
    });
}
</script>