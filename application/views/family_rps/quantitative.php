<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		EVALUER LES FACTEURS DE RPS
        </strong><br>
         <strong style="color: rgb(38, 96, 133)">
		Evaluation quantitative
        </strong>
    </h2>
	<div class="row">
		
		<div class="col-md-3 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b>Pôle :</b></span>
			<br>
			<select id="pole" name="forma" class="form-control">
				<option></option>
				<?php foreach ($poles as $key => $p) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($p->id_pole == $pole_id ) echo "selected"?>><?=$p->pole_nom?></option>
            	<?php } ?>
			</select>
		</div>

		<div class="col-md-3 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b>Etablissement:</b></span>
			<br>
			<select id="perimetre" name="perimetre" class="form-control">
				<?php foreach ($perimetres as $key => $pr) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($pr->id_perimetre == $perimetre_id ) echo "selected"?>><?=$pr->perimetre_nom?></option>
            	<?php } ?>

			</select>
		</div>
        	<div class="col-md-3 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b>UT:</b></span>
			<br>
			<select id="nombre_unite" name="nombre_unite" class="form-control">
				<?php foreach ($units as $key => $u) { ?>
            		<option value="<?=$p->id_pole?>" <?php if($u->id_unite == $unit_id ) echo "selected"?>><?=$u->unite_nom?></option>
            	<?php } ?>

			</select>
		</div>
		
		<div class="col-md-3 pt-5">
			<br>
			<button id="load-btn" class="btn sousMenu submit mt-3 w-100" >Charger les résultats</button>
		</div>
	</div>

	<div id="load-table" class='mt-5'></div>
	<div class="row">
		<form name="form" role="form"  class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
			<?php foreach($families_rps as $family){ ?>

			<table class="table-bordered" width="100%" style="margin-bottom: 58px">	
				<thead>
			 	<tr>
			  		<th >Famille</th>
			   		<th>Facteur</th>
			    	<th >Description</th> 
			    	<th colspan="3">Eposition (EX)</th>
			    	<th colspan="2">Impact</th>  
			    	<th>Niveau de risque</th>
				</tr>
				<tr> 
			 		<th></th> 
			 		<th></th> 
			 		<th></th>
			 	 	<th>Intensité(I)</th>
			 	  	<th>Priorité (P)</th> 
			 	  	<th>Eposition (EX)</th>
			 	  	<th>Impact (IM)</th>
			 	  	<th>Fiabilité</th>
			 	  	<th><span id="nv-risque"></span></th>
			 	</tr> 
			 </thead>
			 	<?php $i= 1;
			 	    foreach($facteurs as $facteur){ 
					    if($facteur->family_rps_id == $family->family_id){ $i++;}
					}?>
			 	<tr> 
					<th rowspan="<?=$i?>"><?=$family->family_rps_name;?></th> 
                  <?php $i= 0;
			 	    foreach($facteurs as $facteur){ 
					    if($facteur->family_rps_id == $family->family_id){
					    	$current_elemnt = [];
					    	foreach ($evaluation_qantitive as $key => $eval) {
					    	 	if($eval->facteur_id == $facteur->facteur_id) {
					    	 		$current_elemnt = $eval;
					    	 	}
					    	}?>
                    <tr>
                    	<th colspan=""><?=$facteur->facteur_name?></th>
                    	<td><input type="text" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_description" value="<?php if(!empty($current_elemnt)) echo $current_elemnt->description?>"></td>
                    	<td>
                    		<select class="form-control intensity" id ="intensity_<?=$family->family_id?>_<?=$facteur->facteur_id?>" data-info="<?=$family->family_id?>_<?=$facteur->facteur_id?>" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_intensity">
                    			<option value="1" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 1) echo "selected"?>>1</option>
                    			<option value="2" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 2) echo "selected"?>>2</option>
                    			<option value="3" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 3) echo "selected"?>>3</option>
                    			<option value="4" <?php if(!empty($current_elemnt)) if($current_elemnt->intensity == 4) echo "selected"?>>4</option>
                    		</select>
                    	</td> 
					    <td>
					    	<select class="form-control" id ="priority_<?=$family->family_id?>_<?=$facteur->facteur_id?>" data-info="<?=$family->family_id?>_<?=$facteur->facteur_id?>" name="<?=$family->family_id?>_<?=$facteur->facteur_id?>_priority">
                    			<option value="0" <?php if(!empty($current_elemnt)) if($current_elemnt->priority == 0) echo "selected"?>>0</option>
                    			<option value="1" <?php if(!empty($current_elemnt)) if($current_elemnt->priority == 1) echo "selected"?>>1</option>
                    		</select>
                    	</td> 
					    <td id="ex_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center"></td>
					    <td id="impact_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center"><?=$facteur->impact?></td>
						    <?php $color_fiabilite = '#fff';
						    		if($facteur->fiabilite > 0 && $facteur->fiabilite < 2) {
										$color_fiabilite = '#66aa88';
									}else if($facteur->fiabilite >= 2 && $facteur->fiabilite < 3) {
										$color_fiabilite = '#ffc000';
									}
									else if($facteur->fiabilite>= 3 && $facteur->fiabilite < 4) {
										$color_fiabilite = '#f79646';
									}
									else if($facteur->fiabilite >= 4 && $facteur->fiabilite < 5) {
										$color_fiabilite = '#c0504d';
									}else if($facteur->fiabilite >= 5) {
										$color_fiabilite = '#000000';
									}?>
					    <td style="background: <?=$color_fiabilite?>"><?=$facteur->fiabilite?></td>
					    <td id="NvRisq_<?=$family->family_id?>_<?=$facteur->facteur_id?>" class="text-center">0</td>	
                	</tr>
                	<?php }
                }?>

				</tr> 

			</table>
	  	<?php }
	  	$rps_evaluation_id = null;
	  	if(!empty($current_elemnt)) $rps_evaluation_id = $current_elemnt->rps_evaluation_id ?>
	  	<div class="form-group" style="margin-top: 20px;">
	  		<a href="/rps_evaluation/qualitative_evaluation/<?=$pole_id?>/<?=$perimetre_id?>/<?=$unit_id?>/<?=$rps_evaluation_id?>" class="submit" style="margin-left: 19px;">Continuer vers l'évaluation qualitative</a>
            <button type="submit" class="submit">
                Sauvegarder
            </button>
           
        </div>
	 </form>

	 </div>
<br>
</div>
<script type="text/javascript">
	$('[id^=intensity]').on('change',function() {
		var intensity = $(this).val();
		var info      = $(this).data('info');
		var priority  = $('#priority_'+info).val() ;
		var ex = parseInt(intensity)+parseInt(priority);
		$('#ex_'+info).text(ex);
		var impact = $('#impact_'+info).text();
		var nvRisq = (ex+parseFloat(impact))/2;
		console.log(nvRisq);
		$('#NvRisq_'+info).text(nvRisq);
	})
	$('[id^=priority_]').on('change',function() {
		var priority  = $(this).val();
		var info      = $(this).data('info');
		var intensity = $('#intensity_'+info).val() ;
		var ex = parseInt(intensity)+parseInt(priority);
		$('#ex_'+info).text(ex);
		var impact = $('#impact_'+info).text();
		var nvRisq = (ex+parseFloat(impact))/2;
		$('#NvRisq_'+info).text(nvRisq);
	});

	$( ".intensity" ).each(function( index ) {
		var intensity = $(this).val();
		var info      = $(this).data('info');
		var priority  = $('#priority_'+info).val() ;
		var ex = parseInt(intensity)+parseInt(priority);

		var color = '#fff';
		if(ex > 0 && ex < 2) {
			color = '#66aa88';
		}else if(ex >= 2 && ex < 3) {
			color = '#ffc000';
		}
		else if(ex >= 3 && ex < 4) {
			color = '#f79646';
		}
		else if(ex >= 4 && ex < 5) {
			color = '#c0504d';
		}else if(ex >= 5) {
			color = '#000000';
		}
		$('#ex_'+info).css('background', color);
		$('#ex_'+info).text(ex);
		var impact = $('#impact_'+info).text();

		var color_impact = '#fff';
		if(impact > 0 && impact < 2) {
			color_impact = '#66aa88';
		}else if(impact >= 2 && impact < 3) {
			color_impact = '#ffc000';
		}
		else if(impact >= 3 && impact < 4) {
			color_impact = '#f79646';
		}
		else if(impact >= 4 && impact < 5) {
			color = '#c0504d';
		}else if(impact >= 5) {
			color_impact = '#000000';
		}

		$('#impact_'+info).css('background', color_impact);

		var nvRisq = (ex+parseFloat(impact))/2;

		var color_NvRisq = '#fff';
		if(nvRisq > 0 && nvRisq < 2) {
			color_NvRisq = '#66aa88';
		}else if(nvRisq >= 2 && nvRisq < 3) {
			color_NvRisq = '#ffc000';
		}
		else if(nvRisq >= 3 && nvRisq < 4) {
			color_NvRisq = '#f79646';
		}
		else if(nvRisq >= 4 && nvRisq < 5) {
			color = '#c0504d';
		}else if(nvRisq >= 5) {
			color_NvRisq = '#000000';
		}
		$('#NvRisq_'+info).css('background', color_NvRisq);
		$('#NvRisq_'+info).text(nvRisq);

	});

</script>