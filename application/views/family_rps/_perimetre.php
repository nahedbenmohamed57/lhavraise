<style type="text/css">
		.pole_title
		{
			background-color: rgba(151, 10, 44) !important;
			border-radius:  8px;
			padding: 6px 2%;
			color: #fff;
			font-size: 16px;
			text-transform: initial;
			b
		}
</style>

<div class="container-fluid" style="margin:0px">
		<a href="/pole" class="btn sousMenu submit mr-2" style=''>
			<i class="fas fa-arrow-left fa-titre" title="Retour"></i>
			<span style="color:#fff;">Retour</span>
		</a>
			
		<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">EVALUER LES FACTEURS DE RPS</strong></h2>
		<h3 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Choix du périmètre de l’évaluation</strong></h3>
		<div class="row">
           
        <div class="col-md-12" style="margin-top: 20px;">
        	<?php $id_p_add=0; ?>
        	<span style='color: rgb(38, 96, 133); display: inline-block; font-size: 32px; margin-bottom: 32px;'><?php echo $pole->pole_nom; ?></span>
        	<a href="<?php echo base_url() ?>unite/crud_unite/add/<?php echo $id_p_add; ?>/<?=$p_id;?>" style="cursor: pointer;">
			</a>
			<form method="post" name="unite" id="unite">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_pole">Pôle</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="pole" class="form-control w-50" onchange="update(this.value)"></select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_etablissement">Etablissement</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="perimetre" class="form-control w-50" onchange="window.location = this.value"></select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					<div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_unite">Unité de travail</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="nombre_unite" id="nombre_unite" placeholder="">
                            <small class="help-block"></small>
                        </div>
                    	</div>
					</div>
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_unite">Date de l’évaluation</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="daste" class="form-control" name="date" id="date" placeholder="">
                            <small class="help-block"></small>
                        </div>
                    	</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3 col-4">
								<label for="lieux">Evaluateurs (Prénoms, noms et fonctions)</label>
							</div>
							<div class="col-sm-9 col-8">
								<input type="text" class="form-control" name="evaluateurs" id="evaluateurs" placeholder="">
								<small class="help-block"></small>
							</div>
						</div>
					</div>
					<div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_personne">Salariés associés à l’évaluation (Prénoms, noms et fonctions)</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="salaries" id="salaries" placeholder="">
                            <small class="help-block"></small>
                        </div>
                    	</div>
					</div>
					<div>
						<button type="submit" class="submit" disabled=""> Démarrer l’évaluation des RPS</button>
					</div>
            </form>
        </div>
    </div>
</div>