<style type="text/css">
		.pole_title
		{
			background-color: rgba(151, 10, 44) !important;
			border-radius:  8px;
			padding: 6px 2%;
			color: #fff;
			font-size: 16px;
			text-transform: initial;
			b
		}
</style>

<div class="container-fluid" style="margin:0px">
		<a href="/pole" class="btn sousMenu submit mr-2" style=''>
			<i class="fas fa-arrow-left fa-titre" title="Retour"></i>
			<span style="color:#fff;">Retour</span>
		</a>
			
		<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Ajouter une unité de travail</strong></h2>
		<div class="row" style="
    margin-bottom: 24px;
    display: inline;
    float: right;
    width: 90%;
">
           
        <div class="col-md-12" style="margin-top: 20px;">

			<form method="post" name="unite" id="unite">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_pole">Pôle</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="pole" class="form-control w-50" name="pole_id">
                            	<option></option>
                            	<?php if(!empty($pole)) {
                            		foreach ($pole as $key => $p) {
                            			?>
                            			<option value="<?=$p->id_pole?>" <?php if($p->id_pole == $p_id) echo "selected"?>><?=$p->pole_nom?></option>
                            			<?php 
                            		}
                            	}?>
                            	
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_etablissement">Etablissement</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="perimetre" class="form-control w-50" name="perimetre_id"></select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					<div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="unite_nom">Nom de l'unité de travail</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="unite_nom" id="unite_nom" placeholder="">
                            <small class="help-block"></small>
                        </div>
                    	</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3 col-4">
								<label for="lieux">Lieux de travail concernés</label>
							</div>
							<div class="col-sm-9 col-8">
								<input type="text" class="form-control" name="lieux" id="lieux"placeholder="">
								<small class="help-block"></small>
							</div>
						</div>
					</div>
					<div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_personne">Nombre de personnes physiques concernées</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="nombre_personne" id="nombre_personne" placeholder="">
                            <small class="help-block"></small>
                        </div>
                    	</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3 col-4">
								<label for="fonction">Fonctions des personnes concernées</label>
							</div>
							<div class="col-sm-9 col-8">
								<input type="text" class="form-control" name="fonction" id="fonction" placeholder="">
								<small class="help-block"></small>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3 col-4">
								<label for="commenatires">Commentaires / remarques</label>
							</div>
							<div class="col-sm-9 col-8">
								<textarea type="text" class="form-control" name="commantaire" id="commenatires" placeholder=""></textarea>
								<small class="help-block"></small>
							</div>
						</div>
					</div>
					<div>
						<button type="submit" class="btn submit"> Termier et sauvegarder</button>
					</div>
            </form>
        </div>
    </div>
</div>