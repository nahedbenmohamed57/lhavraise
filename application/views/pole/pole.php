<?php
    if(!isset($profil))
        if($task == "update") $titre = ('Editer le ');
        else if($task == "show") $titre = ("Détails du ");
        else $titre = ('Ajouter un ');
        $titre .= ("pôle");
?>

</a>
<br>
<br>

<div class="container-fluid">
    <div class="row">
        <input type="hidden" class="form-control" name="id_pole" value="<?php if(isset($pole)) {echo($pole->id_pole);} ?>"  id="pole_id" placeholder="">

        <div class="col-md-6">
            <h2 class="pb-5"><strong style="color: rgb(38, 96, 133);"><?=$titre?> </strong></h2>
            <form name="addpole" method="post" class="form-group" 
                id="<?php   if(isset($pole)) {echo('editpole');}
                            else {echo('addpole');}?>">
                <div class="form-group" id="name-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="pole_nom">Nom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="pole_nom" value="<?php if(isset($pole)) {echo($pole->pole_nom);} ?>"  id="pole_nom" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
      
                <?php if($task != "show"){ ?>
                    <div class="form-group" id="user-submit-group">
                        <button type="submit" class="btn submit" 
                            id="<?php   if(isset($pole)) 
                                            {echo('edit-pole');}
                                        else 
                                            {echo('add-pole');}?>">
                            <div class="glyphicon glyphicon-plus"></div>
                            <?php if(isset($pole)){ echo('Modifier'); } else { echo('Ajouter'); }?>
                        </button>

                        <a href="/pole" class="btn sousMenu submit mr-2">
                            <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
                            <span style="color:#fff;">Retour</span>
                        </a>
                    </div>
                <?php } ?>
                <div id="the-message-add-pole"></div>
            </form>
        </div>
    </div>
</div>