<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133);">
            Liste des pôles
        </strong>
    </h2>
    <br>
  
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
        	<a href="<?php echo base_url() ?>pole/crud_pole/add/" style="cursor: pointer;">
				<button class="sousMenu submit" >
					<i class="fas fa-plus fa-titre" title="Ajouter"></i>
					<span style="color:#fff;">Ajouter pôle</span>
				</button>
			</a>

			<table id="tab" class='table display' style="text-align: center">
				<thead>
					<tr>
						<!-- <th>N°</th> -->
						<th>Pôle</th>
						<th>Modifier/Supprimer</th>
						<th>Etablissment</th>
						<th>Unites de Travail</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach($poles as $pole) : ?>
					<tr>
						<a href="google.com">
							<!-- <td><?=$pole->id_pole?></td> -->
							<td style="color: rgb(38, 96, 133);">   <?=$pole->pole_nom?></td>
							<td>
								<a href="<?php echo base_url();?>pole/crud_pole/update/<?=$pole->id_pole;?>">
									<i class="fas fa-edit" title="Modifier"></i>
								</a>
								<a href="<?php echo base_url();?>pole/crud_pole/delete/<?=$pole->id_pole;?>">
									<i class="fas fa-trash-alt " title="Supprimer"></i>
								</a>
								
							</td>
							<td >
								<a style="text-align: center" href="<?php echo base_url() ?>perimetre/perimetres_pole/<?=$pole->id_pole;?>" style="cursor: pointer;">
									<i class="fas fa-plus" title="Ajouter"></i>
								</a>
							</td> 
							<td style="color: rgb(38, 96, 133);">
							<a style="text-align: center" href="units/add_unite/<?=$pole->id_pole;?>" style="cursor: pointer;">
									<i class="fas fa-plus" title="Ajouter"></i>
							</a></td>
							
						</a>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		
    </div>
</div>

</div>

<script>
	function goBack() {
		window.history.back();
	}
</script>