<div class="container-fluid-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
            Liste des utilisateurs
        </strong>
    </h2>
    <br>
  
</div>

<div class="container-fluid-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
        	<a href="<?php echo base_url() ?>users/crud_user/add/" style="cursor: pointer;">
			<button class="sousMenu submit" >
				<i class="fas fa-plus fa-titre" title="Ajouter"></i>
				<span style="color:#fff;">Ajouter utilisateur</span>
			</button>
			
			</a>
			<table id="tab" class='table display table-bordered'>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Rôle</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($users as $user) {  ?>
					<tr>
						<td><?=$user->user_firstname?></td>
						<td><?=$user->user_lastname?></td> 
						<td><?=$user->user_role->role_name?></td>
						<td>
							<a href="<?php echo base_url();?>users/crud_user/delete/<?=$user->user_id;?>" data-toggle="modal" data-target="#modal_delete" class="delete">
								<i class="fas fa-trash-alt " title="Supprimer"></i>
							</a>
							<a href="<?php echo base_url();?>users/crud_user/update/<?=$user->user_id;?>">
								<i class="fas fa-edit" title="Modifier"></i>
							</a>
							<a href="<?php echo base_url();?>users/crud_user/show/<?=$user->user_id;?>">
								<i class="fas fa-eye" title="Vue"></i>
							</a>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		
		</div>
		
    </div>
</div>
<script>
	$('.delete').on('click',function() {
		var link = $(this).attr('href');
		$('#modal_delete .btn-ok').attr('href', link);
	})
</script>