<?php
    if (!isset($profil))
        if ($task == "update" && $user->user_id == $_SESSION['user_id']) $titre = ("Modifier mon profil");
        else if ($task == "update") $titre = ("Modifier le profil");
        else if($task == "show") $titre = ("Détails de l'utilisateur");
        else $titre = ('Ajouter un utilisateur');
        
        // $titre .= ("mon profil");
    
    if ($monprofil) {
        $titre = ("Modifier mon profil");
    }
?>

<div class="container-fluid" style="margin:0px">
    <div class="row">
        <input type="hidden" class="form-control" name="user_id" value="<?php if(isset($user)) {echo($user->user_id);} ?>"  id="user_id" placeholder="">
        
        <div class="col-md-6 ">
            <?php if ($updated) : ?>
            <div class="alert alert-success text-center"><?= $monprofil == TRUE ? 'Votre' : 'Le' ?> profil a été mis à jour avec succès</div>
            <?php endif; ?>
            
            <h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);"><?=$titre?> </strong></h2>

            <div class="col-md-4"></div><div class="col-md-2"></div>
            <form name="addUsers" method="post" class="form-group" 
                id="<?php   if(isset($user)) {echo('editUser');}
                            else {echo('addUser');}?>">
                <div class="form-group" id="name-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_user">Prénom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="user_firstname" value="<?php if(isset($user)) {echo($user->user_firstname);} ?>"  id="name_user" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="lastname-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="lastname_user">Nom</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="text" class="form-control" name="user_lastname" id="lastname_user" value="<?php if(isset($user)) {echo($user->user_lastname);} ?>" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="email-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="email_user">Email</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="email" class="form-control" name="user_email" id="email_user" value="<?php if(isset($user)) {echo($user->user_email);} ?>" placeholder=""
                            <?php if($task == "show"){ echo("disabled"); } ?>>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                
                <div class="form-group" id="user-id-access-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="user_role">Rôle</label>
                        </div>
                        <?php if(!isset($profil)){?>
                            <div class="col-sm-9 col-8">
                                <select name="user_role" class="form-control" id="user_role">
                                    <?php if($task != "show"){ ?>
                                        <option value="">Choisir le rôle</option>
                                    <?php }  foreach($roles as $role) { 
                                            if($role->role_id == $user->user_role){ ?>
                                                <option value="<?=$role->role_id;?>" selected><?=$role->role_name;?></option>
                                            <?php } else if($task != "show"){ ?>
                                                <option value="<?=$role->role_id;?>"><?=$role->role_name;?></option>
                                            <?php } 
                                    } ?>
                                </select>
                                <small class="help-block"></small>
                            </div>
                        <?php } else {?>
                            <div class="col-sm-9 col-8">
                                <input type="user_role" class="form-control" name="user_role" id="user_role" 
                                value="<?php if(isset($user)) {echo($user->user_role->role_name);} ?>" disabled>
                                <small class="help-block"></small>
                            </div>
                        <?php } ?>
                    </div>
                    <small class="help-block"></small>
                </div>

                <?php if(isset($profil)) {?>
                    <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password">Mot de passe</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="password" class="form-control" name="user_password" id="password_user" placeholder="******">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="password-user-group">
                        <div class="row">
                            <div class="col-sm-3 col-4">
                                <label for="user_password2">Confirmer mot de passe</label>
                            </div>
                            <div class="col-sm-9 col-8">
                                <input type="password" class="form-control" name="password_user_confirm" id="password_user_confirm" placeholder="******">
                                <small class="help-block"></small>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                <div class="form-group" id="password-user-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="user_password">Mot de passe</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <input type="password" class="form-control" name="user_password" id="password_user" placeholder="******">
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?php if($task != "show"){ ?>
                    <div class="form-group" id="user-submit-group">
                        <button type="submit" class="submit" id="<?php if(isset($user))  {echo('edit-user');} else  {echo('add-user');}?>">
                            <div class="glyphicon glyphicon-plus"></div>
                            <?php if(isset($user)){ echo('Modifier'); } else { echo('Ajouter'); }?>
                        </button>

                        <a href="/users" class="btn sousMenu submit mr-2">
                            <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
                            <span style="color:#fff;">Retour</span>
                        </a>
                    </div>
                <?php } ?>
                <div id="the-message-add-user"></div>
            </form>
        </div>

        <br>
		
    </div>
    
</div>