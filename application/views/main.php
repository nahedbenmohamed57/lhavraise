<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			$GLOBALS["modules"] = $modules; 
			include('application/views/includes/header.php');
		?>
	</head>
    
	<body>
		<div class="content">
			<div class="row">
				<div class="col-md-1"><?php include('application/views/includes/sidebar.php'); ?></div>
				<div class="col-md-11">
					
			<div class="content-container">
				<header>
					<?php include('application/views/includes/navigation.php'); ?>
				</header>

				<div class="container-fluid">
					<div id="overlay" class="text-center">
					    <svg xmlns="http://www.w3.org/2000/svg" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-wedges" style="background: none;"><g transform="translate(50,50)"><g ng-attr-transform="scale({{config.scale}})" transform="scale(0.7)"><g transform="translate(-50,-50)"><g transform="rotate(136.981 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="0.75s" begin="0s" repeatCount="indefinite"/><path ng-attr-fill-opacity="{{config.opacity}}" ng-attr-fill="{{config.c1}}" d="M50 50L50 0A50 50 0 0 1 100 50Z" fill-opacity="0.8" fill="#1d3f72"/></g><g transform="rotate(192.736 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"/><path ng-attr-fill-opacity="{{config.opacity}}" ng-attr-fill="{{config.c2}}" d="M50 50L50 0A50 50 0 0 1 100 50Z" transform="rotate(90 50 50)" fill-opacity="0.8" fill="#5699d2"/></g><g transform="rotate(248.49 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1.5s" begin="0s" repeatCount="indefinite"/><path ng-attr-fill-opacity="{{config.opacity}}" ng-attr-fill="{{config.c3}}" d="M50 50L50 0A50 50 0 0 1 100 50Z" transform="rotate(180 50 50)" fill-opacity="0.8" fill="#d8ebf9"/></g><g transform="rotate(304.245 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="3s" begin="0s" repeatCount="indefinite"/><path ng-attr-fill-opacity="{{config.opacity}}" ng-attr-fill="{{config.c4}}" d="M50 50L50 0A50 50 0 0 1 100 50Z" transform="rotate(270 50 50)" fill-opacity="0.8" fill="#71c2cc"/></g></g></g></g></svg>
					</div>
					<div class="content" style="display: none"><?php include('application/views/' . $template_content . '.php'); ?></div>
				</div>
			</div>
			
				</div>
			</div>
			
			
		</div>

		<?php include('application/views/includes/modal.php'); ?>
		
		<footer>
			<?php include('application/views/includes/footer.php'); ?>
		</footer>
    </body>
</html>

<script type="text/javascript">
	$(window).load(function(){
   		$('#overlay').fadeOut();
   		$('.content').fadeIn();
	});
</script>