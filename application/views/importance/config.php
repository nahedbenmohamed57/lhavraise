<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		Evaluation de l'importance des déterminants de la QVT
        </strong>
    </h2>
</div>

<?php
    function get_value($data, $id) {
        $result = array_filter($data, function($item) use ($id) {
            return $item->determinant_id == $id;
        });

        if (sizeof($result) > 0) {
            return array_values($result)[0]->valeur;
        } else {
            return 1;
        }
    }
?>

<div class="container-fluid">
    <!--<h1 class="pb-3">Evaluation de l'importance des déterminants de la QVT</h1>-->
<p>1 : Importance minimale - 2 : Importance moyenne - 3 : Importance haute - 4 : Importance maximale</p>
    <?php if ($saved) : ?>
    <div class="alert alert-success">
            L'évaluation de l'importance des déterminants est terminée
    </div>
    <?php endif; ?>

    <form method="post">
        <?php foreach ($family as $fam) : ?>
        <div class="card">
            <div class="card-header">
                <?=$fam->family_name?>
            </div>
            
            <div class="card-body">
                <?php $fam_id = $fam->family_id; ?>
                <?php $dets = array_filter($determinants, function($item) use ($fam_id) { return $item->family_id === $fam_id; }); ?>
                <?php foreach ($dets as $det) : ?>
                <div class="custom-control pb-3 pt-3">
                    <label><?=$det->determinant_name?></label>
                    <input name='<?=$det->determinant_id?>' value='<?=get_value($importances, $det->determinant_id)?>' type="range" class="custom-range" min="1" max="4">
                    <div class="d-flex">
                       <i title="Importance min"><div class="flex-fill text-left" >1</div></i>
                        <div class="flex-fill text-center">2</div>
                        <div class="flex-fill text-center">3</div>
                        <i title="Importance max"><div class="flex-fill text-left" >4</div></i>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endforeach; ?>
        
        <button type="submit" class="btn sousMenu submit">Terminer</button>

        <button onclick="window.history.back()" class="btn sousMenu submit mr-2">
            <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
            <span style="color:#fff;">Retour</span>
        </button>
    </form>
</div>

<br>
<br>