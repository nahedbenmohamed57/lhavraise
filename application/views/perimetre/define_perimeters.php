<div class="container-fluid" style="margin:0px">
		<!--<a href="/pole" class="btn sousMenu submit mr-2" style=''>
			<i class="fas fa-arrow-left fa-titre" title="Retour"></i>
			<span style="color:#fff;">Retour</span>
		</a>-->
			
		<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Définir les périmètres </strong></h2>
		
		<div class="row perimeter-header">
    		<div class="col-md-2" style="padding: 10px 18px;"><span style="    margin-right: 15px;    font-weight: bold;">Pôle</span><a href="/pole/crud_pole/add/"><i class="fas fa-plus" title="Ajouter"></i></a></div> 
    	    <div class="col-md-5" style="padding: 10px 18px;border-right: 1px solid #ccc;     border-left: 1px solid #ccc;"><span style="    margin-right: 15px;    font-weight: bold;">Etablissement</span><a href="/perimetre/crud_perimetre/add/null/null"><i class="fas fa-plus" title="Ajouter"></i></a></div> 
    	    <div class="col-md-5" style="padding: 10px 18px;"><span style="    margin-right: 15px;    font-weight: bold;">Unité de travail</span><a href="units/add_unite"><i class="fas fa-plus" title="Ajouter"></i></a></div>
    	</div>
        <div class="row perimeter">  
        	<?php
        	foreach ($perimetres as $key => $perimeter) {
        		?>
        		<div class="row pole">
        			<div class="col-md-2" style="border-left: 1px solid #ccc;">
        				<span><?=$perimeter->pole_nom?><a href="/pole/crud_pole/update/<?=$perimeter->id_pole?>"><i class="fas fa-pencil-alt" title="Ajouter"></i></a></span>
        				
        			</div>
        			<div class="col-md-10 list-etap">
        				
        				<?php if(!empty($perimeter->Eteab)) {
        					 foreach ($perimeter->Eteab as $k => $Eteab) { ?>
								<div class="row etab">
	        					  <div class="col-md-6 my-etab">
	        					  	<span><?=$Eteab->perimetre_nom?></span>
	        					  	<a href="/perimetre/crud_perimetre/update/<?=$Eteab->id_perimetre?>/"><i class="fas fa-pencil-alt" title="Ajouter"></i>
                                    </a>
                                    <?php if($_SESSION['role_id'] == 1) {
                                        ?>
                                         <a href="/perimetre/crud_perimetre/delete/<?=$Eteab->id_perimetre?>/<?=$perimeter->id_pole?>" data-toggle="modal" data-target="#modal_delete" class="delete" style="margin-right: 15px;">
                                        <i class="fas fa-trash-alt " title="Supprimer"></i>
                                    </a>
                                     <?php } ?>
                                   
	        					  </div>
	        					  <div class="col-md-6 units">
	        					  <?php if(!empty($Eteab->units)) {
	        					  	?>
	        					  	<table>
	        					  	<?php foreach ($Eteab->units as $ke => $units) { ?>
		        					 
		        					 	<tr><td><span><?=$units->unite_nom?></span>
		        					 		<a href="units/crud_units/update/<?=$units->id_unite?>"><i class="fas fa-pencil-alt" title="Ajouter"></i></a>
                                            <?php if($_SESSION['role_id'] == 1) {
                                                ?>
                                            <a href="units/crud_units/delete/<?=$units->id_unite?>" data-toggle="modal" data-target="#modal_delete"  class="delete" style="margin-right:15px"><i class="fas fa-trash-alt " title="Supprimer"></i></a>
                                        <?php }?>
		        					 	</td></tr>
		        					
	        					   <?php  }?>
	        					   	</table>
	        					 <?php } ?>
 								   </div>
 							    </div>
        					 
        					  <?php } 
        				 }?>
        		
        			</div>
        		</div>
        	
        	<?php }
        	?>
    
    </div>
</div>
<script>
    $('.delete').on('click',function() {
        var link = $(this).attr('href');
        $('#modal_delete .btn-ok').attr('href', link);
    })
</script>