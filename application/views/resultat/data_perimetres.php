<table class="table table-bordered" style="width: 200px;">
    <thead>
        <tr style="background: #f0f3f4;text-align: center;">
        <?php foreach ($units as $unit) : ?>
            <th colspan="3"><?=$unit->unite_nom?>
                <a href="/resultat/unit_result/<?=$unit->pole_id?>/<?=$unit->perimetre_id?>/<?=$unit->id_unite?>/<?=$active_periode_id?>" style="float: right;text-align: center;"><img src="/assets/images/see_more.png"></a>
            </th>
        <?php endforeach; ?>
        </tr>

        <tr>
        <?php foreach ($units as $unit) : ?>
            <th>EX</th>
            <th>IM</th>
            <th>RI</th>
        <?php endforeach; ?>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($family_rps as $family) : ?>
            <?php foreach ($family->facteur as $index => $facteur) : ?>
            <tr>
                <?php 
                foreach ($units as $unit) : ?>
                   <?php
                    $ex = 0;
                    $impact = 0;

                   if(!empty($unit->evals)) {
                        foreach ($unit->evals as $key => $last_evaluation) {
                            if(!empty($last_evaluation->last_eval)) {
                                foreach ($last_evaluation->last_eval as $key => $pv) {

                                    if($pv->facteur_id == $facteur->facteur_id) {
                                        $ex += ($pv->intensity + $pv->priority);
                                        if(!empty($pv->impact)) {
                                            $impact = $pv->impact;
                                        } 
                                    }
                                }
                            }
                        }
                    }
                    $color="";
                    if($ex == 1) {
                        $color = "#67AA87";
                    } elseif($ex == 2) {
                         $color = "#efef32";
                    } elseif($ex == 3) {
                         $color = "orange";
                    } elseif($ex == 4) {
                         $color = "#e26969";
                    }elseif($ex == 5) {
                        $color = "black";
                    }
                    ?>
                    <td style="background: <?=$color?>;color:#fff"><?=$ex?></td>
                    <?php 
                     if($impact >= 1 && $impact < 2) {
                        $color = "#67AA87";
                    } elseif($impact >= 2 && $impact < 3) {
                         $color = "#efef32";
                    } elseif($impact >= 3 && $impact < 4) {
                         $color = "orange";
                    } elseif($impact >= 4 && $impact < 5) {
                         $color = "#e26969";
                    }elseif($impact >=5) {
                        $color = "black";
                    }
                    ?>
                    <td style="background: <?=$color?>;color:#fff"><?=$impact?> </td>
                     <?php $res = $ex+$impact;

                     if($res >= 1 && $res < 2) {
                        $color = "#67AA87";
                    } elseif($res >= 2 && $res < 3) {
                         $color = "#efef32";
                    } elseif($res >= 3 && $res < 4) {
                         $color = "orange";
                    } elseif($res >= 4 && $res < 5) {
                         $color = "#e26969";
                    }elseif($res >= 5) {
                        $color = "black";
                    }
                     ?>
                    <td style="background: <?=$color?>;color:#fff"><?=$res?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>