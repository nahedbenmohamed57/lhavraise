<?php
    function calcAvancementDetr($devices) {
        $total = 0;

        foreach ($devices as $device) {
            $total += $device->avancement;
        }

        return number_format($total / sizeof($devices), 2, '.', '');
    }

    function calcPriorite($determinant) {
        $impo = $determinant->importance;
        $avan = calcAvancementDetr($determinant->dispositifs);

        return ($impo + 4 - $avan) / 2;
    }
?>



        

<table class='table mt-5 mb-5'>
    <thead>
        <tr>
            <th style='background: gray; color: white; border: none;'>Déterminants</th>
            <th style='background: gray; color: white; border: none;'>Dispositifs</th>
            <th style='background: gray; color: white; border: none;' class="text-center">Importance</th>
            <th style='background: gray; color: white; border: none;' class="text-center">Avancement par dispositifs</th>
            <th style='background: gray; color: white; border: none;' class="text-center">Avancement par déterminants</th>
            <th style='background: gray; color: white; border: none;' class="text-center">PRIORITÉ</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($families as $family) : ?>
        <tr>
            <td style='background: silver;' colspan='6'><?=$family->family_name?></td>
        </tr>
        <?php foreach ($family->determinants as $determinant) : ?>

            

           


            <tr>
                <td style='border: thin solid silver;' rowspan="<?=sizeof($determinant->dispositifs)?>" class="align-middle"><?=$determinant->determinant_name?></td>
                <td style='border: thin solid silver;'><?=$determinant->dispositifs[0]->device_name?></td>

                <!--rim -->
               
                

                <td <?php if($determinant->importance == 1) { ?>  bgcolor ="green"<?php }?>    <?php if($determinant->importance == 4) { ?>  bgcolor ="red"<?php }?> bgcolor="yellow"   style='border: thin solid silver;' rowspan="<?=sizeof($determinant->dispositifs)?>" class="align-middle text-center" ><?=25 * $determinant->importance + 0?>%</td>
                <td  <?php if(number_format($determinant->dispositifs[0]->avancement, 2, '.', '') == 1) { ?>  bgcolor ="red"<?php }?>    <?php if(number_format($determinant->dispositifs[0]->avancement, 2, '.', '')== 4) { ?>  bgcolor ="green"<?php }?>  bgcolor="yellow"   class="text-center"><?=25 * number_format($determinant->dispositifs[0]->avancement, 2, '.', '') + 0 * 25?> %</td>
                <td           <?php if(calcAvancementDetr($determinant->dispositifs) == 1) { ?>  bgcolor ="red"<?php }?>    <?php if(calcAvancementDetr($determinant->dispositifs) == 4) { ?>  bgcolor ="green"<?php }?>    bgcolor="yellow"      style='border: thin solid silver;' rowspan="<?=sizeof($determinant->dispositifs)?>" class="align-middle text-center"><?=25 * calcAvancementDetr($determinant->dispositifs) + 0?> %</td>
                <td          <?php if(calcPriorite($determinant) == 1) { ?>  bgcolor ="green"<?php }?>    <?php if(calcPriorite($determinant) == 4) { ?>  bgcolor ="red"<?php }?>              bgcolor="yellow"                 style='border: thin solid silver;' rowspan="<?=sizeof($determinant->dispositifs)?>" class="align-middle text-center"><?=25 * calcPriorite($determinant) + 0?> %</td>
            </tr>
            <tr><?= number_format($determinant->dispositifs[0]->avancement, 2, '.', '') ?></tr>
            <?php $first = TRUE; ?>
            <?php foreach ($determinant->dispositifs as $dispositif) : ?>
                <?php if (!$first) : ?>
                <tr>
                    <td style='border: thin solid silver;'><?=$dispositif->device_name?></td>
                    <td style='border: thin solid silver;' class="text-center"><?=25 * number_format($dispositif->avancement, 2, '.', '') + 0?> %</td>
                </tr>
                <?php endif; ?>
                <?php $first = FALSE; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>