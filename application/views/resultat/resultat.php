<h1 style="color: rgb(38, 96, 133);">Résultat de l'évaluation</h1>

<h2 style="color: rgb(38, 96, 133);"><?php
    switch ($focus) {
        case 1: echo "Résultats généraux"; break;
        case 2: echo "Résultats par pôle"; break;
        case 3: echo "Résultats par établissements"; break;
        case 4: echo "Résultats par unités de travail"; break;
    }
?></h2>
<div class="row" style="margin-bottom:25px">
    <div class="col-md-6">
        <label>Période</label>
        <select class="form-control" name="period" id="period" onchange="init()">
            <option value="0">--Choisir période--</option>
            <?php foreach ($periodes as $key => $periode) { ?>
                <option value="<?=$periode->id?>"><?=$periode->libelle?></option>
           <?php  }?>
           
        </select>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <span id="focus" style="display: none"><?=$focus?></span>
        <?php if($focus == 2){?>
            <div class="row">
                <div class="col-md-4">Pôle: </div>
                <select class="col-md-4 form-control" id="pole">
                    <option value="0">--Choisir--</option>
                   <?php foreach ($poles as $key => $p) { ?>
                            <option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
                        <?php } ?>
                </select>
                <div class="col-md-4">
                    <button class="btn sousMenu submit" style="width: 100%" onclick="init()">Charger</button>
                </div>
            </div>
       <?php } ?>
        <?php if($focus == 3){?>
            <div class="row">
                <div class="col-md-4">Etablissements: </div>
                <select class="col-md-4 form-control" id="perimetre">
                    <option value="0">--Choisir--</option>
                   <?php foreach ($perimetres as $key => $pr) { ?>
                    <option value="<?=$pr->id_perimetre?>"><?=$pr->perimetre_nom?></option>
                <?php } ?>
                </select>
                <div class="col-md-4">
                    <button class="btn sousMenu submit" style="width: 100%" onclick="init()">Charger</button>
                </div>
            </div>
       <?php } ?>

       <?php if($focus == 4){?>
            <div class="row">
                <div class="col-md-4">Unités: </div>
                <select class="col-md-4 form-control" id="unit">
                    <option value="0">--Choisir--</option>
                  <?php foreach ($units as $key => $u) { ?>
                    <option value="<?=$u->id_unite?>"><?=$u->unite_nom?></option>
                   <?php } ?>
                </select>
                <div class="col-md-4">
                    <button class="btn sousMenu submit" style="width: 100%" onclick="init()">Charger</button>
                </div>
            </div>
       <?php } ?>
    </div>
    <div class="col-md-6">
        <a href="/resultat" class="btn sousMenu submit mt-3">Retour au choix du périmètre</a>
    </div>
</div>

<div class="row">
    <div class="col-md-6" style="width: 66vw;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">Familles de RPS</th>
                    <th>Facteurs de RPS</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($family_rps as $family) : ?>
                    <tr>
                        <td rowspan="<?=sizeof($family->facteur) + 1?>"><?=$family->family_rps_name?></td>
                    </tr>

                    <?php foreach ($family->facteur as $index => $facteur) : ?>
                        <tr>
                            <td><?=$index + 1?>. <?=$facteur->facteur_name?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="col-md-6" style="width: 66vw;">
        <div id="load-table" style="overflow-x: auto; white-space: nowrap;"></div>
    </div>
</div>

<button style="background: #67AA87; color: white;" class="btn mt-3 mb-3">Exporter .xls</button>

<script>
    function init() {
        var dataTable = document.getElementById('load-table')
        dataTable.innerHTML = "<p class='m-5 p-5 text-center bg-light'><i class='fa fa-circle-notch fa-spin'></i> Loading</p>"
        
        load()
    }

    function load() {
		$('#load-btn').text('Chargement en cours...');
        var focus = $('#focus').text();
        var pole  = $('#pole').val();
        var etab  = $('#perimetre').val();
        var unit  = $('#unit').val();
        var period = $('#period').val();
        focus = +focus+'/'+pole+'/'+etab+'/'+unit+'/'+period;
        console.log(period);
        console.log(focus);
		$.get(`/resultat/data/`+focus, function(response) {
			var dataTable = document.getElementById('load-table')
			dataTable.innerHTML = response
		})
	}
    
    init()
</script>

<style>
    table tbody td {
        white-space: nowrap;
        overflow: auto;
        text-overflow: ellipsis;
    }
</style>