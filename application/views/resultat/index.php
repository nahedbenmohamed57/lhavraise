<div class="container-fluid result-page">
   <h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">EVALUER LES FACTEURS DE RPS</strong></h2>
		<h3 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Choix du périmètre de l’évaluation</strong></h3>
    <br>

	<div class="row">
		<ul>
			<li>
				<a href="/resultat/show/1">Résultats généraux Ligue Havraise</a>
			</li>

			<li>
				<a href="/resultat/show/2">Résultats par pôle</a>
			</li>

			<li>
				<a href="/resultat/show/3">Résultats par établissements</a>
			</li>

			<!--<li>
				<a href="/resultat/show/4">Résultats par unités de travail</a>
			</li>-->
		</ul>
	</div>
</div>