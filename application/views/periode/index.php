<div class="container-fluid" style="margin:0px">
    <h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Période de l'évaluation</strong></h2>

    <p>Période en cours : <?= $active ? $active : 'Aucune' ?></p>

    <a href="/periode/cloturer" class="btn sousMenu submit" style='float: none; width: 300px;'>
        Clôturer la période
    </a>

    <br>

    <a href="/periode/nouvelle" class="btn sousMenu submit" style='float: none; width: 300px;'>
        <i class="fa fa-plus"></i> Nouvelle période
    </a>

    <br>

    <a href="/periode/historique" class="btn sousMenu submit" style='float: none; width: 300px;'>
        Historique des périodes
    </a>

    <br>
</div>