<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h2 class="pb-5" style="color: rgb(38, 96, 133);">Nouvelle période</h2>

            <?php if ($active) : ?>
            <div class="alert alert-danger">
                Il faut d'abord clôturer la dérniere période "<?=$active?>"
            </div>
            <?php endif; ?>

            <form name="addperiode" method="post" class="form-group" id="addperiode">
                <div class="form-group" id="name-user-group">
                    <label for="libelle">Nom de la période</label>
                    <input type="text" class="form-control" name="libelle" value="" id="libelle" placeholder="">
                </div>
      
                <div class="form-group" id="user-submit-group">
                    <button type="submit" class="btn submit" <?php if ($active) echo "disabled"; ?>>
                        Ajouter
                    </button>

                    <a href="/periode" class="btn sousMenu submit mr-2">
                        <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
                        <span style="color:#fff;">Retour</span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>