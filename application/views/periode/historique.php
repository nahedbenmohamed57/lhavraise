<div class="container-fluid">
    <a href="/periode" class="btn sousMenu submit mr-2" style="float: right;">
        <i class="fas fa-arrow-left fa-titre" title="Retour"></i>
        <span style="color:#fff;">Retour</span>
    </a>

    <h2 class="pb-5" style="color: rgb(38, 96, 133);">Historique des périodes</h2>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom de la période</th>
                <th>Cloturée</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($periodes as $periode) : ?>
            <tr>
                <td><?=$periode->id?></td>
                <td><?=$periode->libelle?></td>
                <td>
                    <?=$periode->active ? '<span class="badge badge-danger">NON</span>' : '<span class="badge badge-success">OUI</span>'?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>