<!DOCTYPE html>
<html lang="fr">
<head>
	<?php include('application/views/includes/header.php'); ?>
</head>
<body>
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-12">
                <img src="<?=base_url()?>assets/images/" alt="logo">
            </div>
        </div>
    </div>
</header>
	<section class="login">
		<div class="container-fluid">
			<div class="row">
				<div class="bloc-login">
					<h2></h2>
					<form method="post" name="login" id="login">
						<div class="form-group" id="email">
							<label for="email">Adresse e-mail</label>
							<input type="mail" onblur="verifyMail()"  value="" class="form-control" id="mail" name="mail" >
							<div class="help-block"></div>
						</div>
						<div class="form-group" id="submit-group">
							<button id="submit-login" onclick="sendMail()" class="submittedButtom btn">Envoyer</button>
						</div>
						<div id="the-message"></div>
					</form>
				</div>
			</div>
			<div class="row">     
			</div>
		</div>
	</section>
	<footer style="margin-top: 134px;">
		<?php include('application/views/includes/footer.php'); ?>
	</footer>

</body>
</html>