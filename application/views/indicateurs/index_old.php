<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		Résultats de l’évaluation
        </strong>
    </h2>
    <br>
	<div class="row">
		
		<div class="col-md-4 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b>Séléctionnner un Pôle :</b></span>
			<br>
			<select style='width: 100%; padding: 4px; border-radius: 3px; border: thin solid silver;' id="pole" name="forma" class="pole" onchange="update(this.value)"></select>
		</div>

		<div class="col-md-4 pt-5">
			<br>
			<span style="color: rgb(38, 96, 133)"><b>Séléctionnner une Autorisation :</b></span>
			<br>
			<select style='width: 100%; padding: 4px; border-radius: 3px; border: thin solid silver;' id="perimetre" name="forma" class="pole" onchange='_currentPerimetres = this.value'></select>
		</div>
		
		<div class="col-md-4">
			<button id="load-btn" class="btn sousMenu submit mt-3 w-100" onclick="load()">Terminer et sauvgarder</button>
		</div>
	</div>
</div>