<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
		Saisir les indicateurs
        </strong>
    </h2>
	<div class="row">
		<div class="col-md-4 pt-5">
			<span style="color: rgb(38, 96, 133)"><b>Séléctionnner un Pôle :</b></span>
			<select id="pole" name="forma" class="pole form-control">
				<option>Pôle</option>
				<?php foreach ($poles as $key => $p) {
					?>
					<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
				<?php } ?>
			</select>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-4 pt-5">
		<span style="color: rgb(38, 96, 133)"><b>Etablissement:</b></span>
		<select class="form-control" id="perimetre" name="forma" class="pole"></select>
	</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<button id="fill-indicator-btn" class="btn sousMenu submit mt-3 w-100" >Démarrer la saisie des indicateurs</button>
		</div>

	</div>
</div>
<script type="text/javascript">
	$('#fill-indicator-btn').on('click', function () {
		var perimetre = $("#perimetre").val();
		if(perimetre != null) {
			 window.location = '/indicateurs/fill_indicator/'+perimetre;
		}
	})
</script>