<div class="container-fluid-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
            Liste des actions
        </strong>
    </h2>
    <br>
  
</div>

<div class="container-fluid-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
        	<a href="<?php echo base_url() ?>action/crud_action/add/" style="cursor: pointer;">
			<button class="sousMenu submit" >
				<i class="fas fa-plus fa-titre" title="Ajouter"></i>
				<span style="color:#fff;">Ajouter action</span>
			</button>
			
			</a>
			<table id="tab" class='table display table-bordered'>
				<thead>
					<tr>
						<th>Action</th>
						<th>Périmètre concerné</th>
						<th>Échéance de mise en oeuvre</th>
						<th>Temps restant</th>
						<th>Actions</th>
					</tr>
				</thead>
				
				<tbody>
				
					<?php foreach($actions as $action) {?>	
					<tr>
						<td><?=$action->action_name?></td>
						<td><?=$action->action_perim?></td> 
						<td><?=$action->echeance?></td>
						<td></td>
						<td>	
							<a href="<?php echo base_url();?>action/crud_action/delete/<?=$action->action_id;?>" data-toggle="modal" data-target="#modal_delete" class="delete">
								<i class="fas fa-trash-alt " title="Supprimer"></i>
							</a>
							<a href="<?php echo base_url();?>action/crud_action/update/<?=$action->action_id;?>">
								<i class="fas fa-edit" title="Modifier"></i>
							</a>
	
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		
		</div>
		
    </div>
</div>

<script>
	$('.delete').on('click',function() {
		var link = $(this).attr('href');
		$('#modal_delete .btn-ok').attr('href', link);
	})
</script>