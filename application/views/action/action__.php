<div class="container-fluid">
    <h2>
        <strong style="color: rgb(38, 96, 133)">
            Plan d'actions
        </strong>
    </h2>
    <br>
  
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <?php if (isset($_GET['error'])) : ?>
            <div class="alert alert-danger">
                Erreur lors du téléchargement du fichier
            </div>
            <?php endif; ?>

        	<button class="sousMenu submit" data-toggle="modal" data-target="#modal-upload">
                <i class="fas fa-plus fa-titre" title="Ajouter"></i>
                <span style="color:#fff;">Nouvelle action</span>
            </button>

			<table id="tab" class='table display'>
				<thead>
					<tr>
						<th>Action</th>
						
						<th>Périmètre concerné</th>
						
						<th>Échéance de mise en oeuvre</th>
						
					
						<th>Temps restant</th>
							
					</tr>					
			
				</thead>
				<tbody>
					
				</tbody>
			</table>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open_multipart('outil/upload');?>
                <div class="modal-header text-white">
                    Nouvelle action
                </div>

                <div class="modal-body">
                    <input type="file" name="userfile">
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-danger btn-ok text-white">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
