<?php
    $current_module = $this->uri->segment(1);

    function getTooltip($module) {
        if($module->module_name == "Evaluation") return "Paramètres la méthode d'évaluation";
        elseif ($module->module_name == "dashboard") return "Accueil";
        // elseif ($module->module_name == "Profil") return "Mon profil";
        elseif ($module->module_name == "Pôle") return "Définir les Périmètres";
        elseif ($module->module_name == "Support utilisateur") return "Support";
        //com rim
        elseif ($module->module_name == "Période") return "Paramètres la periode d'évaluation";
        //com jaber 
        elseif ($module->module_name == "Indcateurs") return "Saisir les indicateurs";
        elseif ($module->module_name == "RPS") return "Evaluer les RPS";
        elseif ($module->module_name == "Action") return "Plan d'actions";
        //fin comm rim
        else return $module->module_name;
    }
?>

<div class="sidebar">
    <ul>
        <?php foreach ($modules as $module) : ?>
            <?php if ($module->link != 'login/logout') : ?>
                <li <?=$module->link === $current_module ? 'class="active"' : ''?> data-toggle="tooltip" data-placement="right" title="<?=getTooltip($module)?>">
                    <a href="<?=base_url() . $module->link?>">
                        <img src="<?=base_url() . 'assets/images/icons-svg/' . $module->icone . '.svg'?>">
                    </a>
                </li>

                

            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>

<script>
    $(document).ready(function() {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    })
</script>