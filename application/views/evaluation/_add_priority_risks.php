<?php   	
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($priority_risks)){
        echo form_open_multipart('/rps_evaluation/crud_priority_risks/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id.'/update/'.$priority_risks->id_priority_risks, $attributes);

    }else {
        echo form_open_multipart('/rps_evaluation/crud_priority_risks/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id.'/add', $attributes);
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <?php if(isset($priority_risks)){?>
                   <input type="hidden" name="id_priority_risks" class="form-control" id="id_priority_risks" value="<?=$priority_risks->id_priority_risks;?>">
                <?php } ?>
                <div class="panel-body"> 
                    <form name="form" role="form" id="" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="name_priority_risks" class="form-control" id="name_priority_risks" 
                                value="<?php if(!empty($priority_risks)) echo $priority_risks->name_priority_risks?>">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit">
                                    <?php  if(isset($priority_risks)){ 
                                        echo("Modifier") ;
                                    }else {
                                        echo("Ajouter") ;
                                    } ?> 
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>