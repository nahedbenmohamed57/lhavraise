<div class="container-fluid" style="margin:0px">
		<a href="/pole" class="btn sousMenu submit mr-2" style=''>
			<i class="fas fa-arrow-left fa-titre" title="Retour"></i>
			<span style="color:#fff;">Retour</span>
		</a>
			
		<h2 class="pb-5" ><strong style="color: rgb(38, 96, 133);">EVALUER LES FACTEURS DE RPS</strong></h2>
		<h3 class="pb-5" ><strong style="color: rgb(38, 96, 133);">Choix du périmètre de l’évaluation</strong></h3>
		<div class="row">
           
        <div class="col-md-12" style="margin-top: 20px;">
        
			<form method="post" name="unite" id="unite">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_pole">Pôle</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="pole" class="form-control w-50" name="pole_id">
                            	<option></option>
                            	<?php foreach ($poles as $key => $p) { ?>
                            		<option value="<?=$p->id_pole?>"><?=$p->pole_nom?></option>
                            	<?php } ?>
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="name_etablissement">Etablissement</label>
                        </div>
                        <div class="col-sm-9 col-8">
                            <select id="perimetre" class="form-control w-50" name="establishment_id"></select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_unite">Unité de travail</label>
                        </div>
                        <div class="col-sm-9 col-8">
                        	<select id="nombre_unite" name="unit_id" class="form-control w-50"> </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
					
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_unite">Date de l’évaluation</label>
                        </div>
                        <div class="col-sm-9 col-8">
                             <select class="form-control w-50" name="   evaluation_periode" style="display: none">
                                <option value="<?=$periodes->id?>" selected><?=$periodes->libelle?></option>
                            </select>
                            <input type="date" name="evaluation_date" class="form-control w-50">
                        </div>
                    </div>
					<div class="row">
						<div class="col-sm-3 col-4">
							<label for="lieux">Evaluateurs (Prénoms, noms et fonctions)</label>
						</div>
						<div class="col-sm-9 col-8">
							<input type="text" class="form-control w-50" name="Evaluator" id="evaluateurs" placeholder="">
							<small class="help-block"></small>
						</div>
					</div>
                    <div class="row">
                        <div class="col-sm-3 col-4">
                            <label for="nombre_personne">Salariés associés à l’évaluation (Prénoms, noms et fonctions)</label>
                        </div>
                        <div class="col-sm-9  col-8">
                            <input type="text" class="form-control w-50" name="salaried" id="salaries" placeholder="">
                            <small class="help-block"></small>
                        </div>
                    </div>
					
					<div>
						<button type="submit" class="submit "> Démarrer l’évaluation des RPS</button>
					</div>
            </form>
        </div>
    </div>
</div>