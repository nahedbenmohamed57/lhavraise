<style>
html {
  overflow-x: hidden;
}
body {
  overflow-x: hidden;
}
footer{
  margin-top: 130px; !important; 
  position: relative !important;
}
.button a{
    background-color:rgba(151, 10, 44) ;
    padding: 7px 12%;
    color: #fff;
    /*margin-top: 200px;*/
}
.question{
	background-color: #e8e8e8;
    width: 80%;
    margin: 0 auto;
    padding: 30px 0px;
}
.reponse {
	color:#000;list-style:none;width: 1000%;margin: 0 auto;
}
.reponse li{
    font-family: source-sans-pro.regular;
    font-size: 17px;
    font-weight: 500;
}
.#next{
	background: #e8e8e8;
	color: #FFF;
	font-size: 0.8em;
	padding: 0.9em 2em;position: relative;
	top: 90px;
}
.#prev{
	background: #e8e8e8;
	color: #FFF;
	font-size: 0.8em;
	padding: 0.9em 2em;position: relative;
	top: 120px;
}

div#next {
  margin-left: 3%;
  margin-left: 85%;
  width: 18%;
  
}
div#prev{
  margin-left: 3%;
  
  margin-top: 18px;
  width: 13% !important;
}
.progress {
  width: 100%;
  height: 50px;
  color: #e0ddde;
  background-color: #e0ddde;
  overflow: hidden;
  margin-top: 24px;
  top: 0;
  left: 0;
}

.progress .step {
  width: 200px;
  float: left;
  margin: 0;
  margin-top: 10px;
}

.progress .progBar {
  width: 100%;
  text-align: center;
  height: 30px;
  padding-top: 12px;
}

.progress .progBar .barWrap {
  border: 1px solid #970a2c;;
  height: 100%;
  overflow: hidden;
}

.progress .progBar .barWrap .bar {
  height: 100%;
  background-color: #970a2c;
}
.progress h3, .progress h6 {
  margin: 0;
  color: #970a2c;
}

.det.active {
  background: #3080C0;
  color: white;
}

</style>

<?php  
  $object = new stdClass();
  $object->question = '';
  $object->choices = '';
  $object->determinant = '';
  $object->listing_score = '';
  $object->id_det='';
  $object->det_avance=0;
  $questions[] =  $object; 

  //initilisation 
  $k=0; $i=0; $p=0;
  
  
  $h=0;
  $id_f=$family->family_id;
  $t=0;

  $last = end($families);
  $lastId = $last->family_id;

  while($t<sizeof($familyQCMs))
  {
    if($familyQCMs[$t]->family_id == $id_f)
    {
      $x[$h]=$familyQCMs[$t];
      $h++;
    }
    $t++;
  }
  
  
  $questions[$i]->question =  $x[$k]->device_name;
  $questions[$i]->questionId =  $x[$k]->device_id;
  $questions[$i]->determinant =  $x[$k]->determinant_name;
  $questions[$i]->choiceId[$p]  =  $x[$k]->listing_id;
  $questions[$i]->choices[$p]  =  $x[$k]->listing_name; 
  $questions[$i]->listing_score[$p]  =  $x[$k]->listing_score;
  $questions[$i]->id_det=$x[$k]->determinant_id;
   $questions[$i]->det_avance=0;

  $k=1;
  do
  {
    if($x[$k]->device_id != $x[$k-1]->device_id)
    {
      $i=$i+1; $p=0;    
      $questions[$i] = new stdClass();
    }else 
    { 
      $p=$p+1 ; 
   
    } 
    $questions[$i]->question =  $x[$k]->device_name;
    $questions[$i]->questionId =  $x[$k]->device_id;
    $questions[$i]->determinant =  $x[$k]->determinant_name;
    $questions[$i]->choiceId[$p]  =  $x[$k]->listing_id;
    $questions[$i]->choices[$p]  =  $x[$k]->listing_name;  
    $questions[$i]->listing_score[$p]  =  $x[$k]->listing_score; 
    $questions[$i]->id_det=$x[$k]->determinant_id;
    $questions[$i]->det_avance=0;
    $k++;

  }while($k < sizeof($x)); 
  //  while($k < 20); 
  //$questions[0]->choices=$questions[1]->choices;

  $nb=0;
  $nb_l=0;
  $id_d=$x[0]->device_id;
  while ($nb<sizeof($x))
  {
    if($id_d==$x[$nb]->device_id)
    {
      $s[$nb_l]=$x[$nb_l]->listing_name;
      $nb_l++;
    }
    $nb++;
  }
  $questions[0]->choices=$s;
 
  //avacement que chaque det

 for ($nb_div=0; $nb_div <= $i; $nb_div++) {
      $tab_id[$nb_div]=$questions[$nb_div]->id_det; 
      $tab_dev[$nb_div]=$questions[$nb_div]->questionId;
    
 }
$nb_eval=0;
$nb_anve=0;

if(sizeof($evaluation)+$i>sizeof($nb_devices))
{
    while ($nb_eval <sizeof($evaluation))
    { 
      if($nb_anve<sizeof($questions))
      {
          if($questions[$nb_anve]->questionId==$evaluation[$nb_eval]->device_id)
          { 
           
            $tab_eval[$nb_anve]=$evaluation[$nb_eval]->avancement;
             //echo nl2br($tab_dev[$nb_anve]." " .$evaluation[$nb_eval]->listing_id ." :  ".$tab_eval[$nb_anve]."\n" );
           
           //echo nl2br($evaluation[$nb_eval]->device_id." " .$evaluation[$nb_eval]->avancement." :  ".$tab_eval[$nb_anve]."\n" );
           $nb_anve=$nb_anve+1;
          }
      }
      $nb_eval++;
    } 
 
 $tab_id_uni=array_unique($tab_id);
 $nb_eval=0;
 for ($j=0; $j < sizeof($tab_id_uni); $j++) { 
    $tab_occ[$j]=count(array_keys($tab_id,$tab_id[$j]));
 }
  $nb_i=0;
  for ($j=0; $j <sizeof($tab_occ) ; $j++) 
  { 

    if ($j==0) 
    {
       $var=array_slice($tab_eval,$nb_i,$tab_occ[$j]);
       $t_som[$j]=array_sum($var)/$tab_occ[$j];
    }
    else
    {
         $nb_i=$nb_i+ $tab_occ[$j];
       $var=array_slice($tab_eval,$nb_i,$tab_occ[$j]);
       $t_som[$j]=array_sum($var)/$tab_occ[$j];
    }
      
       
  }
$nb_i=0;
$nb_x=0;
 $questions[$nb_i]->det_avance=$t_som[$nb_i];

    for ($j=1; $j <sizeof($questions) ; $j++) 
    { 
      $h=$j+1;
      if($h<sizeof($questions))
      {
        if($questions[$j]->id_det==$questions[$nb_x]->id_det) 
        {    
           $questions[$j]->det_avance=$t_som[$nb_i];
          
        }
        else
        {
          $nb_i++;
          $questions[$j]->det_avance=$t_som[$nb_i];
          
        }
       }

       if($j==sizeof($questions)-1)
       {
        $questions[$j]->det_avance=$t_som[$nb_i];
       }
       $nb_x++;
      
    }
}
         
?>

<h3 style="font-weight: bold;"><?=$pole->pole_nom;?></h3>
<h4 style="font-weight: bold;"><?=$perimetre[0]->perimetre_nom;?></h4>
<div style="width: 100%;height: 78px; color: white;display: grid; grid-template-columns: repeat(auto-fit, minmax(1px, 1fr)); margin-bottom: 12px;"> 
 
  <?php
    $nb_f=0;
    while ($nb_f<sizeof($families)) 
    {
        if($families[$nb_f]->family_id!=$id_f)
        {
    ?>
        <p style="font-size: 16px;padding:14px; color: #206086; border: 1px solid gray; margin-bottom: 0px;">
         <?php echo $families[$nb_f]->family_name;?>
         </p>
    <?php
        }
        else
        {
    ?>  
      <p style="font-size: 16px;background: #206086; padding:14px; border: 1px solid #206086; margin-bottom: 0px;">
       <?php echo $families[$nb_f]->family_name;?>
       </p>
    <?php
        }
    ?>
    
    <?php     
      $nb_f++; 
    }
    ?>

</div>

<div style="color: #3080C0; display: grid; grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
    <?php foreach ($determinants as $determinant) : ?>
      <p class="det" style="padding: 16px; margin: 0px; border: thin solid #3080C0;"><?=$determinant->determinant_name?></p>
    <?php endforeach; ?>
</div>

<div class="jumbotron home" style="border-radius: 0px; background: transparent;">
	<form method="post" action="<?=base_url()?>evaluation/save_evaluation/<?=$family->family_id;?>/<?=$pole->id_pole?>/<?=$perimetre[0]->id_perimetre?>" >
		<input type='hidden' id= 'selections' name='selections' value='' />
		<div class="row">
			<div id='quiz'></div>
    </div>
    
    <div class="progress"  style='background: transparent;'>
      <div class="step">
        <h6>dispositif <span id="stepNum" class="stepNum">1</span> / <?php echo $i+1; ?></h6>
      </div>
      
      <div class="progBar">
        <div class="barWrap"></div>
        <div id="bar" class="bar"></div>
      </div>
    </div>

    <div>
        <div class='button' id='prev'><a href='#' ><i class="glyphicon glyphicon-backward" style="margin-right:10px;"></i>Précédent</a></div>
        <div class='button' id='next'><a href='#' ><i class="glyphicon glyphicon-forward" style="margin-right:10px;"></i>Suivant</a></div>
    </div>
</div>
      <div id="myBtncontainer-fluid" class="text-center" style='display: none;'>
        <button class="btn btn-success" id="myBtn" style=" margin-bottom: 38px; background-color:rgba(151, 10, 44);
        border: none; padding: 6px 2%; color: #fff; font-size: 16px; text-transform: initial; margin: auto;">
          <?php if ($id_f != $lastId) { ?>
            Sauvegarder et continuer l'évaluation
          <?php } else { ?>
            Terminer et sauvegarder
          <?php } ?>
        </button>
      </div>
			</div>
	</form>
</div>

<script>
(function() {
  var questions =  <?php echo json_encode($questions); ?>; 
  var questionCounter = 0; //Tracks question number
  var selections = []; //Array containing user choices
  var quiz = $('#quiz'); //Quiz div object
  
  //pro bar
  var totalSteps =  <?php echo json_encode($i); ?>+1;
  var barWidth = $('.barWrap').width();
  var prog = barWidth/totalSteps;
  var currentValue = 1;
  var maxValue = <?php echo json_encode($i); ?>+1;
  
  $('#bar').css('width', prog);
  // Display initial question
  displayNext();
  
  // Click handler for the 'next' button
  $('#next').on('click', function (e) {
    e.preventDefault();
    
    // Suspend click listener during fade animation
    if(quiz.is(':animated')) {        
      return false;
    }
    choose();
    
    // If no user selection, progress is stopped
    if (isNaN(selections[questionCounter])) {
      alert('Choisissez une réponse!');
    } else {
      questionCounter++;
      displayNext();
      //pro bar
      currentValue++;
    if (currentValue > maxValue)
      currentValue = maxValue;
    
    $('#bar').css('width', prog * currentValue);
    $("#stepNum").text(currentValue);
    }
  });
  
  // Click handler for the 'prev' button
  $('#prev').on('click', function (e) {
    e.preventDefault(); 
    if(quiz.is(':animated')) {
      return false;
    }
    choose();
    //pro bar
    questionCounter--;
    displayNext();
      currentValue--;
    if (currentValue < 1)
      currentValue = 1;
    
    $('#bar').css('width', prog * currentValue);
    $("#stepNum").text(currentValue);
  });
  
  // Click handler for the 'Start Over' button
  $('#start').on('click', function (e) {
    e.preventDefault();
    if(quiz.is(':animated')) {
      return false;
    }
    questionCounter = 0;
    selections = [];
    displayNext();
    $('#start').hide();
    $('#myBtncontainer-fluid').hide();
  });
  
  // Animates buttons on hover
 
  
  // Creates and returns the div that contains the questions and 
  // the answer selections
  function createQuestionElement(index) {
    var qElement = $('<div>', {
      id: 'question'
    });
 
    // var determinant = $('<p style="color:rgb(236,102,8); font-size: 20px;font-family:source-sans-pro.regular ; font-weight: 700;">').append('DETERMINANT : ' + questions[index].determinant+' '+questions[index].det_avance+" %");
    // qElement.append(determinant);

    var dets = document.getElementsByClassName('det')
    var current_det = questions[index].determinant

    $('.det').removeClass('active')
    for (var i = 0; i < dets.length; i++) {
      var det = dets[i]
      
      if (det.innerText == current_det) {
        $(det).addClass('active')
      }
    }

    var question = $('<p style="color:#000;font-family:source-sans-pro.regular ; font-weight: 700;">').append(questions[index].question);
    qElement.append(question);

    var questionId = $('<input type="hidden" name="device_id" style="margin-right: 10px;"  value=' + questions[index].questionId  + ' />');
    qElement.append(questionId);
    
    var radioButtons = createRadios(index);
    qElement.append(radioButtons);
    
    return qElement;
  }
  
  // Creates a list of the answer choices as radio inputs
  function createRadios(index) {
    var radioList = $('<ul class="reponse">');
    var item;
    var input = '';
    for (var i = 0; i < questions[index].choices.length; i++) {
      item = $('<li>');
      input = '<input type="radio" name="answer" style="margin-right: 10px;"  value=' + questions[index].questionId  + "" +questions[index].choiceId[i]  + ' />';
      input += questions[index].listing_score[i] + '-' + questions[index].choices[i];
      item.append(input);
      radioList.append(item);
    }
    return radioList;
  }
  
  // Reads the user selection and pushes the value to an array
  function choose() {
    selections[questionCounter] = +$('input[name="answer"]:checked').val();   
  }
  // Displays next requested element
  function displayNext() {
    quiz.fadeOut(function() {
      $('#question').remove();
      if(questionCounter < questions.length){
        var nextQuestion = createQuestionElement(questionCounter);
        quiz.append(nextQuestion).fadeIn();
        if (!(isNaN(selections[questionCounter]))) {
          $('input[value='+selections[questionCounter]+']').prop('checked', true);
        }
        
        // Controls display of 'prev' button
        if(questionCounter === 1){
          $('#prev').show();
        } else if(questionCounter === 0){
          
          $('#prev').hide();
          $('#next').show();
        }
      }else {
        var scoreElem = displayScore();
        quiz.append(scoreElem).fadeIn();
        $('#next').hide();
        $('#prev').hide();
        $('#start').show();
      }
    });
  }
  // Computes score and returns a paragraph element to be displayed
  function displayScore() {
    console.log(selections); 
    document.getElementById('selections').value = selections;  
    $('#myBtncontainer-fluid').show();
    $('#titre').show();
    $('#next').hide();
    $('#prev').hide();
    $('.progress').hide();

    var score = $('text-align: center; margin-top: 70px; display: block;margin-bottom: 69px;">',{id: 'question'});
    var numCorrect = 0;
    for (var i = 0; i < selections.length; i++) {
      if (selections[i] === questions[i].correctAnswer) {
        numCorrect++;
      }
    }
    return score;
  }
})();
</script>
