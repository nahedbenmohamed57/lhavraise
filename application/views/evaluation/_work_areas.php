<?php   	
    $attributes = array('class' => '', 'id' => 'user_form');
    if(isset($work_areas)){
         echo form_open_multipart('/rps_evaluation/crud_work_areas/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id.'/update/'.$work_areas->id_work_areas, $attributes);

    }else {
        echo form_open_multipart('/rps_evaluation/crud_work_areas/'.$pole_id.'/'.$perimetre_id.'/'.$unit_id.'/'.$rps_evaluation_id.'/add', $attributes);
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <?php if(isset($work_areas)){?>
                   <input type="hidden" name="id_work_areas" class="form-control" id="id_work_areas" value="<?=$work_areas->id_work_areas;?>">
                <?php } ?>
                <div class="panel-body"> 
                    <form name="form" role="form" id="" class="form-horizontal form-groups-bordered" method="post" enctype="multipart/form-data">
                       
                        <div class="form-group">
                            <label for="field-1" class="col-sm-12 control-label"> Risques priortaires</label>
                            <div class="col-sm-12">
                               <select class="form-control" name="id_priority_risks">
                                   <?php if(!empty($priority_risks)){ 
                                   foreach ($priority_risks as $key => $risks) {?>
                                     <option value="<?=$risks->id_priority_risks?>" <?php if(!empty($work_areas)) if( $work_areas->id_priority_risks == $risks->id_priority_risks) echo "selected" ?>><?=$risks->name_priority_risks?></option>
                                  <?php  }
                                    } ?>
                               </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-12 control-label"> Situations de travail associées</label>
                            <div class="col-sm-12">
                               <select class="form-control" name="id_working_situations">
                                   <?php if(!empty($working_situations)){ 
                                   foreach ($working_situations as $key => $situations) {?>
                                     <option value="<?=$situations->id_working_situations?>" <?php if(!empty($work_areas)) if( $work_areas->id_working_situations == $situations->id_working_situations) echo "selected" ?>><?=$situations->name_working_situations?></option>
                                  <?php  }
                                    } ?>
                               </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nom</label>
                            <div class="col-sm-12" style="margin-bottom: 20px;">
                                <input type="text" name="work_areas_name" class="form-control" id="work_areas_name" value="<?php if(!empty($work_areas)) echo $work_areas->work_areas_name?>">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit">
                                    <?php  if(isset($work_areas)){ 
                                        echo("Modifier") ;
                                    }else {
                                        echo("Ajouter") ;
                                    } ?> 
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>