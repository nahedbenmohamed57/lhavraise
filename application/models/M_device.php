<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_device extends CI_Model{

    public $table = "device";

    public function __construct(){
        parent::__construct();
    }
	public function get_all_devices(){
        $this->db->select('*');
		$this->db->from('device');
		$query = $this->db->get();
		return($query->result()); 
		
    }
	public function get_device_ByDeter($determinant_id){
        $this->db->select('*');
		$this->db->from('device');
		$this->db->where('determinant_id',$determinant_id); 
		$query = $this->db->get();
		return($query->result()); 
		
    }

    public function get_device_ByID($device_id){
        $this->db->select('*');
		$this->db->from('device');
		$this->db->where('device_id',$device_id); 
		$query = $this->db->get();
		return($query->result()[0]); 
		
    }

    public function add_device($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

    public function get_avancement($id, $id_pole = 0, $id_perim = 0, $id_periode = 0) {
        if ($id_perim != 0) {
            $this->db->where('id_perimetre', $id_perim);
        } else if ($id_pole != 0) { 
            $this->db->where('id_perimetre in (SELECT id_perimetre from perimetre WHERE pole_id = ' . $id_pole . ')');
        }

        if ($id_periode) {
            $this->db->where('periode_id', $id_periode);
        }

        return $this->db->select('AVG(avancement) as evaluation')
            ->from('evaluation')
            ->where("device_id", $id)
            ->get()
            ->result()[0]->evaluation * 4 / 100;
    }

    public function delete_device($device_id){
        $this->db->where('device_id', $device_id);
        $this->db->delete('device');
        $this->db->where('device_id', $device_id);
        $this->db->delete('device_has_listing'); 
    }

    public function update_device($device_id, $data)
    {
        $this->db->where('device_id',$device_id);
		$this->db->set($data);
		$this->db->update('device');
    }

    public function add_deviceListing($data){
        $this->db->insert('device_has_listing',$data);
    }

    public function delete_deviceListing($device_id, $listing_id){
        $this->db->where('device_id', $device_id);
        $this->db->where('listing_id', $listing_id);
        $this->db->delete('device_has_listing'); 
    }

    public function get_device_ByFamily($family_id){
        $this->db->select('*');
        $this->db->from('device');
        $this->db->where('determinant.family_id', $family_id);
        $this->db->join('determinant', 'determinant.determinant_id = device.determinant_id');
		$query = $this->db->get();
		return($query->result()); 
		
    }
}

