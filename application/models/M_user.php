<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_user extends CI_Model{

    public $table = "qvt_user";
    

    public function __construct(){
        parent::__construct();
    }

    public function get_user($user, $password){
        return $this->db->select('*')
            ->from($this->table)
            ->where("user_email",$user)
            ->where("user_password",$password)
			->where('active',1)
            ->get()
            ->result();
    }

    public function add_user($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

    public function address_exist($email){
        return $this->db->select('*')
            ->from($this->table)
            ->where("user_email", $email)
			->where('active', 1)
            ->get()
            ->result();
    }
	
	public function update_user($id, $data)
    {
        $this->db->where('user_id',$id);
		$this->db->set($data);
		$this->db->update('qvt_user');
    }
	
	public function get_users(){
        return $this->db->select('*')
            ->from($this->table)
			->where('active',1)
            ->get()
            ->result();
    }
	public function delete_user($user_id)
    {
        $this->db->where('user_id',$user_id);
		$this->db->set('active', 0);
		$this->db->update('qvt_user');
    }
	
	public function get_user_ById($user_id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("user_id",$user_id)
            ->get()
            ->result()[0];
    }
}