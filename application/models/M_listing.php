<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_listing extends CI_Model{

    public $table = "listing";

    public function __construct(){
        parent::__construct();
    }
	public function get($listing_id){
        $this->db->select('*');
		$this->db->from('listing');
		$this->db->where('listing_id',$listing_id); 
		$query = $this->db->get();
		return($query->result()); 	
    }
	public function get_ById($listing_id){
        $this->db->select('*');
		$this->db->from('listing');
		$this->db->where('listing_id',$listing_id); 
		$query = $this->db->get();
		return($query->result()[0]); 	
    }

    public function add_listing($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

    public function get_listing(){
        $this->db->select('*');
        $this->db->from('listing');
        $this->db->where('deleted',0);
		$query = $this->db->get();
		return($query->result()); 
    }

    public function update_listing($listing_id, $data)
    {
        $this->db->set($data);
        $this->db->where('listing_id',$listing_id);
		$this->db->update('listing');
    }

    public function delete_listing($listing_id){
        $this->db->set('deleted', 1);
        $this->db->where('listing_id',$listing_id);
		$this->db->update('listing');
    }
}