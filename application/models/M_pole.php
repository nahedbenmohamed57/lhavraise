<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_pole extends CI_Model{

    public $table = "pole";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_perimetre');
    }
	
	public function get_pole(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }
    public function get_pole_ById($pole_id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("id_pole",$pole_id)
            ->get()
            ->result()[0];
    }
    
     public function add_pole($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }
    public function update_pole($id, $data)
    {
        $this->db->where('id_pole',$id);
        $this->db->set($data);
        $this->db->update('pole');
    }
    public function delete_pole($pole_id)
    {
        $this->db->where('id_pole',$pole_id);
        $this->db->delete('pole');
        $perimetres=$this->m_perimetre->get_perimetre($pole_id);
        foreach($perimetres as $perimetre){
            $this->m_perimetre->delete_perimetre($perimetre->id_perimetre);
        } 
    }
}