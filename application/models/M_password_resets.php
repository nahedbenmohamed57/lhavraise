<?php

class M_password_resets extends CI_Model 
{
    var $table = 'password_resets';
	
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	public function getByMail($email)
	{
        $this->db->from($this->table);
		$this->db->where("email",$email);
        $query = $this->db->get();
        return $query->row();
    }
	public function getByToken($token)
	{
        $this->db->from($this->table);
		$this->db->where("token",$token);
        $query = $this->db->get();
        return $query->row();
    }
}
