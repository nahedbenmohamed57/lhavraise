<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_unite extends CI_Model{

    public $table = "unite";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_perimetre');
         $this->load->model('m_pole');
    }
    public function get_unite2(){
        return $this->db->select('*')
            ->from($this->table)
            ->order_by("perimetre_id ", "asc")
            ->get()
            ->result();
    }
	
	public function get_unite($id_p){
        return $this->db->select('*')
            ->from($this->table)
            ->order_by("perimetre_id ", "asc")
            ->where("perimetre_id",$id_p)
            ->get()
            ->result();
    }
    public function get_unite_ById($id_per){
        return $this->db->select('*')
            ->from($this->table)
            ->where("id_unite",$id_per)
            ->get()
            ->result()[0];
            
    }
     public function get_unite_ById2($id_per){
        return $this->db->select('*')
            ->from($this->table)
            ->where("id_unite",$id_per)
            ->get()
            ->result();
    }
    
     public function add_unite($entry,$id_p){
        $entry['pole_id'] = $id_p;
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }
    public function update_unite($id, $data)
    {
        $this->db->where('id_unite',$id);
        $this->db->set($data);
        $this->db->update('unite');
    }
    public function delete_unite($per_id)
    {
        $this->db->where('id_unite',$per_id);
        $this->db->delete('unite');
    }
}