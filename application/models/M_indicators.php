<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class M_indicators extends CI_Model{

    public $table = "indicators";

    public function __construct(){
        parent::__construct();
        
    }

    public function insert_indicators($data = null)
    {
      //$this->db->truncate('indicators');
       if(!empty($data)) {
        $this->db->insert_batch('indicators', $data); 
       }
    }
    public function found_indicators_by_famille($family_id, $limit) {
      return $this->db->select('*')
            ->from($this->table)
            ->where("family_id", $family_id)
            ->order_by('date_insert','DESC')
            ->limit($limit)
            ->get()
            ->result();
    }
}