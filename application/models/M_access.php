<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_access extends CI_Model{

    public $table = "access";

    public function __construct(){
        parent::__construct();
    }
	
	public function get_module_role($role_id){
        $this->db->select('*');
		$this->db->from('access');
		$this->db->join('modules', 'modules.module_id = access.module_id');
		$this->db->where('access.role_id',$role_id); 
		$this->db->order_by("modules.sort", "asc");
		$query = $this->db->get();
		return($query->result()); 
		
    }
}