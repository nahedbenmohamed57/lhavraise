<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class m_family_rps extends CI_Model{

    public $table = "family_rps";

    public function __construct(){
        parent::__construct();
        $this->load->model('m_facteur');
    }

    public function get_family($family_id){
        return $this->db->select('*')
            ->from($this->table)
            ->where("family_id",$family_id)
            ->get()
            ->result()[0];
    }

    public function add_family($entry){
        if($this->db->insert($this->table,$entry)){
            return true;
        }
        return false;
    }

	public function update_family($family_id, $data)
    {
        $this->db->where('family_id',$family_id);
		$this->db->set($data);
		$this->db->update('family_rps');
    }
	
	public function get_families(){
        return $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();
    }

    public function delete_family($family_id){
        $this->db->where('family_id', $family_id);
        $this->db->delete('family_rps');
        $facteurs =  $this->m_facteur->get_facteurs_ByFamily($family_id); 
        if(empty($facteurs)) {
            foreach($facteurs as $facteur){
                $this->m_facteur->delete_facteur($facteur->facteur_id);
            } 
        }
        
    }

    public function get_families_ToQCM(){
        $this->db->select('*');
        $this->db->from('family_rps');
        $this->db->join('facteur', 'facteur.family_id = family_rps.family_id');
        $this->db->join('device', 'facteur.facteur_id = device.facteur_id');
        $this->db->join('device_has_listing', 'device_has_listing.device_id = device.device_id');
        $this->db->join('listing', 'listing.listing_id = device_has_listing.listing_id');
        $this->db->order_by("device.device_id", "asc");
         $this->db->order_by("listing.listing_id ", "asc");
        $query = $this->db->get() ->result();
        return ($query);

    }

    public function get_families_ToExport(){
        $this->db->select('*');
        $this->db->from('family_rps');
        $this->db->join('facteur', 'facteur.family_id = family_rps.family_id');
        $this->db->join('device', 'facteur.facteur_id = device.facteur_id');
        $query = $this->db->get() ->result();
        return ($query);
    }
}