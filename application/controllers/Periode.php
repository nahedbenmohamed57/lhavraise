<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends CI_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->load->model('m_periode');
    }

    public function index()
	{ 
		$data["active"] = $this->m_periode->get_active()->libelle;
		$this->load->render('periode/index', $data);
	}

	public function cloturer() {
		$this->m_periode->clear_active();
		redirect("/periode");
	}

	public function historique() {
		$data["periodes"] = $this->m_periode->get();
		$this->load->render('periode/historique', $data);
	}

	public function nouvelle() {
		$data["active"] = $this->m_periode->get_active()->libelle;
		
		if ($_POST) {
			$data = array(
				"libelle" => $this->input->post('libelle'),
				"active" => 1
			);

			$this->m_periode->add($data);
			redirect("/periode");
		}

		$this->load->render('periode/nouvelle', $data);
	}
}

