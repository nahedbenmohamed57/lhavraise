<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultat extends CI_Controller {

    public function __construct() {
    	error_reporting(-1);
		ini_set('display_errors', 1);
        parent::__construct();
        $this->load->model('m_family');
        $this->load->model('m_family_rps');
        $this->load->model('m_facteur');
		$this->load->model('m_determinant');
		$this->load->model('m_evaluation');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_listing');
		$this->load->model('m_perimetre');
		$this->load->model('m_pole');
		$this->load->model('m_rps_evaluation');
		$this->load->model('m_periode');
		$this->load->model('m_deviceHasListing');
		$this->load->model('m_unite');
		$this->load->library('MY_Form_validation');
    }

    public function index() {
		$data["families"] = $this->m_family->get_families();
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		//redirect('resultat/show/1');
 		$this->load->render('resultat/index', $data);
	}

   /* public function general() {
		$data["families"] = $this->m_family->get_families();
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		redirect('resultat/show/1');
 		//$this->load->render('resultat/show/1', $data);
	}*/

	public function show($focus) {
		$family_rps = $this->m_family_rps->get_families();

		foreach ($family_rps as $f) {
			$f->facteur = $this->m_facteur->get_facteurs_ByFamily($f->family_id);
		}

		$data["family_rps"] = $family_rps;
        $data["periodes"]   = $this->m_periode->get();

		$data["focus"] = $focus;
		if($focus == 2) {
			$data["poles"] = $this->m_pole->get_pole();
			//get primietre by pôle
		}
		if($focus == 3) { 
			$data["perimetres"] = $this->m_perimetre->get_perimetre2();
		}
		if($focus == 4) { 
			$data['units']      = $this->m_unite->get_unite2();
		}
		$this->load->render('resultat/resultat', $data);
	}

	public function data($focus = '',$pole_id = null, $etab_id = null, $unit = null, $periode = null) {

		$family_rps = $this->m_family_rps->get_families();

		foreach ($family_rps as $f) {
			$f->facteur = $this->m_facteur->get_facteurs_ByFamily($f->family_id);
		}
		$data["family_rps"] = $family_rps;
		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		$data['active_periode_id'] = $active_periode_id;
       
		if($focus == 1) {
	        $data["poles"]     = $this->m_pole->get_pole();
			foreach ($data["poles"] as $key => $p) {
				//get evaluation by pole for the last periode
				$last_eval = $this->m_rps_evaluation->get_eval_by_pole($p->id_pole, $active_periode_id);
				$p->evals = [];
				foreach ($last_eval as $k => $eval) {
					//get detail evaluation
					$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);
					$p->evals = $last_eval;
					if(!empty($last_eval )) {
						foreach ($last_eval as $i => $value) {
							$value->impact = '';
							$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
							
							if( !empty($impact)) {
								$value->impact = floatval($impact[0]->moy)*1.25;
							} 	
						}
					}	
				}
			}
			
			$this->load->view('resultat/data', $data);
		} elseif($focus == 2) {
			if($pole_id > 0) {
				//get list of etab for the selected pole
				$data["perimetres"]     = $this->m_perimetre->get_perimetre($pole_id);
				foreach ($data["perimetres"] as $key => $p) {
					//get evaluation by pole for the last periode
					$last_evals = $this->m_rps_evaluation->get_eval_by_etablissement($p->id_perimetre, $active_periode_id);
					$p->evals = $last_evals;
					foreach ($p->evals as $k => $eval) {
						//get detail evaluation
						$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);
						
						$eval->last_eval = $last_eval;
						//var_dump($p->evals);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $value) {
								$value->impact = '';
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
								
								if( !empty($impact)) {
									$value->impact = floatval($impact[0]->moy)*1.25;
								} 	
							}
						}	
					}
				}
				$this->load->view('resultat/data_pole', $data);
			}
		} elseif($focus == 3) {
			if($etab_id > 0) {
				//get list of units for the selected pole
				$data["units"]     = $this->m_unite->get_unite($etab_id);
				foreach ($data["units"] as $key => $p) {
					//get evaluation by pole for the last periode
					$last_evals = $this->m_rps_evaluation->get_eval_by_unit($p->id_unite, $active_periode_id);
					$p->evals = $last_evals;
					foreach ($p->evals as $k => $eval) {
						//get detail evaluation
						$last_eval = $this->m_rps_evaluation->get_details_eval($eval->id_rps_evaluation);
						
						$eval->last_eval = $last_eval;
						//var_dump($p->evals);
						if(!empty($last_eval )) {
							foreach ($last_eval as $i => $value) {
								$value->impact = '';
								$impact = $this->m_facteur->get_moy_indicator_by_factor($value->facteur_id);
								
								if( !empty($impact)) {
									$value->impact = floatval($impact[0]->moy)*1.25;
								} 	
							}
						}	
					}
				}
				$this->load->view('resultat/data_perimetres', $data);
			}
		}
	}

	public function unit_result($pole_id= null, $perimetre_id = null, $unit_id = null, $periode = null )
	{
		
		$data["poles"]        = $this->m_pole->get_pole();
		$data["perimetres"]   = $this->m_perimetre->get_perimetre($pole_id);
		$data['units']        = $this->m_perimetre->get_units($perimetre_id);
		//get rps evaluation with the spacific pole , perimetre and unitif we found it we do an edit not a new isert

		$data['pole_id']      = $pole_id;
		$data['perimetre_id'] = $perimetre_id;
		$data['unit_id']      = $unit_id;

		if($periode > 0) {
			$active_periode_id = $periode;
		} else {
			$periode_open       = $this->m_periode->get_active();
			$active_periode_id = 0;
			if(!empty($periode_open)) {
				$active_periode_id = $periode_open->id;
			}
		}
		

		$data["families_rps"] = $this->m_family_rps->get_families(); 
		$data["facteurs"] = $this->m_facteur->get_facteurs();
		if(!empty($data["facteurs"])) {
			foreach ($data["facteurs"] as $key => $fact) {
				$fact->impact = 0;
				$impact = $this->m_facteur->get_moy_indicator_by_factor($fact->facteur_id);
				if( !empty($impact)) {
					$fact->impact = floatval($impact[0]->moy)*1.25;
				} 
				$allFacteors = $this->m_facteur->get_indicator_by_factor($fact->facteur_id);
				$fact->fiabilite =  0;
				if(!empty($allFacteors)) {
					$hasTrend = 0;
					foreach ($allFacteors as $key => $fa) {
						if($fa->trend_index > 0) {
							$hasTrend++;
						}
					}
					if($hasTrend>0) {
						$fact->fiabilite = sizeof($allFacteors)/$hasTrend;
					}
				}
			}
		}

        $evaluation_rps = $this->m_rps_evaluation->get_evaluation_by_periode($pole_id, $perimetre_id, $unit_id, $active_periode_id);
        $data['evaluation_qantitive'] = [];
        if(!empty($evaluation_rps)) {
        	$evaluation_qantitive = $this->m_rps_evaluation->get_evaluation_by_ref_eval($evaluation_rps->id_rps_evaluation);
        	
        	$data['evaluation_qantitive'] = $evaluation_qantitive;
        }
 		$this->load->render('resultat/result_unit', $data);
	}

	/*public function table($id_pole = 0, $id_perim = 0) {
		if ($id_pole == 'all') { $id_pole = 0; }
		if ($id_perim == 'all') { $id_perim = 0; }

		$data["families"] = $this->m_family->get_families(); 
		
		foreach ($data['families'] as $family) {
			$family->determinants = $this->m_determinant->get_determinants_ByFamily($family->family_id);

			foreach ($family->determinants as $determinant) {
				$determinant->dispositifs = $this->m_device->get_device_ByDeter($determinant->determinant_id);
				$determinant->importance = $this->m_determinant->get_importance($determinant->determinant_id, $id_pole);

				foreach ($determinant->dispositifs as $device) {
					$device->avancement = $this->m_device->get_avancement($device->device_id, $id_pole, $id_perim);
				}
			}
		}

		$this->load->view('resultat/table', $data);
	}
	
	public function index2($id_pole,$id_per) {
		$data["families"] = $this->m_family->get_families(); 
		$data["deters"] = $this->m_determinant->get_determinants_byPole($id_pole);

		$data["devices"] = $this->m_device->get_all_devices();
		$data["id_pole"] = $id_pole;
		$data["id_perimetre"] = $id_per;
		$data["outputs"] = $this->m_family->get_families_ToQCM(); 
		$families=$data["families"];
		$deters=$data["deters"];
		$nb=0;
		
		for($j=0; $j<sizeof($families); $j++)
    	{   
   	        for($i=0; $i< sizeof($deters); $i++) 
	        {
				$avancement = 0;
				
				if($deters[$i]->family_id == $families[$j]->family_id )
	            {
	             	$t_dev[$nb]["device"]= $this->m_device->get_device_ByDeter($deters[$i]->determinant_id);
	             	$t_dev[$nb]["idDeter"] = $deters[$i]->determinant_id; 
	             	$t_de = $t_dev[$nb]["device"];
	             	$size = sizeof($t_dev[$nb]["device"]);
					$nb++;
					 
	             	for ($k=0; $k <$size; $k++) { 
	             		$tab_cot[$k]=$this->m_evaluation->get_evaluation_cotation2($_SESSION["user_id"],$t_de[$k]->device_id,$id_per,$id_pole); 
	             		
	             		$avancement = $avancement + $tab_cot[$k]->avancement;
					}
					 
	             	$tab_avan[$nb] = round($avancement / $size);
	            }
	        }
		}
		
		$data["t_avan"] = $tab_avan;

 		$this->load->render('resultat/resultat_perimetre',$data);
	}

	function getListDevice($idDeter) {
		$result["device"] = $this->m_device->get_device_ByDeter($idDeter); 
		$result["idDeter"] = $idDeter; 
		echo json_encode($result); 
	}
	
	function getEvaluationCotation2($idDevice,$idDeter,$id_per) {
		$result["cotation"] = $this->m_evaluation->get_evaluation_cotation2($_SESSION["user_id"],$idDevice,$id_per); 
		if($result["cotation"] == false){
			$result["cotation"]['listing_id']  = 0;
			$result["cotation"]['avancement']  = 0; 
			$result["cotation"]["listing_name"]= "Pas de choix"; 
		}
		$result["idDeter"] = $idDeter; 
		$result["idDevice"] = $idDevice; 
		echo json_encode($result); 
	}

	function getEvaluationCotation($idDevice,$idDeter) {
		$result["cotation"] = $this->m_evaluation->get_evaluation_cotation2($_SESSION["user_id"],$idDevice); 
		if($result["cotation"] == false){
			$result["cotation"]['listing_id']  = 0;
			$result["cotation"]['avancement']  = 0; 
			$result["cotation"]["listing_name"]= "Pas de choix"; 
		}
		$result["idDeter"] = $idDeter; 
		$result["idDevice"] = $idDevice; 
		echo json_encode($result); 
	}*/
}
