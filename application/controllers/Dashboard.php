<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_role');
    }

    public function index()
	{ 
        $data["user"] = $this->m_user->get_user_ById($_SESSION['user_id']); 
        $data["user"]->user_role= $this->m_role->get_role( $data["user"]->user_role);
        $this->load->render('dashboard',$data);
    }   
}
