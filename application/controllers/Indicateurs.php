<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicateurs extends CI_Controller {

    public function __construct() {
    	error_reporting(-1);
		ini_set('display_errors', 1);
        parent::__construct();
        $this->load->model('m_family');
		$this->load->model('m_determinant');
		$this->load->model('m_evaluation');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_listing');
		$this->load->model('m_perimetre');
		$this->load->model('m_pole');
		$this->load->model('m_evaluation');
		$this->load->model('m_deviceHasListing');
		$this->load->model('m_indicators');
		$this->load->library('MY_Form_validation');
    }

    public function index() {
		$data["families"] = $this->m_family->get_families(); 
		$data["poles"] = $this->m_pole->get_pole();
		$data["perimetres"] = $this->m_perimetre->get_perimetre2();
 		$this->load->render('indicateurs/index', $data);
	}

	public function table($id_pole = 0, $id_perim = 0) {
		if ($id_pole == 'all') { $id_pole = 0; }
		if ($id_perim == 'all') { $id_perim = 0; }

		$data["families"] = $this->m_family->get_families(); 
		
		foreach ($data['families'] as $family) {
			$family->determinants = $this->m_determinant->get_determinants_ByFamily($family->family_id);

			foreach ($family->determinants as $determinant) {
				$determinant->dispositifs = $this->m_device->get_device_ByDeter($determinant->determinant_id);
				$determinant->importance = $this->m_determinant->get_importance($determinant->determinant_id, $id_pole);

				foreach ($determinant->dispositifs as $device) {
					$device->avancement = $this->m_device->get_avancement($device->device_id, $id_pole, $id_perim);
				}
			}
		}

		$this->load->view('indicateurs/index', $data);
	}
	
	public function index2($id_pole,$id_per) {
		$data["families"] = $this->m_family->get_families(); 
		$data["deters"] = $this->m_determinant->get_determinants_byPole($id_pole);

		$data["devices"] = $this->m_device->get_all_devices();
		$data["id_pole"] = $id_pole;
		$data["id_perimetre"] = $id_per;
		$data["outputs"] = $this->m_family->get_families_ToQCM(); 
		$families=$data["families"];
		$deters=$data["deters"];
		$nb=0;
		
		for($j=0; $j<sizeof($families); $j++)
    	{   
   	        for($i=0; $i< sizeof($deters); $i++) 
	        {
				$avancement = 0;
				
				if($deters[$i]->family_id == $families[$j]->family_id )
	            {
	             	$t_dev[$nb]["device"]= $this->m_device->get_device_ByDeter($deters[$i]->determinant_id);
	             	$t_dev[$nb]["idDeter"] = $deters[$i]->determinant_id; 
	             	$t_de = $t_dev[$nb]["device"];
	             	$size = sizeof($t_dev[$nb]["device"]);
					$nb++;
					 
	             	for ($k=0; $k <$size; $k++) { 
	             		$tab_cot[$k]=$this->m_evaluation->get_evaluation_cotation2($_SESSION["user_id"],$t_de[$k]->device_id,$id_per,$id_pole); 
	             		
	             		$avancement = $avancement + $tab_cot[$k]->avancement;
					}
					 
	             	$tab_avan[$nb] = round($avancement / $size);
	            }
	        }
		}
		
		$data["t_avan"] = $tab_avan;

 		$this->load->render('resultat/resultat_perimetre',$data);
	}

	function getListDevice($idDeter) {
		$result["device"] = $this->m_device->get_device_ByDeter($idDeter); 
		$result["idDeter"] = $idDeter; 
		echo json_encode($result); 
	}
	
	function getEvaluationCotation2($idDevice,$idDeter,$id_per) {
		$result["cotation"] = $this->m_evaluation->get_evaluation_cotation2($_SESSION["user_id"],$idDevice,$id_per); 
		if($result["cotation"] == false){
			$result["cotation"]['listing_id']  = 0;
			$result["cotation"]['avancement']  = 0; 
			$result["cotation"]["listing_name"]= "Pas de choix"; 
		}
		$result["idDeter"] = $idDeter; 
		$result["idDevice"] = $idDevice; 
		echo json_encode($result); 
	}

	function getEvaluationCotation($idDevice,$idDeter) {
		$result["cotation"] = $this->m_evaluation->get_evaluation_cotation2($_SESSION["user_id"],$idDevice); 
		if($result["cotation"] == false){
			$result["cotation"]['listing_id']  = 0;
			$result["cotation"]['avancement']  = 0; 
			$result["cotation"]["listing_name"]= "Pas de choix"; 
		}
		$result["idDeter"] = $idDeter; 
		$result["idDevice"] = $idDevice; 
		echo json_encode($result); 
	}

		/** new functions **/
	public function fill_indicator($id_perimetre = null) {
		
        $data["families"]              = $this->m_family->get_families(); 
        if(!empty($data["families"] )) {
        	foreach ($data["families"]  as $key => $fam) {
        		$fam->list_determinant = $this->m_determinant->get_determinants_ByFamily($fam->family_id);
        		$fam->list_perimetre = $this->m_indicators->found_indicators_by_famille($fam->family_id,$limit = sizeof($fam->list_determinant));
        	}
        }
       
		$data["titledeter"]            = "Ajouter déterminant"; 
		$data["titleAddFamily"]        = "Ajouter famille";
		$data["titleUpdateFamily"]     = "Modifier famille";
		$data["titleUpdatDeterminant"] = "Modifier déterminant";

		//search if we have an old inout with the speic pole and perimetre
		//$data['permi'] = $this->m_indicators->found_indicators($id_perimetre);
		
		if(!empty($_POST)) {
			$mydata  = $_POST;
			$newdata = [];
			$i       = 0;
			$year    = [];
			
			foreach ($mydata as $key => $indicator) {
				
				$year3 = $year2 = $year1 = 0;
				$details = explode('_', $key);
			    array_push($year, $indicator);
			  
                if ($i == 3) {
                		array_push($newdata, array('id_perimetre'=> $id_perimetre, 'family_id' =>$details[0], 'determinant_id' => $details[1],'year3'=>$year[0],'year2'=> $year[1], 'year1' => $year[2],'trend_index' => $year[3],'date_insert' => date('Y-m-d H:i:s')));
                	$i = -1;
                	$year = [];
                }
                $i++;
			}
			$this->m_indicators->insert_indicators($newdata);
			Redirect('indicateurs/fill_indicator/'.$id_perimetre); 
		}
		
		$this->load->render('indicateurs/fill_indicator', $data);
	}
}
