<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Importance extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
    }

    public function index()
	{
        $this->load->model('m_pole');
        $data["poles"] = $this->m_pole->get_pole();
        $this->load->render('importance/index', $data);
    }

    public function config($pole_id) {
        $this->load->model('m_family');
        $this->load->model('m_determinant');
        $this->load->model('m_importance');
        
        if ($_POST) {
            foreach ($_POST as $id => $importance) {
                if ($this->m_importance->exists($id, $pole_id)) {
                    $this->m_importance->update($id, $pole_id, $importance);
                } else {
                    $this->m_importance->create($id, $pole_id, $importance);
                }
            }

            $data["saved"] = TRUE;
        }

        $data["pole"] = $pole_id;
        $data["importances"] = $this->m_importance->get($pole_id);
        $data["family"] = $this->m_family->get_families();
        $data["determinants"] = $this->m_determinant->get_determinants();
        $this->load->render('importance/config', $data);
    }
}
