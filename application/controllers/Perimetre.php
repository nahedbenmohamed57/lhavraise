<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class perimetre extends CI_Controller {

    public function __construct()
	{
		
        parent::__construct();
        error_reporting(-1);
		ini_set('display_errors', 1);
        $this->load->model('m_pole');
        $this->load->model('m_perimetre');
        $this->load->model('m_role');
        $this->load->library('MY_Form_validation');
    }

    public function index()
	{

		$data["perimetres"] = $this->m_perimetre->get_deatil_perimetre(); 

		$this->load->render('perimetre/define_perimeters', $data);
    }

    public function list_perimetre()
	{
		$data["perimetres"] = $this->m_perimetre->get_perimetre2(); 
		$this->load->render('perimetre/perimetre', $data);
    }
    public function perimetres_pole($id_p)
	{
		$data["perimetres"] = $this->m_perimetre->get_perimetre($id_p); 
		$data["p_id"]=$id_p;
		$data["pole"] = $this->m_pole->get_pole_ById($id_p);
		$this->load->render('perimetre/perimetre', $data);
    }
	public function crud_perimetre($task, $perimetre_id = false,$p_id=null){
		
		$data["roles"]    = $this->m_role->get_all_roles();
		$data["task"]     = $task;
		$data["poles"]    = $this->m_pole->get_pole();
		$data['p_id']     = $p_id;
		$data['has_pole'] = true;
		
		if($p_id == 'null') {
			$data['has_pole'] = false;
		}
		
		if(!empty($perimetre_id) && $perimetre_id != 'null' ){
			$data["perimetre"] = $this->m_perimetre->get_perimetre_ById($perimetre_id);
		}

		if($task == "delete"){
			$this->m_perimetre->delete_perimetre($perimetre_id); 
			Redirect('perimetre'); 
		}else if($task == "show"){
			$this->load->render('perimetre/show_perimetre', $data);
		}else  if($task == "update"){
			if(!empty($_POST)){
				$this->m_perimetre->update_perimetre($perimetre_id, $_POST); 
				if(isset($_POST['pole_id'])) {
					$p_id = $_POST['pole_id'];
				}

				Redirect('perimetre/perimetres_pole/'.$p_id); 
			}else {
				$data["perimetre"] = $this->m_perimetre->get_perimetre_ById($perimetre_id); 
				$this->load->render('perimetre/show_perimetre', $data);
			}
		}else if($task == "add"){ 
			if(!empty($_POST)){
				if($p_id == 'null') {
					if(isset($_POST['pole_id'])) {
						$p_id = $_POST['pole_id'];
					}
				} 
				$this->m_perimetre->add_perimetre($_POST,$p_id);
				Redirect('perimetre/perimetres_pole/'.$p_id); 
			}else {
				$this->load->render('perimetre/show_perimetre', $data);
			}
		}
	}

	public function get_perimetres_by_pole($id_p)
	{
		$perimetres = $this->m_perimetre->get_perimetre($id_p); 
		echo json_encode($perimetres);
    }

    public function get_units_by_pole($id_perimetre)
	{
		$units = $this->m_perimetre->get_units($id_perimetre); 
		echo json_encode($units);
    }
}
