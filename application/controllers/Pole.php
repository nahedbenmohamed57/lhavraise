<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pole extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_pole');
        $this->load->model('m_role');
        $this->load->library('MY_Form_validation');
        $this->load->model('m_user');
    }

    public function index()
	{
		if($_SESSION['role_id'] == 3 ) {
			Redirect('/dashboard');
		}

		$data["poles"] = $this->m_pole->get_pole(); 
		$this->load->render('pole/all_pole', $data);
    }
    
	public function crud_pole($task, $pole_id= false){
		$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		if(!empty($pole_id)){
			$data["pole"] = $this->m_pole->get_pole_ById($pole_id);
		}

		if($task == "delete"){
			$this->m_pole->delete_pole($pole_id); 
			Redirect('pole'); 
		}else if($task == "show"){
			$this->load->render('pole/pole', $data);
		}else  if($task == "update"){
			if(!empty($_POST)){
				$this->m_pole->update_pole($pole_id, $_POST); 
				Redirect('pole'); 
			}else {
				$data["pole"] = $this->m_pole->get_pole_ById($pole_id); 
				$this->load->render('pole/pole', $data);
			}
		}else if($task == "add"){ 
			if(!empty($_POST)){
				$this->m_pole->add_pole($_POST); 
				Redirect('pole'); 
			}else {
				$this->load->render('pole/pole', $data);
			}
		}
	}
}
