<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Family_rps extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('m_facteur');
        $this->load->model('m_family_rps');
        $this->load->model('m_determinant');
		$this->load->model('m_role');
		$this->load->model('m_device');
		$this->load->model('m_listing');
		$this->load->model('m_deviceHasListing');
		$this->load->library('MY_Form_validation');
    }

    public function index()
	{
		
		$data["families_rps"] = $this->m_family_rps->get_families(); 
		$data["facteurs"] = $this->m_facteur->get_facteurs();
		
		$data["titledeter"] = "Ajouter déterminant"; 
		$data["titleAddFamilyRps"] = "Ajouter une famille RPS";
		$data["titleUpdateFamily"] = "Modifier famille";
		$data["titleUpdatfacteur"] = "Modifier facteur";
		

		$this->load->render('family_rps/all_facteurs', $data);
    }

    public function crud_family_rps($task, $family_id= false){
    	
		$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		if(!empty($family_id)){
			$data["family"] = $this->m_family_rps->get_family($family_id);
		}
		if($task == "delete"){
			$this->m_family_rps->delete_family($family_id); 
			header('Content-Type: application/json');
			echo json_encode('facteurs');
			exit();
		}else if($task == "show"){
			$this->load->render('users/user', $data);
		}else  if($task == "update"){
			if(!empty($_POST)){
				$this->m_family_rps->update_family($_POST["family_id"], $_POST); 
				Redirect('facteurs');; 
			}else {
				$data["family"] = $this->m_family_rps->get_family($family_id); 
				$this->load->view('family_rps/_family_rps',$data);
			}
		}else if($task == "add"){ 
			if(!empty($_POST)){
				var_dump($_POST);
				$this->m_family_rps->add_family($_POST); 
				Redirect('facteurs'); 
			}else {
				$this->load->view('family_rps/_family_rps');
			}
		}
	}
}
