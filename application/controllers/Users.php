<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class users extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_role');
        $this->load->library('MY_Form_validation');
    }

    public function index()
	{
		$data["users"] = $this->m_user->get_users(); 
		foreach($data["users"] as $user){
			$user->user_role = $this->m_role->get_role($user->user_role);
		}
		$this->load->render('users/all_users', $data);

    }
    public function verifemail()
	{
		$current_mail = $this->m_user->address_exist($_POST["user_email"]);
		
        if ($current_mail) {
            echo json_encode(array("result"=>1));
        } else {
            echo json_encode(array("result"=>0));
        }
    }
	public function crud_user($task, $user_id= false){
        error_reporting(-1);
		ini_set('display_errors', 1);

		$data["roles"] = $this->m_role->get_all_roles();
		$data["task"] = $task;
		$data['monprofil'] = 0; 
		if (isset($_GET['success'])) {
            $data['updated'] = TRUE;
		} else {
			$data['updated'] = false;
		}
		
		if(!empty($user_id)){
			$data["user"] = $this->m_user->get_user_ById($user_id);
		}
		if($task == "delete"){
			$this->m_user->delete_user($user_id); 
			Redirect("users"); 
		}else if($task == "show"){
			$this->load->render('users/user', $data);
		}else  if($task == "update"){
			$data['monprofil'] = 1; 
			if(!empty($_POST)){
				if(isset($_POST["user_password"])){
					$_POST["user_password"] = md5($_POST["user_password"]);
					unset($_POST["password_user_confirm"]); 
				}

				if ($user_id == $_SESSION["user_id"]) {
					$redirect="profil"; 
				} else {
					$redirect="user"; 
				}
				$this->m_user->update_user($user_id, $_POST);
				echo json_encode(array("redirect"=>$redirect,"user"=>$user_id));
			} else {
				$this->load->render('users/user', $data);
			}
		}else {
			if(!empty($_POST)){
				$_POST["user_password"] =  md5($_POST["user_password"]);
				if($this->m_user->add_user($_POST)){
					echo json_encode(array("result"=>1));
				}else{
					echo json_encode(array("result"=>0));
				}

			}else {
				$this->load->render('users/user', $data);
			}
		}
	}
}
