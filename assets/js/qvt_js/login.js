function validate(tag, type, message) 
{
	if (type == 'error') {
		tag.next().text(message).addClass('color-red');
		tag.addClass('border-red').removeClass('border-green');
		$('.submittedButtom').prop('disabled', true);
	} else {
		tag.next().text('').removeClass('color-red');
		tag.addClass('border-green').removeClass('border-red');
		$('.submittedButtom').prop('disabled', false);
	}
}
function verifyMail(callback) 
{
	var email =  $('#mail').val(); 
	var url = decodeURIComponent('../login/verifyMail/' + email); 
	var data;
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		url: url,
		success: function (resp) {
			data = resp;
			if(resp == 1){
				validate($('#mail'), "success", "");
			}
			if (callback) {
				callback(data);
			}
		},
	});
}
function sendMail() {
	verifyMail(function(resp) {
		if(resp == 0){
			validate($('#mail'), "error", "Utilisateur n'existe pas");
		}else {
			validate($('#mail'), "success", "");
			var email = $('#mail').val(); 
			var newUrl = decodeURIComponent('../login')
			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: decodeURIComponent('../login/forgetPwd/' + email),
				success: function (resp) {
					console.log(resp); 
					if(resp == true){
						window.location.href = newUrl ; 
					}else {
						location.reload();
					}
				},
			});
		}
	}); 
}
function checkPwd(token) {
	var errors = 0;
	if($("#password1").val()!=''){	
		if ($("#password1").val().length<8){
			validate($("#password1"), "error", "8 caractere au minimum");
			errors++;
		}else{
			validate($("#password1"), "success", "");
		}
	}else if($("#password1").val()==''){
		validate($("#password1"), "success", "");
	}
	if ($("#password2").val()!=$("#password1").val()){
		validate( $("#password2"), "error", "Confirmation mot de passe est incorrecte");
		errors++;
	}else{
		validate( $("#password2"), "success", "");
	}
	if (errors!=0){
		return false;
		///refrech page 
	}else{
		var pwd = encodeURIComponent($("#password1").val());
		window.location = window.location + '/' + pwd; 
	//	window.location = window.location + '../login/resetpwd/' + token + '/' + pwd;
	}
}