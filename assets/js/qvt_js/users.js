jQuery(function ($) {
    function validate(tag, type, message) {
        if (type == 'error') {
            tag.next().text(message).addClass('color-red');
            tag.addClass('border-red').removeClass('border-green');
            $(':input[type="submit"]').prop('disabled', true);
        } else {
            tag.next().text('').removeClass('color-red');
            tag.addClass('border-green').removeClass('border-red');
            $(':input[type="submit"]').prop('disabled', false);
        }
    }

    $("#name_user").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            validate($(this), "success", "");
        }
    })

    $("#lastname_user").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            validate($(this), "success", "");
        }
    })

    $("#email_user").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            var reg = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/i;
            if (!reg.test(value)) {
                validate($(this), "error", "Champ invalide");
            } else {
                var formData = new FormData($('#addUser')[0]);
                $.ajax({
                    type: 'POST',
                    data: formData,
                    async: false,
                    url: window.location.origin + "/users/verifemail",
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (res) {
                        if (res.result == 1) {
                            validate($("#email_user"), "error", "Email exist déjà");
                        } else if (res.result == 0) {
                            validate($("#email_user"), "success", "");
                        }
                    }
                });
            }
        }
    })

    $("#password_user").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            if (value.length < 6) {
                validate($(this), "error", "6 caractere au minimum");
            } else {
                validate($(this), "success", "");
            }
        }
    })
    $("#user_role").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            validate($(this), "success", "");
        }
    })

    $("#user_id_access").on("blur", function () {
        var value = $(this).val().trim();
        if (value == 0) {
            validate($(this), "error", "Champ obligatoire");
        } else {
            validate($(this), "success", "");
        }
    })



    //add user
     $("#addUser").submit(function(event) {
        event.preventDefault();
        var errors = 0;
        if ($("#name_user").val().trim() == '') {
            validate($("#name_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#name_user"), "success", "");
        }

        if ($("#lastname_user").val().trim() == '') {
            validate($("#lastname_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#lastname_user"), "success", "");
        }

        if ($("#user_id_access").val() == 0) {
            validate($("#user_id_access"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#user_id_access"), "success", "");
        }

        if ($("#email_user").val().trim() == '') {
            validate($("#email_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            var reg = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/i;
            if (!reg.test($("#email_user").val().trim())) {
                validate($("#email_user"), "error", "Champ invalide");
                errors++;
            } else {
                var formData = new FormData($('#addUser')[0]);
                $.ajax({
                    type: 'POST',
                    data: formData,
                    async: false,
                    url: window.location.origin + "/users/verifemail",
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (res) {
                        if (res.result == 1) {
                            validate($("#email_user"), "error", "Email exist déjà");
                            errors++;
                        } else if (res.result == 0) {
                            validate($("#email_user"), "success", "");
                        }
                    }
                });
            }
        }

        if ($("#password_user").val().trim() == '') {
            validate($("#password_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            if ($("#password_user").val().length < 6) {
                validate($("#password_user"), "error", "6 caractere au minimum");
                errors++;
            } else {
                validate($("#password_user"), "success", "");
            }
        }
        if($('#user_role').val().trim() == '') {
            validate($("#user_role"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#user_role"), "success", "");
        }
        if (errors != 0) {
            return 0;
        } else {
            var formData = new FormData($('#addUser')[0]);
            $.ajax({
                type: 'POST',
                data: formData,
                async: false,
                url: window.location.origin + "/users/crud_user/add",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (res) {
                    if (res.result == 0) {
                        window.location.replace(window.location.origin + "/crud_user/add");
                    } else if (res.result == 1) {
                        window.location.replace(window.location.origin + "/users");
                    }
                }
            });
        }
    })


    //edit
    $("#edit-user").on("click", function (e) {
        e.preventDefault();

        var errors = 0;

        if ($("#name_user").val().trim() == '') {
            validate($("#name_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#name_user"), "success", "");
        }

        if ($("#lastname_user").val().trim() == '') {
            validate($("#lastname_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#lastname_user"), "success", "");
        }

        if ($("#user_id_access").val() == 0) {
            validate($("#user_id_access"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#user_id_access"), "success", "");
        }

        if ($("#email_user").val().trim() == '') {
            validate($("#email_user"), "error", "Champ obligatoire");
            errors++;
        } else {
            var reg = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/i;
            if (!reg.test($("#email_user").val().trim())) {
                validate($("#email_user"), "error", "Champ invalide");
                errors++;
            } else {

            }
        }

        if (typeof $("#password_user") === "undefined") {
            if ($("#password_user").val().trim() == '') {
                validate($("#password_user"), "error", "Champ obligatoire");
                errors++;
            } else {
                if ($("#password_user").val().length < 6) {
                    validate($("#password_user"), "error", "6 caractere au minimum");
                    errors++;
                } else {
                    validate($("#password_user"), "success", "");
                }
            }
        }

        if (typeof $("#password_user_confirm") === "undefined") {
            if ($("#password_user_confirm").val().trim() == '') {
                validate($("#password_user_confirm"), "error", "Champ obligatoire");
                errors++;
            } else {
                if ($("#password_user_confirm").val() != $("#password_user").val()) {
                    validate($("#password_user_confirm"), "error", "Mots de passe non identiques");
                    errors++;
                } else {
                    validate($("#password_user_confirm"), "success", "");
                }
            }
        }

        if (errors != 0) {
            return 0;
        } else {
            var user_id = $('#user_id').val();
            var formData = new FormData($('#editUser')[0]);
            $.ajax({
                type: 'POST',
                data: formData,
                async: false,
                url: window.location.origin + '/users/crud_user/update/' + user_id,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (res) {
                    console.log(res);

                    if (res["redirect"] == "profil") {
                        window.location.replace(window.location.origin + "/profil?success");
                    } else {
                        window.location.replace(window.location.origin + "/users/crud_user/update/" + res["user"] + "?success");
                    }
                }
            });
        }
    })
})