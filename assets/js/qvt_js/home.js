jQuery(function ($) {
    function validate(tag, type, message) {
        if (type == 'error') {
            tag.next().text(message).addClass('color-red');
            tag.addClass('border-red').removeClass('border-green');
        } else {
            tag.next().text('').removeClass('color-red');
            tag.addClass('border-green').removeClass('border-red');
        }
    }

    $("#user").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            validate($(this), "success", "");
        }
    })

    $("#password").on("blur", function () {
        var value = $(this).val().trim();
        if (value == '') {
            validate($(this), "error", "Champ obligatoire");
        } else {
            validate($(this), "success", "");
        }
    })

    $("#submit-login").on("click", function (e) {
        e.preventDefault();
        var errors = 0;

        if ($("#user").val().trim() == '') {
            validate($("#user"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#user"), "success", "");
        }

        if ($("#password").val().trim() == '') {
            validate($("#password"), "error", "Champ obligatoire");
            errors++;
        } else {
            validate($("#password"), "success", "");
        }

        if (errors != 0) {
            return 0;
        } else {
            var formData = new FormData($('#login')[0]);
            $.ajax({
                type: 'POST',
                data: formData,
                async: false,
                url: window.location.href + "login/sign",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (res) {
                    if (res.result == 1) {
                        window.location.replace(window.location.href + "dashboard");
                    } else if (res.result == 0) {
                        $('#the-message').append('<div class="alert alert-danger">' +
                            '<span class="glyphicon glyphicon-remove"></span>' +
                            ' Verifier votre pseudo et mot de passe' +
                            '</div>');
                        $('.form-group').removeClass('has-error')
                            .removeClass('has-danger');
                        $('.text-danger').remove();

                        // close the message after seconds
                        $('.alert-danger').delay(1500).show(10, function () {
                            $(this).delay(1500).hide(10, function () {
                                $(this).remove();
                            });
                        });
                    }
                }
            });
        }
    })
})